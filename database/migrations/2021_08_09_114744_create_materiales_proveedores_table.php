<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMaterialesProveedoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('materiales_proveedores', function (Blueprint $table) {
            $table->id();
            $table->integer('material_id');
            $table->integer('proveedor_id');
            $table->double('precio')->comment("Precio del proveedor");
            $table->double('precio_publico')->comment("Precio para cotización");
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('materiales_proveedores');
    }
}
