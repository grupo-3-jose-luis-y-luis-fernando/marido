<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiciosAdicionalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitud_conceptos', function (Blueprint $table) {
            $table->id();
            $table->integer('solicitud_id');
            $table->integer('concepto_id');
            $table->tinyInteger('aceptado')->comment('1.-Aceptado 0.-No aceptado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitud_conceptos');
    }
}
