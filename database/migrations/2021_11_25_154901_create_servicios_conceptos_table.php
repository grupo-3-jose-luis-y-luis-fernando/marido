<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiciosConceptosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servicios_conceptos', function (Blueprint $table) {
            $table->id();
            $table->integer('servicio_id');
            $table->string('clave');
            $table->string('nombre');
            $table->decimal('costo_agendado', 11, 2);
            $table->decimal('costo_urgente', 11, 2);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servicios_conceptos');
    }
}
