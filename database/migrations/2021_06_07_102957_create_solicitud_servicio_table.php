<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSolicitudServicioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitud_servicio', function (Blueprint $table) {
            $table->id();
            $table->integer('servicio_id');
            $table->integer('cliente_id');
            $table->tinyInteger('tipo')->comment('1.- Urgente 2.- Programado');
            $table->timestamp('horario_programado')->nullable()->comment('fecha y hora en la que se programa la visita del técnico');
            $table->tinyInteger('estatus')->comment('1.-Procesando 2.-Asignado 3.-Finalizado 4.-Cancelado 5.-Trabajando 6.-Reasignado'); 
            $table->string('descripcion')->nullable();
            $table->string('orden_conekta')->nullable(); //Guarda el id de la orden final generado por conekta 
            $table->string('visita_conekta')->nullable(); // Guarda el id de la orden de la visita generada por conketa
            $table->string('estatus_orden')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitud_servicio');
    }
}
