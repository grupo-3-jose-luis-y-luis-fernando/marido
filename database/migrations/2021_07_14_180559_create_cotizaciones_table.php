<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCotizacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cotizaciones', function (Blueprint $table) {
            $table->id();
            $table->string('solicitud_id');
            $table->string('archivo')->nullable();
            $table->double("costo",10,2);
            $table->tinyInteger('tipo')->comment("1.-Estimación 2.-Cotización");
            $table->tinyInteger('estatus')->comment("1.- Enviada 2.-Aceptada 3.-Rechazada");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cotizaciones');
    }
}
