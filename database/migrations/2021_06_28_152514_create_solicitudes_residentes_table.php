<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSolicitudesResidentesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitudes_residentes', function (Blueprint $table) {
            $table->id();
            $table->integer('residente_id');
            $table->integer('solicitud_id');
            $table->timestamp('horario_llegada')->nullable(); //Para saber en horario en el que el residente debe llegar
            $table->timestamp('horario_inicio')->nullable(); //Para saber en qué horario inicio a trabajar el técnico
            $table->timestamp('horario_fin')->nullable(); //Para saber en que horario terminó el técnico
            $table->tinyInteger('estatus')->comment('1.-Asignado 2.-Finalizado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitudes_residentes');
    }
}
