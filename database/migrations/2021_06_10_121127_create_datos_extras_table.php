<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDatosExtrasTable extends Migration
{
    /**
     * Run the migrations.
     *Tabla que permite el almacenar datos extras para los usuarios, por ejemplo RFC
     * @return void
     */
    public function up()
    {
        Schema::create('datos_extras', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('rfc')->nullable();
            $table->string('avatar')->nullable();
            $table->string('cliente_conekta')->nullable();
            $table->string('matricula')->nullable();
            $table->string('telefono')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datos_extras');
    }
}
