<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicioTecnicoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servicio_tecnico', function (Blueprint $table) {
            $table->id();
            $table->integer('tecnico_id');
            $table->integer('solicitud_servicio_id');
            $table->timestamp('horario_estimado')->nullable(); //Para saber en que horario se estima que puede llegar el técnico
            $table->timestamp('horario_llegada')->nullable(); //Para saber el horario real en el que llegó el técnico
            $table->timestamp('horario_inicio')->nullable(); //Para saber en qué horario inicio a trabajar el técnico
            $table->timestamp('horario_fin')->nullable(); //Para saber en que horario terminó el técnico
            $table->tinyInteger('estatus')->comment('1.-Asignado 2.-Finalizado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servicio_tecnico');
    }
}
