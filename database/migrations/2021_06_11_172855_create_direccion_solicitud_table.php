<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDireccionSolicitudTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('direccion_solicitud', function (Blueprint $table) {
            $table->id();
            $table->integer('solicitud_id');
            $table->string('estado');
            $table->string('municipio');
            $table->string('localidad');
            $table->string('codigo_postal');
            $table->string('calle');
            $table->string('num_int')->nullable();
            $table->string('num_ext')->nullable();
            $table->text('referencias')->nullable();
            $table->string('latitud');
            $table->string('longitud');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('direccion_solicitud');
    }
}
