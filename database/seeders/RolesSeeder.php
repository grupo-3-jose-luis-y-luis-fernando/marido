<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RolesSeeder extends Seeder
{
    public function run()
    {
        Role::create(['name' => 'root']);               //Tiene acceso a todas las funciones del sistema
        Role::create(['name' => 'admin']);              //Tiene acceso a ciertas funciones (realizar la captura de los servicios)
        Role::create(['name' => 'tecnico']);            //Es el que realiza los servicios (in App)
        Role::create(['name' => 'residente']);         //Es el que realiza los servicios (in App)
        Role::create(['name' => 'cliente']);            //Es el que solicita los servicios (in App)
        Role::create(['name' => 'analista']);
    }
}
