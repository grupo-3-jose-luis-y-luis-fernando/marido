<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use Spatie\Permission\Models\Permission;

class UsersSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->insert([
            [
                'name'      =>'Test',
                'paterno'   =>'Root',
                'materno'   =>'Marido',
                'email'     =>'soporte@marido.mx',
                'password'  =>bcrypt('¡SoporteMarido!')
            ],[
                'name'      =>'Administrador',
                'paterno'   =>'Marido',
                'materno'   =>'Mx',
                'email'     =>'sistema@marido.mx',
                'password'  =>bcrypt('AdminMarido!')
            ],[
                'name'      =>'Test',
                'paterno'   =>'Técnico',
                'materno'   =>'Marido',
                'email'     =>'tecnico@marido.mx',
                'password'  =>bcrypt('TecnicoMarido!')
            ],[
                'name'      =>'Test',
                'paterno'   =>'Residente',
                'materno'   =>'Marido',
                'email'     =>'residente@marido.mx',
                'password'  =>bcrypt('ResidenteMarido!')
            ],[
                'name'      =>'Test',
                'paterno'   =>'Analista',
                'materno'   =>'Marido',
                'email'     =>'analista@marido.mx',
                'password'  =>bcrypt('AnalistaMarido!')
            ],[
                'name'      =>'Test',
                'paterno'   =>'Cliente',
                'materno'   =>'Marido',
                'email'     =>'cliente@marido.mx',
                'password'  =>bcrypt('ClienteMarido!')
            ]
        ]);
        
        
    
        User::find(1)->assignRole('root');
        User::find(2)->assignRole('admin');
        User::find(3)->assignRole('tecnico');
        User::find(4)->assignRole('residente');
        User::find(5)->assignRole('analista');
        User::find(6)->assignRole('cliente');
    }
}
