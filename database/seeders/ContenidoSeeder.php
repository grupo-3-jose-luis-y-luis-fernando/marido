<?php

namespace Database\Seeders;

use App\Models\Configuraciones;
use App\Models\Materiales;
use App\Models\MaterialesProveedores;
use App\Models\ServiciosConceptos;
use App\Models\UnidadesMedidas;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class ContenidoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categorias')->insert([
            ['nombre'=>'Aires acondicionados','costo_mano_obra'=>'100'],
            ['nombre'=>'Eléctrico','costo_mano_obra'=>'60'],
            ['nombre'=>'Fontaneria','costo_mano_obra'=>'25'],
            ['nombre'=>'Herreria','costo_mano_obra'=>'25'],
            ['nombre'=>'Impermeabilizante','costo_mano_obra'=>'25'],
            ['nombre'=>'Pintura','costo_mano_obra'=>'25'],
            ['nombre'=>'Tablaroca','costo_mano_obra'=>'25']
        ]);

        UnidadesMedidas::insert([
            [
                'nombre'        =>'Kilogramo',
                'abreviatura'   =>'Kg'
            ],
            [
                'nombre'        =>'Litro',
                'abreviatura'   =>'L'
            ],
            [
                'nombre'        =>'Mililitro',
                'abreviatura'   =>'ml'
            ],
            [
                'nombre'        =>'Pieza',
                'abreviatura'   =>'Pz'
            ]
        ]);

        Materiales::insert([
            [
                'nombre'    =>'Apagador sencillo Mca Royer',
                'unidad_id' =>'4'
            ],
            [
                'nombre'    =>'Contacto sencillo mca royer',
                'unidad_id' =>'4'
            ],
            [
                'nombre'    =>'Gas refrigerante',
                'unidad_id' =>'4'
            ],
            [
                'nombre'    =>'Llave mezcladora',
                'unidad_id' =>'4'
            ],
        ]);

        DB::table('servicios')->insert([
            [
                'nombre'        =>'Suministro y colocación de contacto',
                'categoria_id'  =>'2',
                'costo_agendado'=>'500',
                'costo_urgente' =>'1000',
                'descripcion'   =>'' 
            ] 
        ]);

        ServiciosConceptos::insert([
            ['servicio_id'=>1,"nombre"=>"Concepto 1","costo_agendado"=>250,"costo_urgente"=>300],
            ['servicio_id'=>1,"nombre"=>"Concepto 2","costo_agendado"=>251,"costo_urgente"=>301],
            ['servicio_id'=>1,"nombre"=>"Concepto 3","costo_agendado"=>252,"costo_urgente"=>302]
        ]);

        DB::table('proveedores')->insert([
            ['nombre'=>'Proveedor A','rfc'=>'XAXX010101000','direccion'=>'Conocida','telefono'=>'0000000000','email'=>'proveedora@dominio.com'],
            ['nombre'=>'Proveedor B','rfc'=>'XAXX010101000','direccion'=>'Conocida','telefono'=>'0000000001','email'=>'proveedorb@dominio.com']
        ]);

        MaterialesProveedores::insert([
            [
                "material_id"   =>1,
                "proveedor_id"  =>1,
                "precio"        =>80,
                "precio_publico"=>100,
            ],
            [
                "material_id"   =>2,
                "proveedor_id"  =>2,
                "precio"        =>80,
                "precio_publico"=>150,
            ],
            [
                "material_id"   =>3,
                "proveedor_id"  =>1,
                "precio"        =>80,
                "precio_publico"=>110,
            ],
            [
                "material_id"   =>4,
                "proveedor_id"  =>2,
                "precio"        =>80,
                "precio_publico"=>120,
            ],
        ]);

        Configuraciones::create([
            'pvk_conekta'   =>'key_reuEYSaYlMjh4jCGwVh7Me0',
            'puk_conekta'   =>'key_PRlK5zltmYTCDYb6LqwO1RV'
        ]);
    }
}
