<?php

namespace App\Providers;

use Illuminate\Http\Request;
use Symfony\Component\Finder\Finder;
use Illuminate\Support\Facades\Route;
use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    protected $namespace = 'App\Http\Controllers';
    
    public const Cliente = '/panel';

    public function boot()
    {
        $this->configureRateLimiting();

        $this->routes(function () {
            Route::prefix('api')
                ->middleware('api')
                ->namespace($this->namespace)
                ->group(function(){
                    $this->requireRoutes('routes/api');
                });

            Route::middleware('web')
                ->namespace($this->namespace)
                ->group(function(){
                    $this->requireRoutes('routes/web');
                });
        });
    }

    protected function configureRateLimiting()
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(60)->by(optional($request->user())->id ?: $request->ip());
        });
    }

    public function requireRoutes($path)
    {
        return collect(Finder::create()->in(base_path($path))->name('*.php'))->each(function($file){
            require $file->getRealPath();
        });
    }
}
