<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DireccionSolicitud extends Model
{
    use HasFactory;
    protected $table = "direccion_solicitud";
    protected $fillable = ["solicitud_id","estado","municipio", "localidad","codigo_postal","calle","num_int","num_ext","latitud","longitud","referencias"];
}
