<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MensajeTecnico extends Model
{
    use HasFactory;
    protected $table = "tecnico_mensaje_cliente";
    protected $fillable = ["tecnico_id","solicitud_id","mensaje"];
}
