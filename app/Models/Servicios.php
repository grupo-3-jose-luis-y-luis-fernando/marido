<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Servicios extends Model
{
    use SoftDeletes;
    protected $table = 'servicios';
    protected $fillable = ['nombre', 'categoria_id', 'costo_agendado', 'costo_urgente', 'descripcion'];
}
