<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SolicitudServicio extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = "solicitud_servicio";
    protected $fillable = ['servicio_id','cliente_id','tipo','horario_programado','estatus','descripcion','orden_conekta','estatus_orden','visita_conekta'];
}
