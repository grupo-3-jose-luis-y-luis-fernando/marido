<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Direcciones extends Model
{
    use HasFactory;
    protected $table = "direcciones";
    protected $fillable = ["user_id","estado","municipio", "localidad","codigo_postal","calle","num_int","num_ext","latitud","longitud","referencias","alias"];
}
