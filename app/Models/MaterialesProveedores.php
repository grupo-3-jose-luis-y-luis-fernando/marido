<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MaterialesProveedores extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = "materiales_proveedores";
    protected $fillable = ["material_id","proveedor_id","precio","precio_publico"];
}
