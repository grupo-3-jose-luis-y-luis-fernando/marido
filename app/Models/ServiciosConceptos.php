<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiciosConceptos extends Model
{
    use SoftDeletes;
    protected $table = "servicios_conceptos";    
    protected $fillable = ["servicio_id","clave","nombre","costo_agendado","costo_urgente"];
}
