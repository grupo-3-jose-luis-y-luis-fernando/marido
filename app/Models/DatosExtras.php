<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DatosExtras extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = "datos_extras";
    protected $fillable = ["rfc","user_id","avatar","cliente_conekta","telefono","matricula"];
}
