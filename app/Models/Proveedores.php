<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Proveedores extends Model
{
    use SoftDeletes;
    protected $table = 'proveedores';
    protected $fillable = ['nombre', 'rfc', 'direccion', 'telefono', 'email'];
}
