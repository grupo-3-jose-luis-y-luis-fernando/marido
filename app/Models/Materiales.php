<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Materiales extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = "materiales";
    protected $fillable = ["nombre","unidad_id"];
}
