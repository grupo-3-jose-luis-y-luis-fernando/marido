<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SolicitudesResidentes extends Model
{
    use HasFactory;
    protected $table = "solicitudes_residentes";
    protected $fillable = ["residente_id","solicitud_id","estatus","horario_inicio","horario_fin","horario_llegada"];
}
