<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServicioTecnico extends Model
{
    use HasFactory;
    protected $table = "servicio_tecnico";
    protected $fillable = ["tecnico_id","solicitud_servicio_id","estatus","horario_inicio","horario_fin","horario_llegada","horario_estimado"];
}
