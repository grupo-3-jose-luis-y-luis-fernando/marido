<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Nosotros extends Model
{
    protected $fillable = ["nosotros","imagen","mision","vision","valores"];
}
