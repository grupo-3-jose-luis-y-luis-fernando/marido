<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GaleriaSolicitud extends Model
{
    use HasFactory;
    protected $table = "galeria_solicitud";
    protected $fillable = ["imagen", "solicitud_id"];
}
