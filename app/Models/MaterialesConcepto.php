<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MaterialesConcepto extends Model
{    
    protected $fillable = ["solicitud_concepto_id",'material_id','cantidad'];
    protected $table = "materiales_concepto";
}
