<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UnidadesMedidas extends Model
{
    use SoftDeletes;    
    protected $table = "unidades_medidas";
    protected $fillable = ["nombre","abreviatura"];
}
