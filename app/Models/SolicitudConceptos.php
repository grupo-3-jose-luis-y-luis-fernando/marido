<?php

namespace App\Models;
 
use Illuminate\Database\Eloquent\Model;

class SolicitudConceptos extends Model
{ 
    protected $fillable = ["concepto_id","solicitud_id","aceptado"];
}
