<?php

namespace App\Http\Controllers\Administrativo;

use App\Http\Controllers\Controller;
use App\Models\Materiales;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class InventarioController extends Controller
{
    public function index()
    {
        return view("administrador.materiales.inventario.index");
    }

    public function datatable(Request $request)
    {
        $inventario = Materiales::select('materiales.id','materiales.nombre','inventario.cantidad','unidades_medidas.nombre as unidad')
        ->join('unidades_medidas','unidades_medidas.id','materiales.unidad_id')
        ->leftJoin('inventario','inventario.material_id','materiales.id')->get();
        return DataTables::of($inventario)
        ->addColumn('cantidad',function($inventario){
            return $inventario->cantidad ?? 0;
        })
        ->addColumn('btn', function($inventario){
            return "<form class='form' action='".route('root.crear_inventario')."'>
                        ".csrf_field()."
                        <input type='hidden' name='material' value='{$inventario->id}'>
                        <button type='sumbit' class='btn btn-success'>Ingresar</button>
                    </form>";
        })
        ->rawColumns(['btn'])
        ->toJson();
    }

    public function crear(Request $request)
    {
        $material = Materiales::select('materiales.*','unidades_medidas.nombre as unidad')
        ->join('unidades_medidas','unidades_medidas.id','materiales.unidad_id')
        ->where('materiales.id',$request->material)
        ->first();
        return response()->json([
            'modal' => view('administrador.materiales.inventario.form',[
                'material'  =>$material
            ])->render()
        ]);
    }

    public function guardar(Request $request)
    {
        
    }
}
