<?php

namespace App\Http\Controllers\Administrativo;

use App\Http\Controllers\Controller;
use App\Models\Configuraciones;
use Illuminate\Http\Request;

class ConfiguracionesController extends Controller
{
    public function index_conekta()
    {
        $conekta = Configuraciones::find(1);
        return view('administrador.configuraciones.index_conekta',[
            'conekta'   =>$conekta
        ]);
    }

    public function guardar_conekta(Request $request)
    {
        $this->validate($request,[
            'llave_publica' =>'required',
            'llave_privada' =>'required'
        ]);

        $conekta = Configuraciones::find(1);
        $conekta->pvk_conekta = $request->llave_privada;
        $conekta->puk_conekta = $request->llave_publica;
        $conekta->save();
        return response()->json([
            'swal'      =>  'true',
            'type'      =>  'success',
            'title'     =>  'Éxito',
            'text'      =>  'Se ha guardado las llaves',
        ], 200);
    }
}
