<?php

namespace App\Http\Controllers\Administrativo;

use App\Http\Controllers\Controller;
use App\Models\Categorias;
use App\Models\Servicios;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;

class CategoriasController extends Controller
{
    public function index()
    {
        return view('administrador.servicios.categorias.index');
    }

    public function datatable(Request $request)
    {
        $categorias = Categorias::all();
        return DataTables::of($categorias)
        ->addColumn('btn', function($categorias){
            return "<a href='".route('root.editar_categoria',$categorias->id)."' class='btn btn-success'>                       
                        Editar 
                    </a>
                    <form action='".route('root.eliminar_categoria',$categorias->id)."' class='form d-inline' method='POST' data-alert=true>
                        ".csrf_field()."
                        <button type='submit' class='btn btn-danger'>Eliminar</button>
                    </form>
                    ";
        })
        ->addColumn('porcentaje', function($categorias){
            return "%".number_format($categorias->costo_mano_obra,0);
        })
        ->addColumn('imagen', function($categorias){
            return "<img src='".asset('storage/categorias/'.$categorias->imagen)."' class='img-fluid'>";
        })
        ->rawColumns(['btn','imagen'])
        ->toJson();
    }

    public function crear()
    {
        return view('administrador.servicios.categorias.form',[
                'form_edit' =>false
            ]);
    }

    public function guardar(Request $request)
    {
        $this->validate($request,[
            'categoria'                 =>'required|string',
            'costo_mano_obra'           =>'required'
        ],
        [
            'categoria.required'        =>'El nombre de la categoría es requerido',
            'costo_mano_obra.required'  =>'El porcentaje de la mano de obra es requerido'
        ]);
        $nombre_imagen = "";
        if($request->has('imagen')){
            $imagen = $request->file('imagen');
            $nombre_imagen = uniqid().".".$imagen->clientExtension();
            $imagen->storeAs('categorias', $nombre_imagen, 'public');
        }
        Categorias::create([
            'nombre'            =>$request->categoria,
            'costo_mano_obra'   =>$request->costo_mano_obra,
            'imagen'            =>$nombre_imagen
        ]);

        return response()->json([
            'swal'          =>true,
            'type'          =>'success',
            'title'         =>'Éxito',
            'text'          =>'Categoría Guardada',
            'load'          =>route('root.categorias')
        ], 200);
    }

    public function editar($id_categoria)
    {
        $categoria = Categorias::find($id_categoria);
        return view('administrador.servicios.categorias.form',[
                'form_edit' =>true,
                'categoria' =>$categoria
            ]);
    }

    public function actualizar(Request $request, $id_categoria)
    {
        $this->validate($request,[
            'categoria'         =>'required',
            'costo_mano_obra'   =>'required'
        ],
        [
            'categoria.required'        =>'El nombre de la categoría es requerido',
            'costo_mano_obra.required'  =>'El porcentaje de mano de obra es requerido'
        ]);
        
        $categoria = Categorias::find($id_categoria);
        $nombre_imagen = $categoria->imagen;
        if($request->has('imagen')){
            $imagen = $request->file('imagen');
            $nombre_imagen = uniqid().".".$imagen->clientExtension();
            $imagen->storeAs('categorias', $nombre_imagen, 'public');
            Storage::delete('public/categorias/'.$categoria->imagen);
        } 
        $categoria->update(['nombre' => $request->categoria,'costo_mano_obra'=>$request->costo_mano_obra,'imagen'=>$nombre_imagen]);

        return response()->json([
            'swal'          =>true,
            'type'          =>'success',
            'title'         =>'Éxito',
            'text'          =>'Se ha actualizado la categoría',
            'load'          =>route('root.categorias'),            
            'modal_close'   =>true
        ], 200);
    }

    public function eliminar($id_categoria)
    {
        try {
            DB::beginTransaction();
            $categoria = Categorias::find($id_categoria);
            Storage::delete('public/categorias/'.$categoria->imagen);
            $servicios = Servicios::where('categoria_id',$categoria->id)->get();
            foreach ($servicios as $item) {
                $item->delete();
            }
            $categoria->delete();
            DB::commit();
            return response()->json([
                'swal'          =>true,
                'type'          =>'success',
                'title'         =>'Éxito',
                'text'          =>'Se ha eliminado la categoría',
                'dttable'       =>'#dt_categorias', 
            ], 200); 
        } catch (\Throwable $th) {
            DB::rollback();
            return response()->json([
                'swal'          =>true,
                'type'          =>'error',
                'title'         =>'Error',
                'text'          =>$th->getMessage(),
            ], 500);
        }
        
    }
}
