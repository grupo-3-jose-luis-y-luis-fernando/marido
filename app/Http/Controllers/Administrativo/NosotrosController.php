<?php

namespace App\Http\Controllers\Administrativo;

use App\Http\Controllers\Controller;
use App\Models\Nosotros;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class NosotrosController extends Controller
{
    public function index()
    {
        $nosotros = Nosotros::find(1);
        return view('administrador.web.nosotros.index',[
            'nosotros'  =>$nosotros
        ]);
    }

    public function actualizar(Request $request)
    {
       $this->validate($request,[
           'nosotros'   =>'required',
           'imagen'     =>'required|mimes:png,jpg,jpeg',
       ],[
           'required'       =>'El campo :attribute es requerido',
           'imagen.mimes'   =>'Solo se permiten imágenes con formato jpg y png'
       ]);
        $nombre_imagen = "";
        $nosotros = Nosotros::updateOrCreate(
            ['id'=>1],
            [
                'nosotros'  =>$request->nosotros,
                'imagen'    =>$request->imagen,
                'mision'    =>$request->mision,
                'valores'   =>$request->valores,
                'vision'    =>$request->vision
            ]
        );
        if($request->has("imagen")){
            $imagen = $request->file('imagen');
            $nombre_imagen = uniqid().".".$imagen->clientExtension();
            $imagen->storeAs('nosotros', $nombre_imagen, 'public');
            Storage::delete('public/nosotros/'.$nosotros->imagen);
            $nosotros->imagen = $nombre_imagen;
            $nosotros->save();
        }

        return response()->json([
            'swal'          =>true,
            'type'          =>'success',
            'title'         =>'Éxito',
            'text'          =>'Se ha actualizado la categoría',
            'reload'        =>true
        ], 200);
    }
}
