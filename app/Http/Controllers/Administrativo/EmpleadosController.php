<?php

namespace App\Http\Controllers\Administrativo;

use App\Http\Controllers\Controller;
use App\Models\DatosExtras;
use App\Models\User;
use App\Models\Roles;
use App\Models\Empleados;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Yajra\DataTables\Facades\DataTables;

class EmpleadosController extends Controller
{
    public function index()
    {
        return view('administrador.empleados.index');
    }

    public function datatable(Request $request)
    {
        $empleados = User::select("users.*")->whereHas('roles', function ($filter) {
            $filter->where('roles.name', '<>', 'cliente')->where('users.id','<>',1);
        });
        return DataTables::of($empleados)
        ->addColumn('name', function($empleados){
            return $empleados->name." ".$empleados->paterno." ".$empleados->materno;
        })
        ->addColumn('rol', function($empleados){
            return $empleados->getRoleNames()[0];
        })
        ->addColumn('btn', function($empleados){
            return "<a class='btn btn-success text-white' href='".route('root.editar_empleado', $empleados->id)."'>Editar</a>
                    <form class='form d-inline' method='POST' action='".route('root.eliminar_empleado', $empleados->id)."' data-alert=true>
                        ".csrf_field()."
                        <button class='btn btn-danger delete' data-url='".route('root.eliminar_empleado', $empleados->id)."'>Eliminar</button>
                    </form>";
        })
        ->rawColumns(['rol', 'btn'])
        ->toJson();
    }

    public function crear()
    {
        $roles = Roles::where('name','<>','cliente')->get();
        return view('administrador.empleados.form',[
            'form_edit'     =>false,
            'roles'         => $roles
        ]);
    }

    public function editar($id_empleado)
    { 
        $empleado = User::select('users.*','datos_extras.avatar')->leftJoin('datos_extras','datos_extras.user_id','users.id')->where('users.id',$id_empleado)->firstOrFail();
        $roles = Roles::where('name','<>','cliente')->get();

        return view('administrador.empleados.form',[
            'form_edit'     =>true,
            'empleado'      =>$empleado,
            'roles'         => $roles,
        ]);
    }

    public function guardar(Request $request)
    {
        $this->validate($request,[
            'nombre'        =>'required|string',
            'paterno'       =>'required|string',
            'materno'       =>'required|string',
            'email'         =>'required|string|email|unique:users,email',
            'password'      =>'required|string',
            'rol'           =>'required|string',
            'foto'          =>'image|mimes:png,jpeg,jpg'
        ],
        [
            'nombre.required'       =>'El nombre es requerido',
            'paterno.required'      =>'El apellido paterno es requerido',
            'materno.required'      =>'El apellido materno es requerido',
            'email.required'    	=>'El correo es requerido',
			'email.email'       	=>'Formato de correo no válido',
			'email.unique'			=>'El correo ya está en uso, intente con otro',
            'password.required'     =>'La contraseña con la que el empleado accedera al sistema es requerido',
            'rol.required'          =>'Seleccione el rol que tendrá el empleado',
            'foto.mimes'            =>'La imagen debe ser png o jpg'
        ]);

        $user = User::create([
            'name' 	    =>$request->nombre,
            'paterno'   =>$request->paterno,
            'materno'   =>$request->materno,
            'email'     =>$request->email,
            'password'  =>bcrypt($request->password)
        ]);
        $user->assignRole($request->rol);

        if($request->has('foto')){
            $imagen = $request->foto;
            $nombre_imagen = uniqid().".".$imagen->clientExtension();
            $imagen->storeAs('usuarios', $nombre_imagen, 'public');
            DatosExtras::create([
                'user_id'   =>$user->id,
                'avatar'    =>$nombre_imagen
            ]);
        }

        return response()->json([
            'swal'  =>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se ha registrado al empleado', 
            'load'  =>  route('root.empleados')
        ], 200);
    }

    public function actualizar(Request $request, $id_empleado)
    {
        $empleado = User::find($id_empleado);      //Obtener el id de la cuenta del empleado para ignorarla
        $this->validate($request,[
            'nombre'        =>'required|string',
            'paterno'       =>'required|string',
            'materno'       =>'required|string',
            'email'         =>['required', Rule::unique('users')->ignore($empleado->id)],
            'rol'           =>'required|string',
        ],
        [
            'nombre.required'       =>'Proporcione el nombre del empleado',
            'paterno.required'      =>'Proporcione el apellido paterno del empleado',
            'materno.required'      =>'Proporcione el apellido materno del empleado',
            'email.required'    	=>'Proporcione un correo para la cuenta del empleado',
			'email.email'       	=>'Formato de correo no válido',
			'email.unique'			=>'El correo ya está en uso, intente con otro',
            'rol.required'          =>'Indique el rol que desempeñara el empleado'
        ]);

        $empleado->name     =$request->nombre;
        $empleado->paterno  =$request->paterno;
        $empleado->materno  =$request->materno;
        $empleado->email    =$request->email;
        if(!empty($request->password))
        {
            $empleado->password = bcrypt($request->password);
        }
        $empleado->save();
        if($empleado->getRoleNames()[0]!=$request->rol){
            $empleado->removeRole($empleado->getRoleNames()[0]);
            $empleado->assignRole($request->rol);
        }

        if($request->has('foto')){
            $datos_extras = DatosExtras::where('user_id',$empleado->id)->first();
            $imagen = $request->foto;
            $nombre_imagen = uniqid().".".$imagen->clientExtension();
            $imagen->storeAs('usuarios', $nombre_imagen, 'public');

            if(!$datos_extras){
                DatosExtras::create([
                    'user_id'   =>$empleado->id,
                    'avatar'    =>$nombre_imagen
                ]);
            }else{
                $datos_extras->imagen = $nombre_imagen;
                $datos_extras->save();
            }

        }

        if($request->has('matricula')){
            $datos_extras = DatosExtras::where('user_id',$empleado->id)->first();
            $imagen = $request->matricula;
            $nombre_imagen = uniqid().".".$imagen->clientExtension();
            $imagen->storeAs('usuarios', $nombre_imagen, 'public');

            if(!$datos_extras){
                DatosExtras::create([
                    'user_id'   =>$empleado->id,
                    'matricula' =>$nombre_imagen
                ]);
            }else{
                $datos_extras->matricula = $nombre_imagen;
                $datos_extras->save();
            }
            
        }
        return response()->json([
            'swal'  =>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se han actualizado los datos del empleado',
            'load'  =>  route('root.empleados')
        ], 200);
    }

    public function eliminar($id_empleado)
    {
        $empleado = User::find($id_empleado);
        $empleado->delete();

        return response()->json([
            'swal'      =>  'true',
            'type'      =>  'success',
            'title'     =>  'Éxito',
            'text'      =>  'Se ha eliminado el empleado',
            'dttable'   =>  "#dt_empleados",
        ], 200);
    }
}
