<?php

namespace App\Http\Controllers\Administrativo;

use App\Http\Controllers\Controller;
use App\Models\Inventario;
use App\Models\Materiales;
use App\Models\MaterialesProveedores;
use App\Models\Proveedores;
use App\Models\UnidadesMedidas;
use Exception;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class MaterialesController extends Controller
{
    public function index()
    {
        return view("administrador.materiales.index");
    }

    public function datatable(Request $request)
    {
        $materiales = Materiales::select("materiales.*",'unidades_medidas.nombre as unidad')->join('unidades_medidas','unidades_medidas.id','materiales.unidad_id')->get();
        return DataTables::of($materiales)
        ->addColumn('btn', function($materiales){
            return '<form class="mb-3 form" action="'.route('root.material_proveedor', $materiales->id).'" method="POST">
                        '.csrf_field().'
                        <button type="submit" class="btn btn-primary">Agregar proveedor</button>
                    </form>
                    <form action="'.route('root.editar_material',$materiales->id).'" method="POST" class="form d-inline">
                        '.csrf_field().'
                        <button type="submit" class="btn btn-success">Editar</button>
                    </form>
                    <form action="'.route('root.eliminar_material').'" method="POST" class="form d-inline" data-alert=true>
                        '.csrf_field().'
                        <input type="hidden" name="material_id" value="'.$materiales->id.'">
                        <button type="submit" class="btn btn-danger">Eliminar</button>
                    </form>';
        })
        ->addColumn('proveedores', function($materiales){
            $proveedores = MaterialesProveedores::select('proveedores.nombre','materiales_proveedores.*')->join('proveedores','proveedores.id','materiales_proveedores.proveedor_id')->where('material_id',$materiales->id)->get();
            $tabla= "<table class='table'>
                        <thead>
                            <tr>
                                <th class='border-0'></th>
                                <th class='border-0'>Precio</th>
                                <th class='border-0'>Precio público</th>
                                <th class='border-0'></th>
                            </tr>
                        </thead>";
            foreach ($proveedores as $item) {
                $botones = "<form class='form d-inline' action='".route('root.editar_material_proveedor',['id_material'=>$materiales->id,'id_proveedor'=> $item->proveedor_id])."'>
                                ".csrf_field()."
                                <button type='submit' class='btn btn-success'>
                                    <i class='fa fa-pencil'></i>
                                </button>
                            </form>
                            <form class='form d-inline' action='".route('root.eliminar_material_proveedor',['id_material'=>$materiales->id,'id_proveedor'=> $item->proveedor_id])."' data-alert=true>
                                ".csrf_field()."
                                <button type='submit' class='btn btn-danger'>
                                    <i class='fa fa-trash'></i>
                                </button>
                            </form>";
                $tabla.="<tr>
                            <td>{$item->nombre}</td>
                            <td>$".number_format($item->precio,2,'.',',')."</td>
                            <td>$".number_format($item->precio_publico,2,'.',',')."</td>
                            <td>{$botones}</td>
                        </tr>";
            }
            $tabla.= "</table>";
            return $tabla;
        })
        ->rawColumns(['btn','proveedores'])
        ->toJson();
    }

    public function crear(Request $request)
    {
        $unidades = UnidadesMedidas::orderBy("nombre","ASC")->get();
        return response()->json([
            'modal' =>view('administrador.materiales.form',[
                'form_edit' =>false,
                'unidades'  =>$unidades
            ])->render()
        ]);
    }

    public function editar(Request $request, $id_material)
    {
        $material = Materiales::find($id_material);
        $unidades = UnidadesMedidas::orderBy("nombre","ASC")->get();
        return response()->json([
            'modal' =>view('administrador.materiales.form',[
                'form_edit' =>true,
                'unidades'  =>$unidades,
                'material'  =>$material
            ])->render()
        ]);
    }

    public function guardar(Request $request)
    {
        $this->validate($request,[
            'material'  =>'required',
            'unidad'    =>'required'
        ],[
            'material.required' =>'El nombre del material es requerido',
            'unidad.required'   =>'Debe seleccionar una unidad de medida',
        ]);

        $material = Materiales::create([
            'nombre'    =>$request->material,
            'unidad_id' =>$request->unidad
        ]);

        Inventario::create([
            'material_id'   =>$material->id,
            'cantidad'      =>0
        ]);
        return response()->json([
            'swal'      =>true,
            'title'     =>'Éxito',
            'text'      =>'Se ha creado el material',
            'type'      =>'success',
            'dttable'   =>'#dt_materiales',
            'modal_close'=>true,
        ]);
    }

    public function actualizar(Request $request)
    {
        $this->validate($request,[
            'material'  =>'required',
            'unidad'    =>'required',
        ],[
            'material.required' =>'El nombre del material es requerido',
            'unidad.required'   =>'La unidad de medida es requerido',
        ]);
        $material = Materiales::find($request->material_id);
        $material->nombre = $request->material;
        $material->unidad_id = $request->unidad;
        $material->save();
        return response()->json([
            'swal'      =>true,
            'title'     =>'Éxito',
            'text'      =>'Se ha actualizado el material',
            'type'      =>'success',
            'dttable'   =>'#dt_materiales',
            'modal_close'=>true,
        ]);
    }

    public function eliminar(Request $request)
    {
        $material = Materiales::find($request->material_id);
        $material->delete();
        return response()->json([
            'swal'      =>true,
            'title'     =>'Éxito',
            'text'      =>'Se ha eliminado el material',
            'type'      =>'success',
            'dttable'   =>'#dt_materiales', 
        ]);
    }

    public function proveedor($id_material)
    {
        $material = Materiales::find($id_material);
        $proveedores = Proveedores::all();
        return response()->json([
            "modal" =>view("administrador.materiales.proveedor.form",[
                'form_edit'     =>false,
                'proveedores'   =>$proveedores,
                'material'      =>$material
            ])->render()
        ]);
    }

    public function guardar_proveedor(Request $request, $id_material)
    {
        $this->validate($request,[
            'proveedor'     =>'required',
            'precio'        =>'required|not_in:0',
            'precio_publico'=>'required|not_in:0'
        ],[
            'proveedor.required'        =>'Debe seleccionar un proveedor',
            'precio.required'           =>'El precio del proveedor es requerido',
            'precio.not_in'             =>'El precio del proveedor no puede ser 0',
            'precio_publico.required'   =>'El precio para el público es requerido',
            'precio_publico.not_in'     =>'El precio para el público no puede ser 0'
        ]);
        try {
            $material_proveedor = MaterialesProveedores::where([
                ['material_id',$id_material],
                ['proveedor_id',$request->proveedor]
            ])->first();
            if($material_proveedor){
                throw new Exception("Este proveedor ya ha sido agregado al material, seleccione otro.", 1);
            }
    
            MaterialesProveedores::create([
                'material_id'   =>$id_material,
                'proveedor_id'  =>$request->proveedor,
                'precio'        =>$request->precio,
                'precio_publico'=>$request->precio_publico
            ]);
            return response()->json([
                'swal'      =>true,
                'title'     =>'Éxito',
                'text'      =>'Se ha agregado el proveedor',
                'type'      =>'success',
                'form_clear'=>true,
                'dttable'   =>"#dt_materiales"
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'message'   =>$th->getMessage()
            ],500);
        }
        
    }

    public function editar_proveedor($id_material, $id_proveedor)
    {
        $material = Materiales::find($id_material);
        $proveedor = Proveedores::find($id_proveedor);
        $material_proveedor = MaterialesProveedores::where([
            'material_id'   =>$id_material,
            'proveedor_id'  =>$id_proveedor
        ])->first();
        return response()->json([
            "modal" =>view("administrador.materiales.proveedor.form",[
                'form_edit'         =>true,
                'proveedor'         =>$proveedor,
                'material'          =>$material,
                'material_proveedor'=>$material_proveedor
            ])->render()
        ]);
    }

    public function actualizar_proveedor(Request $request, $id_material)
    {
        $this->validate($request,[
            'precio'            =>'required|not_in:0',
            'precio_publico'    =>'required|not_in:0'
        ],[
            'precio.required'           =>'El precio del proveedor es requerido',
            'precio_publico.required'   =>'El precio del público es requerido',
            'precio.not_in'             =>'El precio del proveedor no puede ser 0',
            'precio_publico.not_in'     =>'El precio para el público no puede ser 0'
        ]);

        $material_proveedor = MaterialesProveedores::where([
            'proveedor_id'  =>$request->proveedor,
            'material_id'   =>$id_material
        ])->first();
        $material_proveedor->precio = $request->precio;
        $material_proveedor->precio_publico = $request->precio_publico;
        $material_proveedor->save();
        return response()->json([
            'swal'      =>true,
            'title'     =>'Éxito',
            'text'      =>'Se ha actualizado al proveedor del material',
            'type'      =>'success',
            'dttable'   =>"#dt_materiales",
            'modal_close'=>true,
        ]);
    }

    public function eliminar_proveedor($id_material, $id_proveedor)
    {
        $material_proveedor = MaterialesProveedores::where([
            'material_id'=>$id_material,
            'proveedor_id'=>$id_proveedor
        ])->first();
        $material_proveedor->delete();
        return response()->json([
            'swal'      =>true,
            'title'     =>'Éxito',
            'text'      =>'Se ha eliminado al proveedor del material',
            'type'      =>'success',
            'dttable'   =>"#dt_materiales"
        ]);

    }

    public function buscar(Request $request)
    {
        $busqueda = $request->buscar;
        $materiales = Materiales::where('nombre','like', "%".$busqueda."%")->get();
        $html = "";
        foreach ($materiales as $item) {
            $html.="<div class='item' data-id='{$item->id}'>{$item->nombre}</div>";
        }
        return $html;
    }

    public function agregar(Request $request)
    {
       
        //SELECT * FROM `materiales` INNER JOIN materiales_proveedores ON materiales_proveedores.material_id = materiales.id WHERE materiales.id = 1 ORDER BY materiales_proveedores.precio_publico DESC

        $material = Materiales::select('materiales.nombre','materiales.id','materiales_proveedores.precio_publico','unidades_medidas.nombre as unidad')
        ->join('unidades_medidas','unidades_medidas.id','materiales.unidad_id')
        ->join('materiales_proveedores','materiales_proveedores.material_id','materiales.id')->where([
            ['materiales.id',$request->producto]
        ])->orderBy('materiales_proveedores.precio_publico','DESC')->first();
        $request->session()->push('materiales', ['producto'=>$request->producto,'cantidad'=>0]);
        print_r(session(['materiales' => 'id']));
            $precio = number_format($material->precio_publico,2,'.',',');
            $tr = " <tr id='producto-{$material->id}'>
                        <td>{$material->nombre}</td>
                        <td width='150'> 
                            <input type='number' class='form-control cantidad' min=0 name='cantidad[{$material->id}]'>
                        </td>
                        <td class='precio' precio='{$precio}'>$ {$precio} {$material->unidad}</td>
                        <td class='subtotal'>$0.0</td>
                        <td><button type='button' class='btn btn-danger eliminar-material'><i class='fa fa-trash'></i></button></td>
                    </tr>";
            return response()->json([
                'material'  =>$tr
            ]); 
    }
}
