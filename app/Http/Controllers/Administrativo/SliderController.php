<?php

namespace App\Http\Controllers\Administrativo;

use App\Http\Controllers\Controller;
use App\Models\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;

class SliderController extends Controller
{
    public function index()
    {
        return view('administrador.web.slider.index');
    }

    public function datatable(Request $request)
    {
        $slider = Slider::all();
        return DataTables::of($slider)
        ->addColumn('imagen', function($slider){
            return "<img src=".asset('storage/slider/'.$slider->imagen)." class='img-fluid'>";
        })
        ->addColumn('btn',function($slider){
            return "<a class='btn btn-success text-white' href='".route('root.editar_slider',$slider->id)."'>Editar</a> 
                    <form class='form d-inline' data-alert='true'>
                        ".csrf_field()."
                        <button type='submit' class='btn btn-danger'>Eliminar</button>
                    </form>";
        })
        ->rawColumns(['btn','imagen'])
        ->toJson();
    }

    public function crear()
    {
        return view('administrador.web.slider.form',[
            'form_edit' =>false
        ]);
    }
    
    public function guardar(Request $request)
    {
        $this->validate($request,[
            'imagen'    =>'required',
        ]);

        $imagen = $request->file('imagen');
        $archivo = uniqid().".".$imagen->clientExtension();
        $imagen->storeAs('slider', $archivo, 'public');
        Slider::create([
            'imagen'    =>$archivo,
            'url'       =>$request->url,
            'target'    =>$request->target
        ]); 
        return response()->json([
            'swal'  =>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se ha creado el slider',
            'load'  =>  route('root.slider')
        ], 200);
    }

    public function editar($slider_id)
    {
        $slider = Slider::find($slider_id);
        return view('administrador.web.slider.form',[
            'form_edit' =>true,
            'slider'    =>$slider
        ]);
    }

    public function actualizar(Request $request)
    {
        $slider = Slider::find($request->slider);
        if ($request->has('imagen')) {
            $imagen = $request->file('imagen');
            $archivo = uniqid().".".$imagen->clientExtension();
            $imagen->storeAs('slider', $archivo, 'public');
            Storage::delete('slider/'.$slider->imagen);
            $slider->imagen = $archivo;
        }
        $slider->url = $request->url;
        $slider->target = $request->target;
        $slider->save();
        return response()->json([
            'swal'  =>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se ha actualizado el slider',
            'load'  =>  route('root.slider')
        ], 200);
    }
}
