<?php

namespace App\Http\Controllers\Administrativo;

use App\Http\Controllers\Controller;
use App\Models\Galeria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;

class GaleriaController extends Controller
{
    public function index()
    {
        return view('administrador.web.galeria.index');
    }

    public function datatable(Request $request)
    {
        $galeria = Galeria::all();
        return DataTables::of($galeria)
        ->addColumn('imagen', function($galeria){
            return  "<img src='".asset('storage/galeria/'.$galeria->imagen)."' class='img-fluid'>";
        })
        ->addColumn('btn',function($galeria){
            return "<form class='form d-inline' action='".route('root.editar_galeria',$galeria->id)."'>
                        ".csrf_field()."
                        <button type='submit' class='btn btn-primary'>Editar</button>
                    </form>
                    <form class='form d-inline' action='".route('root.eliminar_galeria')."' data-alert='true'>
                        ".csrf_field()."
                        <input type='hidden' name='galeria' value='".$galeria->id."'>
                        <button type='submit' class='btn btn-danger'>Eliminar</button>
                    </form>";
        })
        ->rawColumns(['btn','imagen'])
        ->toJson();
    }

    public function crear()
    {
        return response()->json([
            'modal' =>view('administrador.web.galeria.form',[
                'form_edit' =>false
            ])->render()
        ]);
    }

    public function guardar(Request $request)
    {
        $this->validate($request,[
            'imagen'    =>'required',
        ]);

        $imagen = $request->file('imagen');
        $archivo = uniqid().".".$imagen->clientExtension();
        $imagen->storeAs('galeria', $archivo, 'public');
        Galeria::create([
            'imagen'    =>$archivo,
        ]); 
        return response()->json([
            'swal'      =>'true',
            'type'      =>'success',
            'title'     =>'Éxito',
            'text'      =>'Se ha subido la imagen',
            'dttable'   =>'#dt_galeria',
            'modal_close'=>true,
        ], 200);
    }

    public function editar($galeria_id)
    {
        $galeria = Galeria::find($galeria_id);
        return response()->json([
            'modal' =>view('administrador.web.galeria.form',[
                'form_edit' =>true,
                'galeria'   =>$galeria
            ])->render()
        ]);
    }

    public function actualizar(Request $request)
    {
        $galeria = Galeria::find($request->galeria);
        if ($request->has('imagen')) {
            $imagen = $request->file('imagen');
            $archivo = uniqid().".".$imagen->clientExtension();
            $imagen->storeAs('galeria', $archivo, 'public');
            Storage::delete('galeria/'.$galeria->imagen);
            $galeria->imagen = $archivo;
            $galeria->save();
        } 
        return response()->json([
            'swal'  =>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se ha actualizado la imagen',
            'dttable'   =>'#dt_galeria',
            'modal_close'=>true,
        ], 200);
    }

    public function eliminar(Request $request)
    {
        $galeria = Galeria::find($request->galeria);
        Storage::delete('galeria/'.$galeria->imagen);
        $galeria->delete();
        return response()->json([
            'swal'  =>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se ha eliminado la imagen',
            'dttable'   =>'#dt_galeria', 
        ], 200);
    }

}
