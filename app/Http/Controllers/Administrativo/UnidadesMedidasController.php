<?php

namespace App\Http\Controllers\Administrativo;

use App\Http\Controllers\Controller;
use App\Models\UnidadesMedidas;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class UnidadesMedidasController extends Controller
{
    public function index()
    {
        return view("administrador.materiales.unidades.index");
    }

    public function datatable(Request $request)
    {
        $unidades = UnidadesMedidas::all();
        return DataTables::of($unidades)
        ->addColumn('nombre', function($unidades){
            return $unidades->nombre."({$unidades->abreviatura})";
        })
        ->addColumn('btn', function($unidades){
            return ' 
                <form class="form d-inline"  method="POST" action="'.route('root.editar_unidad_medida', $unidades->id).'">
                    '.csrf_field().'
                    <button class="btn btn-success" type="submit">Editar</button>
                </form>
                <form class="form d-inline" data-alert=true method="POST" action="'.route('root.eliminar_unidad_medida').'">
                    '.csrf_field().'
                    <input type="hidden" value="'.$unidades->id.'" name="unidad">
                    <button class="btn btn-danger" type="submit">Eliminar</button>
                </form>
            ';
        })
        ->rawColumns(['btn'])
        ->toJson();
    }

    public function crear()
    {
        return response()->json([
            'modal' =>view("administrador.materiales.unidades.form",[
                'form_edit' =>false,
            ])->render()
        ]);
    }

    public function editar($id_unidad)
    {
        $unidad = UnidadesMedidas::find($id_unidad);
        return response()->json([
            'modal' =>view("administrador.materiales.unidades.form",[
                'form_edit' =>true,
                'unidad'    =>$unidad
            ])->render()
        ]);
    }

    public function guardar(Request $request)
    {
        $this->validate($request,[
            'unidad'        =>'required',
            'abreviatura'   =>'required'
        ],[
            'unidad.required'       =>'La unidad de medida es requerida',
            'abreviatura.required'  =>'La abreviatura es requerida'
        ]);
        UnidadesMedidas::create([
            'nombre'        =>$request->unidad,
            'abreviatura'   =>$request->abreviatura
        ]);

        return response()->json([
            'swal'          =>true,
            'title'         =>'Éxito',
            'text'          =>'Se ha creado la unidad de medida',
            'type'          =>'success',
            'modal_close'   =>true,
            'dttable'       =>'#dt_unidades_medidas'
        ]);
    }

    public function actualizar(Request $request)
    {
        $this->validate($request,[
            'unidad'        =>'required',
            'abreviatura'   =>'required'
        ],[
            'unidad.required'       =>'La unidad de medida es requerida',
            'abreviatura.required'  =>'La abreviatura es requerida'
        ]);
        $unidad = UnidadesMedidas::find($request->unidad_id);
        $unidad->nombre = $request->unidad;
        $unidad->abreviatura = $request->abreviatura;
        $unidad->save();
        return response()->json([
            'swal'          =>true,
            'title'         =>'Éxito',
            'text'          =>'Se ha actualizado la unidad de medida',
            'type'          =>'success',
            'modal_close'   =>true,
            'dttable'       =>'#dt_unidades_medidas'
        ]);
    }

    public function eliminar(Request $request)
    {
        $unidad = UnidadesMedidas::find($request->unidad);
        $unidad->delete();
        return response()->json([
            'swal'          =>true,
            'title'         =>'Éxito',
            'text'          =>'Se ha eliminado la unidad de medida',
            'type'          =>'success',
            'modal_close'   =>true,
            'dttable'       =>'#dt_unidades_medidas'
        ]);
    }
}
