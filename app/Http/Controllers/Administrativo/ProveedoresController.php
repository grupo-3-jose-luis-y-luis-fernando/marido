<?php

namespace App\Http\Controllers\Administrativo;

use App\Http\Controllers\Controller;
use App\Models\Proveedores;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Models\ProductosXProveedor;
use Yajra\DataTables\Facades\DataTables;

class ProveedoresController extends Controller
{
    public function index()
    {
        return view('administrador.proveedores.index');
    }

    public function datatable(Request $request)
    {
        $proveedores = Proveedores::all();
        return DataTables::of($proveedores)
        ->addColumn('btn', function($proveedores){
            return "<a class='btn btn-success text-white' href='".route('root.editar_proveedor', $proveedores->id)."'>Editar</a>
            <form class='form d-inline' action='".route('root.eliminar_proveedor', $proveedores->id)."' method='POST' data-alert=true>
                ".csrf_field()."
                <button class='btn btn-danger' type='submit'>Eliminar</button>
            </form>";
        })
        ->rawColumns(['btn'])
        ->toJson();
    }

    public function crear()
    {
        return view('administrador.proveedores.form',[
            'form_edit'     =>false
        ]);
    }

    public function editar($id_proveedor)
    {
        $proveedor = Proveedores::findOrFail($id_proveedor);
        return view('administrador.proveedores.form',[
            'form_edit'     =>true,
            'proveedor'     =>$proveedor
        ]);
    }

    public function guardar(Request $request)
    {
        $this->validate($request,[
            'nombre'    =>'required',
            'direccion' =>'required',
            'rfc'       =>'required',
            'telefono'  =>'required|unique:proveedores,telefono',
        ],
        [
            'nombre.required'       =>'El nombre del proveedor es requerido',
            'direccion.required'    =>'La dirección del proveedor es requerido',
            'rfc.required'          =>'El RFC del proveedor es requerido',
            'telefono.required'     =>'El teléfono de contacto es requerido',
            'telefono.unique'       =>'El télefono proporcionado ya está registrado con otro proveedor, intente con otro'
        ]);
 
        Proveedores::create([
            'nombre'    => $request->nombre,
            'rfc'       => $request->rfc,
            'direccion' => $request->direccion,
            'telefono'  => $request->telefono,
            'email'     => $request->email,
        ]);

        return response()->json([
            'swal'  =>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se ha registrado al proveedor',
            'load'  =>  route('root.proveedores')
        ], 200);
    }

    public function actualizar(Request $request, $id_proveedor)
    {
        $this->validate($request,[
            'nombre'    =>'required',
            'direccion' =>'required',
            'rfc'       =>'required',
            'telefono'  =>['required', Rule::unique('proveedores')->ignore($id_proveedor)],
        ],
        [
            'nombre.required'       =>'Proporcione el nombre del proveedor',
            'direccion.required'    =>'Proporcione la direcció del proveedor',
            'rfc.required'          =>'Proporcione el rfc del proveedor',
            'telefono.required'     =>'Proporcione un número de contacto',
            'telefono.unique'       =>'El télefono proporcionado ya está registrado con otro proveedor'
        ]);

        $proveedor = Proveedores::find($id_proveedor);
        $proveedor->nombre      = $request->nombre;
        $proveedor->rfc         = $request->rfc;
        $proveedor->direccion   = $request->direccion;
        $proveedor->telefono    = $request->telefono;
        $proveedor->email       = $request->email;
        $proveedor->save();

        return response()->json([
            'swal'  =>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se han actualizado los datos del proveedor',
            'load'  =>  route('root.proveedores')
        ], 200);
    }

    public function eliminar($id_proveedor)
    {
        $proveedor = Proveedores::find($id_proveedor); 
        $proveedor->delete(); 

        return response()->json([
            'swal'      =>  'true',
            'type'      =>  'success',
            'title'     =>  'Éxito',
            'text'      =>  'Se ha eliminado el proveedor',
            'dttable'   =>  "#dt_proveedores",
        ], 200);
    }
}
