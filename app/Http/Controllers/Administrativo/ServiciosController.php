<?php

namespace App\Http\Controllers\Administrativo;

use App\Http\Controllers\Controller; 
use App\Models\Servicios;
use App\Models\Categorias;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class ServiciosController extends Controller
{
    public function index()
    {
        return view('administrador.servicios.index');
    }

    public function datatable(Request $request)
    {
        $servicios = Servicios::select('servicios.id', 
        'servicios.nombre', 
        'categorias.nombre as categoria',
        'servicios.costo_agendado',
        'servicios.costo_urgente',
        'servicios.descripcion')
        ->join('categorias', 'servicios.categoria_id', 'categorias.id')->get();
        
        return DataTables::of($servicios)
        ->addColumn('nombre', function($servicios){
            return "<p>".$servicios->nombre."</p>";
        })
        ->addColumn('costo', function($servicios){
            return "<p> Normal: $".$servicios->costo_agendado."</p><p>Urgente: $".$servicios->costo_urgente."</p>";
        })
        ->addColumn('btn', function($servicios){
            return "
            <a href='".route('root.conceptos',$servicios->id)."' class='btn btn-primary'>Conceptos</a>
            <a class='btn btn-success text-white' href='".route('root.editar_servicio', $servicios->id)."'>Editar</a>
            <form class='form d-inline' action='".route('root.eliminar_servicio', $servicios->id)."' data-alert=true method='POST'>
                ".csrf_field()."
                <button class='btn btn-danger delete' data-url='".route('root.eliminar_servicio', $servicios->id)."'>Eliminar</button>                
            </form>";
        })
        ->rawColumns(['nombre', 'costo','btn'])
        ->toJson();
    }

    public function crear()
    {
        $categorias = Categorias::orderby('nombre')->get();
        return view('administrador.servicios.form',[
            'form_edit'     =>false,
            'categorias'    =>$categorias
        ]);
    }

    public function editar($id_servicio)
    {
        $servicio = Servicios::findOrFail($id_servicio);
        $categorias = Categorias::orderby('nombre')->get();
        return view('administrador.servicios.form',[
            'form_edit'     =>true,
            'categorias'    =>$categorias,
            'servicio'      =>$servicio
        ]);
    }

    public function guardar(Request $request)
    {
        $this->validate($request,[
            'nombre'        =>'required|string', 
            'categoria'     =>'required|numeric',
            'agendado'      =>'required|numeric|not_in:0',
            'urgente'       =>'required|numeric|not_in:0',
            'descripcion'   =>'required|string',
        ],
        [
            'nombre.required'        =>'El nombre común del servicio es requerido',
            'categoria.required'    =>'La categoría del servicio es requerido',
            'agendado.required'     =>'El precio del servicio programado es requerido',
            'agendado.numeric'      =>'El precio del servicio programado debe ser un número',
            'agendado.not_in'       =>'El precio del servicio programado no puede ser cero',
            'urgente.required'      =>'El precio del servicio urgente es requerido',
            'urgente.numeric'       =>'El precio del servicio urgente debe ser un número',
            'urgente.not_in'        =>'El precio del servicio urgente no puede ser cero',
            'descripcion.required'  =>'La descripción del servicio es requerido',
        ]);

        Servicios::create([
            'nombre'            =>$request->nombre, 
            'categoria_id'      =>$request->categoria,
            'costo_agendado'    =>$request->agendado,
            'costo_urgente'     =>$request->urgente,
            'descripcion'       =>$request->descripcion,
        ]);

        return response()->json([
            'swal'  =>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se ha registrado el servicio',
            'load'  =>  route('root.servicios')
        ], 200);
    }

    public function actualizar(Request $request, $id_servicio)
    {
        $this->validate($request,[
            'nombre'         =>'required|string', 
            'categoria'     =>'required|numeric',
            'agendado'      =>'required|string',
            'urgente'       =>'required|string',
            'descripcion'   =>'required|string',
        ],
        [
            'nombre.required'       =>'Indique el nombre para el servicio', 
            'categoria.required'    =>'Indique la categoria a la que corresponde el servicio',
            'agendado.required'     =>'Indique el precio del servicio',
            'urgente.required'      =>'Indique el precio para el servicio cuando es urgente',
            'descripcion.required'  =>'Proporcione una descripcion del servicio',
        ]);

        Servicios::where('id', $id_servicio)->update([
            'nombre'            =>$request->nombre, 
            'categoria_id'      =>$request->categoria,
            'costo_agendado'    =>$request->agendado,
            'costo_urgente'     =>$request->urgente,
            'descripcion'       =>$request->descripcion,
        ]);

        
        return response()->json([
            'swal'  =>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se han actualizado los datos del servicio',
            'load'  =>  route('root.servicios')
        ], 200);
    }

    public function eliminar($id_servicio)
    {
        Servicios::destroy($id_servicio);
        return response()->json([
            'swal'      =>  'true',
            'type'      =>  'success',
            'title'     =>  'Éxito',
            'text'      =>  'Se ha eliminado el servicio',
            'dttable'   =>  "#dt_servicios",
        ], 200);
    }
}
