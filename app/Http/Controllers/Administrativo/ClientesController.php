<?php

namespace App\Http\Controllers\Administrativo;

use App\Http\Controllers\Controller;
use App\Models\DatosExtras;
use App\Models\Direcciones;
use App\Models\User; 
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class ClientesController extends Controller
{
    public function index()
    {
        return view('administrador.clientes.index');
    }

    public function datatable(Request $request)
    {
        $clientes = User::select('users.*','datos_extras.rfc')
        ->leftJoin('datos_extras','datos_extras.user_id','=','users.id')
        ->whereHas('roles', function ($filter) {
            $filter->where('roles.name', '=', 'cliente');
        })
        ->get();

        return DataTables::of($clientes)
        ->addColumn('nombre', function($clientes){
            return $clientes->name." ".$clientes->paterno." ".$clientes->materno;
        })
        ->addColumn('direccion', function($clientes){
            $direcciones = Direcciones::where('user_id',$clientes->id)->get();
            $direccion = "";
            foreach ($direcciones as $item) {
                $direccion.= "<label><strong>".$item->alias.": </strong></label>";
                $direccion.=$item->calle.($item->num_ext?$item->num_ext:"").($item->num_int?$item->num_int:"").",".$item->localidad.",".$item->codigo_postal." ".$item->municipio.",".$item->estado.".";
                $direccion.="<div class=''w-100></div>";
            }
            return  $direccion;
        })
        ->addColumn('rfc', function($clientes){
            if(empty($clientes->rfc))
            {
                return "No disponible";
            }else{
                return $clientes->rfc;
            }
        })
        ->addColumn('btn', function($clientes){
            return "<a class='btn btn-success text-white' href='".route('root.editar_cliente', $clientes->id)."'>Editar</a>";
        })
        ->rawColumns(['btn','direccion'])
        ->toJson();
    }

    public function editar($id_cliente)
    {
        $cliente = User::findOrFail($id_cliente);
        return view('administrador.clientes.form',[
            'cliente'       =>$cliente
        ]);
    }

    public function actualizar(Request $request, $id_cliente)
    {

        $rules = [
            'nombre'    =>'required',
            'paterno'   =>'required',
            'materno'   =>'required', 
        ];
        if($request->filled('rfc') ){
            $rules['rfc'] = 'required';
        }
        $messages = [
            'nombre.required'       =>'Proporcione el nombre del cliente',
            'paterno.required'      =>'Proporcione el apellido paterno del cliente',
            'materno.required'      =>'Proporcione el apellido materno del cliente', 
            'rfc.required'          =>'Proporcione el rfc del cliente',
        ];
        $this->validate($request,$rules,$messages);
        $cliente = User::find($id_cliente);
        $cliente->name      = $request->nombre;
        $cliente->paterno   = $request->paterno;
        $cliente->materno   = $request->materno;
        DatosExtras::updateOrCreate(
            ['user_id'=>$cliente->id],
            ['user_id'=>$cliente->id,'rfc'=>$request->rfc]
        );  
        $cliente->save(); 

        return response()->json([
            'swal'  =>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se han actualizado los datos del cliente',
            'load'  =>  route('root.clientes')
        ], 200);
    }


    public function eliminar($id_cliente)
    {
        $cliente = User::find($id_cliente);
        $cliente->delete();

        return response()->json([
            'swal'      =>  'true',
            'type'      =>  'success',
            'title'     =>  'Éxito',
            'text'      =>  'Se ha eliminado el cliente',
            'dttable'   =>  "#dt_clientes",
        ], 200);
    }
}
