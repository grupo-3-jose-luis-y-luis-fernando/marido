<?php

namespace App\Http\Controllers\Administrativo;

use App\Http\Controllers\Controller;
use App\Models\Servicios;
use App\Models\ServiciosConceptos;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class ServiciosConceptosController extends Controller
{
    public function index($id_servicio)
    {
        $servicio = Servicios::find($id_servicio);
        return view('administrador.servicios.conceptos.index',[
            'servicio'  =>$servicio
        ]);
    }

    public function datatable(Request $request,$id_servicio)
    {
        $concepto = ServiciosConceptos::where('servicio_id',$id_servicio)->get();
        
        return DataTables::of($concepto)
        ->addColumn('nombre', function($concepto){
            return "<p>".$concepto->nombre."</p>";
        })
        ->addColumn('costo', function($concepto){
            return "<p> Normal: $".$concepto->costo_agendado."</p><p>Urgente: $".$concepto->costo_urgente."</p>";
        })
        ->addColumn('btn', function($concepto){
            return " 
            <a class='btn btn-success text-white' href='".route('root.editar_concepto', ['id_servicio'=>$concepto->servicio_id,'id_concepto'=>$concepto->id])."'>Editar</a>
            <form class='form d-inline' action='".route('root.eliminar_concepto', ['id_servicio'=>$concepto->servicio_id,'id_concepto'=>$concepto->id])."' data-alert=true method='POST'>
                ".csrf_field()."
                <button class='btn btn-danger'>Eliminar</button>                
            </form>";
        })
        ->rawColumns(['nombre', 'costo','btn'])
        ->toJson();
    }

    public function crear($id_servicio)
    {
        return view('administrador.servicios.conceptos.form',[
            'form_edit'     =>false, 
            'servicio_id'   =>$id_servicio
        ]);
    }

    public function editar($id_servicio,$id_concepto)
    {
        $concepto = ServiciosConceptos::findOrFail($id_concepto); 
        return view('administrador.servicios.conceptos.form',[
            'form_edit'     =>true, 
            'concepto'      =>$concepto,
            'servicio_id'   =>$id_servicio
        ]);
    }

    public function guardar(Request $request, $servicio_id)
    {
        $request->merge([
            'agendado' => str_replace(",", "", $request->agendado),
            'urgente' => str_replace(",", "", $request->urgente)
        ]);

        $this->validate($request,[
            'clave'         =>'required',
            'nombre'        =>'required|string',  
            'agendado'      =>'required|numeric|not_in:0',
            'urgente'       =>'required|numeric|not_in:0', 
        ],
        [
            'clave.required'        =>'La clave del concepto es requerido', 
            'nombre.required'        =>'El nombre común del servicio es requerido', 
            'agendado.required'     =>'El precio del servicio programado es requerido',
            'agendado.numeric'      =>'El precio del servicio programado debe ser un número',
            'agendado.not_in'       =>'El precio del servicio programado no puede ser cero',
            'urgente.required'      =>'El precio del servicio urgente es requerido',
            'urgente.numeric'       =>'El precio del servicio urgente debe ser un número',
            'urgente.not_in'        =>'El precio del servicio urgente no puede ser cero', 
        ]);

        ServiciosConceptos::create([
            'clave'             =>$request->clave,
            'servicio_id'       =>$servicio_id,
            'nombre'            =>$request->nombre,  
            'costo_agendado'    =>$request->agendado,
            'costo_urgente'     =>$request->urgente, 
        ]);

        return response()->json([
            'swal'  =>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se ha registrado el concepto',
            'load'  =>  route('root.conceptos',$servicio_id)
        ], 200);
    }

    public function actualizar(Request $request, $servicio_id,$concepto_id)
    {
        $request->merge([
            'agendado' => str_replace(",", "", $request->agendado),
            'urgente' => str_replace(",", "", $request->urgente)
        ]);
        $this->validate($request,[
            'nombre'         =>'required|string',            
            'agendado'      =>'required|string',
            'urgente'       =>'required|string', 
            'clave'         =>'required'
        ],
        [
            'clave.required'    =>'La clave del concepto es requerido',
            'nombre.required'       =>'Indique el nombre para el servicio', 
            'agendado.required'     =>'Indique el precio del servicio',
            'urgente.required'      =>'Indique el precio para el servicio cuando es urgente', 
        ]);

        ServiciosConceptos::where('id', $concepto_id)->update([
            'nombre'            =>$request->nombre,  
            'costo_agendado'    =>$request->agendado,
            'costo_urgente'     =>$request->urgente,
            'clave'             =>$request->clave
        ]);

        
        return response()->json([
            'swal'  =>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se han actualizado los datos del concepto',
            'load'  =>  route('root.conceptos',$servicio_id)
        ], 200);
    } 

    public function eliminar($id_servicio,$concepto_id)
    {
        ServiciosConceptos::destroy($concepto_id);
        return response()->json([
            'swal'      =>  'true',
            'type'      =>  'success',
            'title'     =>  'Éxito',
            'text'      =>  'Se ha eliminado el concepto',
            'dttable'   =>  "#dt_conceptos",
        ], 200);
    }
}
