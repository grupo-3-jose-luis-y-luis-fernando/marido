<?php

namespace App\Http\Controllers;

use App\Models\Categorias;
use App\Models\Galeria;
use App\Models\Nosotros;
use App\Models\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WebController extends Controller
{
    public function index()
    {
        $slider = Slider::all();
        $categorias = Categorias::all();
        $nosotros = Nosotros::find(1);
        $galeria = Galeria::orderBy(DB::raw("RAND()"))->take(8)->get();
        return view('web.index',[
            'slider'        =>$slider,
            'categorias'    =>$categorias,
            'nosotros'      =>$nosotros,
            'galeria'       =>$galeria
        ]);
    }
}
