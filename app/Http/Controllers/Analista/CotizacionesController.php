<?php

namespace App\Http\Controllers\Analista;

use App\Http\Controllers\Controller;
use App\Models\Cotizaciones;
use App\Models\DireccionSolicitud;
use App\Models\MaterialesConcepto;
use App\Models\MaterialesSolicitud;
use App\Models\Servicios;
use App\Models\SolicitudConceptos;
use App\Models\SolicitudServicio;
use Carbon\Carbon;
use Elibyy\TCPDF\Facades\TCPDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;  
class CotizacionesController extends Controller
{
    public function index($solicitud_id)
    {
        $conceptos = SolicitudConceptos::select('solicitud_conceptos.id as solicitud_concepto',
        'solicitud_conceptos.aceptado',
        'servicios_conceptos.id as concepto_id',
        'servicios_conceptos.servicio_id',
        'servicios_conceptos.nombre',
        'categorias.costo_mano_obra'
        )
        ->join('servicios_conceptos','servicios_conceptos.id','solicitud_conceptos.concepto_id')
        ->join('servicios','servicios.id','servicios_conceptos.servicio_id')
        ->join('categorias','categorias.id','servicios.categoria_id')
        ->where('solicitud_id',$solicitud_id)
        ->orderBy('aceptado','DESC')
        ->get(); 
        foreach ($conceptos as $concepto) {
            $materiales = MaterialesConcepto::select('materiales.id','materiales.nombre','materiales_concepto.cantidad',DB::raw('MAX(materiales_proveedores.precio_publico) as costo'),'inventario.cantidad as disponibilidad')
            ->join('materiales','materiales.id','materiales_concepto.material_id')
            ->leftJoin('materiales_proveedores','materiales_proveedores.material_id','materiales.id')
            ->leftJoin('inventario','inventario.material_id','materiales.id')
            ->where('solicitud_concepto_id',$concepto->solicitud_concepto)
            ->groupBy('materiales.id')
            ->get(); 
            $concepto['materiales'] = $materiales;
        }
        return view('analista.cotizaciones.index',[
            'solicitud_id'  =>$solicitud_id,
            'conceptos'     =>$conceptos
        ]);
    }

    public function guardar(Request $request, $solicitud_id)
    {               
        
        $nombre_archivo = uniqid().".pdf";       
        $archivo = $this->previsualizacion($request,$solicitud_id); 
        rename(public_path('storage/solicitudes/cotizaciones/'.$archivo->original['nombre']),public_path('storage/solicitudes/cotizaciones/'.$nombre_archivo));

        $total = 0;
        $subtotal = 0;
        foreach ($request->importes as $item => $value){
            $importe = str_replace(',','',$value);
            $subtotal = $subtotal + $importe; 
        
        }
        $descuento = $request->descuento;
        $total_descuento = $descuento > 0 ? ($subtotal * ($descuento / 100)) : 0;
        $subtotal_con_descuento = $total_descuento + $subtotal;
        $iva = $subtotal_con_descuento * 0.16;
        $total = $iva+$subtotal_con_descuento;
        Cotizaciones::create([
            'archivo'       =>$nombre_archivo,
            'costo'         =>$total,
            'tipo'          =>2,
            'estatus'       =>1,
            'solicitud_id'  =>$solicitud_id
        ]);
        return response()->json([
            'swal'  =>  'true',
            'type'  =>  'success',
            'title' =>  'Éxito',
            'text'  =>  'Se ha enviado la cotización',
            'load'  =>  route('analista.solicitudes')
        ], 200);
    }

    public function previsualizacion(Request $request, $solicitud_id)
    { 
        
        $this->validate($request,[
            'descuento' =>'required|numeric'
        ]);
        $nota = $solicitud_id;
        if($solicitud_id < 1000){
            $nota = "S".str_pad($solicitud_id, 4, "0", STR_PAD_LEFT); 
        }
       
        $fecha = Carbon::now()->locale('es');     
        $cliente = SolicitudServicio::select("users.name","users.paterno","users.materno")->join('users','users.id','solicitud_servicio.cliente_id')->where('solicitud_servicio.id',$solicitud_id)->first();
        $direccion = DireccionSolicitud::select('estado','municipio','localidad','codigo_postal','calle','num_int','num_ext','referencias')->where('solicitud_id',$solicitud_id)->first(); 
        $conceptos = SolicitudConceptos::select('solicitud_conceptos.id as solicitud_concepto',
        'solicitud_conceptos.aceptado',
        'servicios_conceptos.id as concepto_id',
        'servicios_conceptos.servicio_id',
        'servicios_conceptos.nombre',
        'categorias.costo_mano_obra',
        'servicios_conceptos.clave'
        )
        ->join('servicios_conceptos','servicios_conceptos.id','solicitud_conceptos.concepto_id')
        ->join('servicios','servicios.id','servicios_conceptos.servicio_id')
        ->join('categorias','categorias.id','servicios.categoria_id')
        ->where([
            ['solicitud_id',$solicitud_id],
            ['aceptado',1]
        ])
        ->orderBy('aceptado','DESC')
        ->get(); 

        $html = view('analista.cotizaciones.pdf',[
            'fecha'     =>$fecha,
            'nota'      =>$nota,
            'cliente'   =>$cliente,
            'direccion' =>$direccion,
            'conceptos' =>$conceptos,
            'importes'  =>$request->importes,
            'subtotal'  =>0,
            'descuento' =>$request->descuento
        ])->render(); 
        TCPDF::setHeaderCallback(function($pdf){
            //$file, $x=null, $y=null, $w=0, $h=0, $type='', $link='', $align='', $resize=false, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false, $alt=false, $altimgs=array()
            $pdf->Image(public_path('images/logo-header.jpeg'), 20, 10,25, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
            // Set font
        
            $pdf->setFont('poppinsmedium','',12);
            $pdf->SetTextColor(146,146,146);
            $pdf->Cell(0, 0, 'MANTENIMIENTO Y REPARACIÓN INDUSTRIAL Y DOMÉSTICA', 0, 1, 'C', 0, '', 0);
            
            $pdf->setFont('poppinsmedium','',10);
            $pdf->SetTextColor(130,140,194);
            $pdf->SetXY(50,16);
            $pdf->Cell(0, 0, 'Calle Polluelos 221 Int. 1, Fracc. Las Águilas, C.P. 29049 Tuxtla Gutiérrez, Chiapas',  0, 1, 'C', 0, '', 0);
            
        });

        TCPDF::setFooterCallback(function($pdf){
            //$file, $x=null, $y=null, $w=0, $h=0, $type='', $link='', $align='', $resize=false, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false, $alt=false, $altimgs=array()
            $pdf->setFont('poppinsmedium','',10);
            $pdf->SetTextColor(146,146,146);
            $pdf->SetY(-20);
            $pdf->Cell(0, 0, 'Teléfono oficina 961 656 7839', 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $pdf->SetY(-15);
            $pdf->Cell(0, 0, 'Celular 961 314 1526', 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $pdf->SetTextColor(35,41,103);
            $pdf->SetY(-10);
            $pdf->Cell(0, 0, route('index'), 0, false, 'C', 0, '', 0, false, 'T', 'M');
            // Set font 
            
        });
        
        TCPDF::SetTitle('Cotización N.'.$nota);
        TCPDF::SetCreator("Grupo Cybac");
        TCPDF::SetAuthor('MARIDO');
        TCPDF::SetSubject('Previsualización de Cotización');
        TCPDF::SetKeywords('cotización, previo, MARIDO, CYBAC');
        TCPDF::SetFont('poppinsmedium');         
        TCPDF::SetMargins(10,40,10); 
        TCPDF::SetAutoPageBreak(TRUE,25);
        TCPDF::AddPage('','LETTER'); 
        TCPDF::writeHTML($html, true, false, true, true, '');
        TCPDF::Output(public_path('storage/solicitudes/cotizaciones/'.$nota.'.pdf'),'F');

        return response()->json([
            'pdf'       =>asset('storage/solicitudes/cotizaciones/'.$nota.'.pdf'),
            'nombre'    =>$nota.".pdf"
        ]);
    }
}
