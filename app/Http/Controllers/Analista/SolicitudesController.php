<?php

namespace App\Http\Controllers\Analista;

use App\Http\Controllers\Controller;
use App\Models\Configuraciones;
use App\Models\Cotizaciones;
use App\Models\GaleriaSolicitud; 
use Illuminate\Http\Request;
use App\Models\SolicitudServicio;
use App\Models\User;
use App\Models\ServicioTecnico;
use App\Models\SolicitudConceptos;
use App\Models\SolicitudesResidentes;
use App\Notifications\FireBaseNotificacion;
use Carbon\Carbon; 
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Yajra\DataTables\Facades\DataTables;

class SolicitudesController extends Controller
{
    private $firebase = "";
    public function __construct() {
        $this->firebase = new FireBaseNotificacion();
    }
    public function index()
    {
        return view('analista.solicitudes.index');
    }

    public function datatable(Request $request)
    {
        $solicitudes = SolicitudServicio::select("solicitud_servicio.*","users.name",'servicios.nombre as servicio','direccion_solicitud.municipio','direccion_solicitud.estado','direccion_solicitud.localidad','direccion_solicitud.codigo_postal','direccion_solicitud.num_int','direccion_solicitud.num_ext','direccion_solicitud.calle','direccion_solicitud.id as direccion_id')
        ->join('users','users.id','=','solicitud_servicio.cliente_id')
        ->join('servicios','servicios.id','=','solicitud_servicio.servicio_id')
        ->leftJoin('direccion_solicitud','direccion_solicitud.solicitud_id','=','solicitud_servicio.id')
        ->orderBy('solicitud_servicio.estatus','ASC')
        ->orderBy('solicitud_servicio.tipo','DESC')
        ->get();
        return DataTables::of($solicitudes)
        ->addColumn('folio', function($solicitudes){
            if($solicitudes->id < 1000){
                $nota = "S".str_pad($solicitudes->id, 4, "0", STR_PAD_LEFT); 
            }
            return $nota;
        })
        ->addColumn('cliente',function($solicitudes){
            return $solicitudes->name;
        })
        ->addColumn('tipo',function($solicitudes){
            return $solicitudes->tipo == 1 ?"Programado":"Urgente"; 
        })
        ->addColumn('horario', function($solicitudes){
            return $solicitudes->tipo == 1 ? $solicitudes->horario_programado : $solicitudes->created_at;
        })
        ->addColumn('estatus', function($solicitudes){
            $estatus = [1 =>"<span class='badge badge-warning'>Pendiente</span>", 2=>"<span class='badge badge-primary'>Asignado</span>", 3=>"<span class='badge badge-success'>Finalizado</span>",4=>"<span class='badge badge-danger'>Cancelada</span>",5=>"<span class='badge badge-info'>Trabajando</span>",6=>"<span class='badge badge-warning'>Reasignado</span>"];
            
            return $estatus[$solicitudes->estatus];
        })
        ->addColumn('direccion', function($solicitudes){
            if(!$solicitudes->codigo_postal){
                return "";
            }
            return $solicitudes->calle.($solicitudes->num_ext?$solicitudes->num_ext:"").($solicitudes->num_int?$solicitudes->num_int:"").", ".$solicitudes->localidad.", ".$solicitudes->codigo_postal." ".$solicitudes->municipio.", ".$solicitudes->estado.".";
        })
        ->addColumn('btn', function($solicitudes){
            if($solicitudes->estatus == 1 || $solicitudes->estatus == 6){
                $button = "";
                if($solicitudes->estatus == 6){
                    $button = "<form class='form' action='".route('analista.cancelar')."' method='POST' data-alert='true'>
                                ".csrf_field()."
                                <input type='hidden' name='solicitud_id' value='".$solicitudes->id."'>
                                <button class='btn btn-danger' type='submit'>Cancelar</button>
                                </form>";
                }
                return "<a class='btn btn-success' href='".route('analista.editar_solicitud',$solicitudes->id)."'>Asignar solicitud</a> ".$button;
            }else{
                $conceptos = SolicitudConceptos::where('solicitud_id',$solicitudes->id)->get();
                $acciones = "<a class='btn btn-primary text-white' href='".route('analista.ver_solicitud',$solicitudes->id)."'>Ver solicitud</a> ";
                $cotizacion = Cotizaciones::where([
                    ['tipo', 2],
                    ['solicitud_id',$solicitudes->id]
                ])->first();
                if($conceptos->count() > 0 && !$cotizacion){
                    $acciones.= "<a class='btn btn-success text-white' href='".route('analista.cotizacion',$solicitudes->id)."'>Agregar cotización </a>";
                }
                return $acciones;
                
            }
            
        })
        ->rawColumns(['btn','estatus'])
        ->toJson();
    }
    
    public function ver($solicitud_id)
    {
        $solicitud = SolicitudServicio::select(
            'solicitud_servicio.*',
            'servicios.nombre', 
            'users.name',
            'users.paterno',
            'users.materno',
            DB::raw('(SELECT CONCAT(users.name," ",IF(ISNULL(users.paterno),"",users.paterno)," ",IF(ISNULL(users.materno),"",users.materno)) FROM servicio_tecnico INNER JOIN users on users.id = servicio_tecnico.tecnico_id WHERE solicitud_servicio_id = solicitud_servicio.id ) as tecnico'),
            DB::raw('(SELECT CONCAT(users.name," ",IF(ISNULL(users.paterno),"",users.paterno)," ",IF(ISNULL(users.materno),"",users.materno)) FROM solicitudes_residentes INNER JOIN users on users.id = solicitudes_residentes.residente_id WHERE solicitud_id = solicitud_servicio.id ) as residente'),
            'direccion_solicitud.estado',
            'direccion_solicitud.municipio',
            'direccion_solicitud.localidad',
            'direccion_solicitud.codigo_postal',
            'direccion_solicitud.calle',
            'direccion_solicitud.num_int',
            'direccion_solicitud.num_ext',
            'direccion_solicitud.referencias'
        )
        ->join('users','users.id','=','solicitud_servicio.cliente_id')
        ->join('servicios','servicios.id','=','solicitud_servicio.servicio_id')
        ->join('direccion_solicitud','direccion_solicitud.solicitud_id','=','solicitud_servicio.id')
        ->where('solicitud_servicio.id',$solicitud_id)
        ->first();
        $cotizacion = Cotizaciones::where([
            ['solicitud_id',$solicitud_id],
            ['tipo',2]
        ])->first();
        $estimacion = Cotizaciones::where([
            ['solicitud_id',$solicitud_id],
            ['tipo',1]
        ])->first();

        $galeria = GaleriaSolicitud::where('solicitud_id',$solicitud_id)->get();

        return view('analista.solicitudes.ver',[
            'solicitud' =>$solicitud,
            'estimacion'=>$estimacion,
            'cotizacion'=>$cotizacion,
            'galeria'   =>$galeria
        ]);
    }

    public function editar($solicitud_id)
    {
        $tecnicos = User::select("users.*", DB::raw("(SELECT COUNT(*) FROM servicio_tecnico WHERE servicio_tecnico.tecnico_id = users.id AND servicio_tecnico.estatus = 1) as servicios "))->role('tecnico')
        ->get();
        $residentes = User::select("users.*")->role('residente')
        ->get();
        $solicitud = SolicitudServicio::select('solicitud_servicio.*','servicios.nombre','users.name','users.paterno','users.materno')->join('servicios','servicios.id','=','solicitud_servicio.servicio_id')->join('users', 'users.id','=','solicitud_servicio.cliente_id')->where('solicitud_servicio.id',$solicitud_id)->first();
        $galeria = GaleriaSolicitud::where('solicitud_id',$solicitud->id)->get();
        return view('analista.solicitudes.form',[
            'tecnicos'  =>$tecnicos,
            'solicitud' =>$solicitud,
            'residentes'=>$residentes,
            'galeria'   =>$galeria
        ]);
    }

    public function asignar(Request $request)
    {
        $rules = [
            'tecnico'       =>'required',
            'fecha_llegada' =>'required', 
            'residente'     =>'required'
        ];
        if($request->hasFile('estimacion') || $request->filled('costo_estimado')){
            $rules['estimacion'] = 'mimes:pdf|required';
            $rules['costo_estimado'] = 'required';
        }
        $this->validate($request,$rules,[
            'tecnico.required'      =>'Por favor, seleccione un técnico',
            'fecha_llegada.required'=>'La fecha y hora de llegada del técnico es requerida',
            'residente.required'    =>'Por favor, seleccione un residente',
            'costo_estimado.required'=>'El costo estimado es requerido',
            'estimacion.required'   =>'La ficha de la estimación es requerida'
        ]);
        try {
            DB::beginTransaction();

            $solicitud = SolicitudServicio::find($request->solicitud);
            $horario_programado = Carbon::parse($solicitud->tipo == 2 ? $solicitud->created_at : $solicitud->horario_programado );
            $horario_llegada = Carbon::parse($request->fecha_llegada);     
            if(!$horario_programado->isSameDay($horario_llegada) ){
                if($horario_programado->gt($horario_llegada) ){
                    throw new Exception("La fecha de llegada debe ser el mismo día de la solicitud o posterior a esta.", 500);
                }                
            }       
           
            $solicitud->estatus = 2;
            $solicitud->save();
            $archivo = "";

            if($request->hasFile('estimacion')){                
                $estimacion = $request->file('estimacion');
                $archivo = uniqid().".".$estimacion->clientExtension();
                $estimacion->storeAs('solicitudes/estimaciones', $archivo, 'public');
                $costo =str_replace(",", "", $request->costo_estimado);
                Cotizaciones::create([
                    'estatus'   =>1,
                    'tipo'      =>1,
                    'solicitud_id'=>$solicitud->id,
                    'archivo'   =>$archivo,
                    'costo'     =>$costo
                ]);
            }
            
            SolicitudesResidentes::create([
                'residente_id'      =>$request->residente,
                'solicitud_id'      =>$request->solicitud,
                'estatus'           =>1,
                'horario_llegada'   =>$request->fecha_llegada,
            ]);

            ServicioTecnico::create([
                'tecnico_id'            =>$request->tecnico,
                'solicitud_servicio_id' =>$request->solicitud,
                'horario_estimado'      =>$request->fecha_llegada,
                'estatus'               =>1
            ]);
            $tecnico = User::find($request->tecnico);
            DB::commit();
            $this->firebase->enviar_ahora("Asignación", "Se te ha asignado una solicitud",$tecnico->token_notificacion, $broadcast = false);
            return response()->json([
                'swal'  =>  'true',
                'type'  =>  'success',
                'title' =>  'Éxito',
                'text'  =>  'Se ha asignado el servicio',
                'load'  =>  route('analista.solicitudes')
            ], 200);

        } catch (\Throwable $th) {
            DB::rollback();
            return response()->json([
                'status'=>'false',
                'type'  =>'error',
                'title' =>'Error',
                'message'  =>$th->getMessage()
            ],500);
        }
        
    }

    public function asignados(Request $request)
    {
        $servicios = ServicioTecnico::where([
            ['servicio_tecnico.tecnico_id',$request->tecnico],
            ['servicio_tecnico.estatus',1]
        ])->get();
        
        return response()->json($servicios, 200);
    }

    public function cancelar(Request $request)
    {
        $conekta = Configuraciones::select("pvk_conekta as llave_privada","puk_conekta as llave_publica")->where("id",1)->first();
        $solicitud = SolicitudServicio::find($request->solicitud_id);
        $response =  Http::withBasicAuth($conekta->llave_privada,'')->withHeaders([
            'accept'    =>'application/vnd.conekta-v2.0.0+json'
        ])->post('https://api.conekta.io/orders/'.$solicitud->visita_conekta.'/capture');
       if($response['payment_status'] == "paid"){
            $solicitud->estatus = 4;
            $solicitud->save();
            return response()->json([
                'swal'  =>  'true',
                'type'  =>  'success',
                'title' =>  'Éxito',
                'text'  =>  'Se ha cancelado el servicio',
                'dttable'  =>'#dt_solicitudes'
            ], 200);
       }

    }
}
