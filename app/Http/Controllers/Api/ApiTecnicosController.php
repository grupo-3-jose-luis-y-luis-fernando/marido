<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Cotizaciones;
use App\Models\Evidencias;
use App\Models\GaleriaSolicitud;
use App\Models\Materiales;
use App\Models\MaterialesConcepto;
use App\Models\MensajeTecnico; 
use App\Models\ServicioTecnico;
use App\Models\SolicitudConceptos;
use App\Models\SolicitudesResidentes;
use App\Models\SolicitudServicio;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ApiTecnicosController extends Controller
{
    /**
     * Lista las solicitudes asignadas a un técnico
     */
    public function solicitudes(Request $request)
    {
        $solicitudes = ServicioTecnico::select('solicitud_servicio.id as solicitud_id',
        DB::raw("(SELECT CONCAT(users.name,IF(ISNULL(users.paterno),'',CONCAT(' ',users.paterno)), IF(ISNULL(users.materno),'',CONCAT(' ',users.materno))) FROM users WHERE users.id = solicitud_servicio.cliente_id) as cliente"),
        DB::raw("IF(solicitud_servicio.tipo = 1, 'Programado','Urgente') as tipo "),
        "solicitud_servicio.descripcion",
        "solicitud_servicio.horario_programado",
        'servicio_tecnico.horario_estimado',
        'servicio_tecnico.horario_llegada',
        'servicio_tecnico.horario_inicio',
        'servicio_tecnico.horario_fin',
        DB::raw("(CASE WHEN solicitud_servicio.estatus = 1 THEN 'Pendiente' WHEN solicitud_servicio.estatus = 2 THEN 'Asignado' WHEN solicitud_servicio.estatus = 5 THEN 'Trabajando' ELSE 'Finalizado' END) as estatus"),
        'servicios.nombre as servicio',
        'direccion_solicitud.estado',
        'direccion_solicitud.municipio',
        'direccion_solicitud.localidad',
        'direccion_solicitud.calle',
        'direccion_solicitud.num_ext',
        'direccion_solicitud.num_int',
        'direccion_solicitud.codigo_postal',
        'direccion_solicitud.referencias',
        'direccion_solicitud.latitud',
        'direccion_solicitud.longitud',
        'solicitud_servicio.visita_conekta',
        'solicitud_servicio.orden_conekta',
        'solicitud_servicio.estatus_orden')
        ->join('solicitud_servicio','solicitud_servicio.id','=','servicio_tecnico.solicitud_servicio_id')
        ->join('servicios','servicios.id','=','solicitud_servicio.servicio_id')
        ->leftJoin('direccion_solicitud','direccion_solicitud.solicitud_id','=','solicitud_servicio.id')
        ->orderBy('solicitud_servicio.estatus','ASC')
        ->orderBy('solicitud_servicio.tipo','DESC')
        ->where([
            ['servicio_tecnico.tecnico_id', $request->user()->id]
        ])->get();
        foreach ($solicitudes as $item) {
            $materiales = MaterialesConcepto::join('solicitud_conceptos','solicitud_conceptos.id','materiales_concepto.solicitud_concepto_id')
            ->where('solicitud_id',$item->solicitud_id)->first();
            $cotizacion = Cotizaciones::where([
                ['solicitud_id',$item->solicitud_id],
                ['tipo',2]
            ])->first();
            $galeria = GaleriaSolicitud::select(DB::raw("CONCAT('".asset('storage/solicitudes/')."/',imagen) as imagen"))->where('solicitud_id',$item->solicitud_id)->get();
            $item['galeria'] = $galeria;
            $item['paso'] = 0; //Muestra la vista para la actualización de la hora de llegada del técnico
            if(!is_null($item->horario_llegada)){ //Muestra a la vista de alta de servicios y materiales
                $item['paso'] = 1;
            }
            if($materiales){ 
                $evidencias = Evidencias::where("solicitud_id",$item->solicitud_id)->first();
                if($evidencias){
                    $item['paso'] = 3;
                    $item['evidencias_aceptadas'] = false;
                    $solicitud_residente = SolicitudesResidentes::where('solicitud_id',$item->solicitud_id)->first();
                    if($solicitud_residente->estatus == 2){
                        $item['evidencias_aceptadas'] = true;
                    }
                }else{
                    $item['paso'] = 2; //Muestra la vista con el botón de inicio de trabajo
                    if($cotizacion && $cotizacion->estatus == 2){
                        $item['bloqueo'] = false;
                    }else{
                        $item['bloqueo'] = true;
                    } 
                }
            }
        }
        return response()->json($solicitudes, 200);
    }

    /**
     * Agrega los materiales que requiere de la solicitud para enviarse al analista
     */
    public function agregar_materiales(Request $request)
    {
        $this->validate($request,[
            'solicitud_id'          =>'required',
            'conceptos'             =>'required'
        ],[
            'solicitud_id.required'         =>'El ID de la solicitud es requerido',
            'conceptos.required'            =>'Los conceptos son requeridos',
        ]);
        try {
            DB::beginTransaction();
            $solicitud = SolicitudServicio::find($request->solicitud_id);
            $solicitud_tecnico = ServicioTecnico::where("solicitud_servicio_id",$request->solicitud_id)->first();
            if($request->user()->getRoleNames()[0]!="tecnico"){
                throw new Exception("El usuario no es un técnico.", 500);
            }
            if(!$solicitud){
                throw new Exception("La solicitud no existe, verifique la petición.", 500);
            }else{
                if(!$solicitud_tecnico){
                    throw new Exception("La solicitud aún no ha sido asignada a un técnico", 500);
                }else{
                    if(is_null($solicitud_tecnico->horario_llegada))
                        throw new Exception("El técnico no ha marcado una hora de llegada", 500); 
                }
            }
            $conceptos = SolicitudConceptos::where('solicitud_id',$solicitud->id)->get();
            foreach ($conceptos as $item) {
                $materiales = MaterialesConcepto::where('solicitud_concepto_id',$item->id)->get();
                foreach ($materiales as $itemm) {
                    $itemm->delete();
                }
                $item->delete();
            }

            foreach ($request->conceptos as $concepto) {
                $solicitud_concepto = SolicitudConceptos::create([
                    "concepto_id"   =>$concepto['concepto_id'],
                    "solicitud_id"  =>$request->solicitud_id,
                    "aceptado"      =>$concepto['aceptado']
                ]);
                if($concepto['aceptado'] == 1){
                    if(!array_key_exists('materiales',$concepto)){
                        throw new Exception("Faltan materiales en un concepto aceptado", 500);
                    }
                }
                if(array_key_exists("materiales",$concepto)){
                    foreach ($concepto['materiales'] as $material) {
                        MaterialesConcepto::create([
                            'solicitud_concepto_id' =>$solicitud_concepto->id,
                            'material_id'           =>$material['material_id'],
                            'cantidad'              =>$material['cantidad']
                        ]);
                    }
                }
            }
            DB::commit();
            return response()->json([
                'mensaje'   =>'Solicitud de materiales enviados'
            ]);
            
        } catch (\Throwable $th) {
            DB::rollBack();
            return response()->json([
                'mensaje'   =>$th->getMessage()
            ],500);
        } 
    }

    public function mensaje(Request $request)
    {
        $this->validate($request,[
            'solicitud_id'  =>'required',
            'mensaje'       =>'required'
        ]);
        try {
            $solicitud = SolicitudServicio::find($request->solicitud_id);
            $solicitud_tecnico = ServicioTecnico::where("solicitud_servicio_id",$request->solicitud_id)->first();
            if($request->user()->getRoleNames()[0]!="tecnico"){
                throw new Exception("El usuario no es un técnico.", 500);
            }
            if(!$solicitud){
                throw new Exception("La solicitud no existe, verifique la petición.", 500);
            }else{
                if(!$solicitud_tecnico){
                    throw new Exception("La solicitud aún no ha sido asignada a un técnico", 500);
                }
            }
            $mensaje = MensajeTecnico::where('solicitud_id',$request->solicitud_id)->first();
            if($mensaje){
                if($mensaje->tecnico_id == $request->user()->id){
                    $mensaje->mensaje = $request->mensaje;
                    $mensaje->save();
                    return response()->json([
                        'mensaje'   =>'Mensaje actualizado.'
                    ]);
                }else{
                    throw new Exception("No es posible actualizar el mensaje, porque este usuario no está asignado a esta solicitud.", 500);
                }
            }else{
                MensajeTecnico::create([
                    'tecnico_id'    =>$request->user()->id,
                    'mensaje'       =>$request->mensaje,
                    'solicitud_id'  =>$request->solicitud_id
                ]);
                return response()->json([
                    'mensaje'   =>'Mensaje guardado'
                ], 200);
            }
        } catch (\Throwable $th) {
            return response()->json([
                'mensaje'   =>$th->getMessage()
            ],500);
        }
    }

    public function actualizar_llegada(Request $request)
    {
        $this->validate($request,[
            'solicitud_id'  =>'required',
            'fecha_llegada' =>'required'
        ],[
            'solicitud_id.required'     =>'El ID de la solicitud es requerida.',
            'fecha_llegada.required'    =>'La fecha de llegada es requerida.'
        ]);
        try {
            if($request->user()->getRoleNames()[0]!="tecnico"){
                throw new Exception("El usuario no es un técnico.", 500);
            }
            $servicio = ServicioTecnico::select('servicio_tecnico.*')
            ->where('solicitud_servicio_id',$request->solicitud_id)
            ->join('solicitud_servicio','solicitud_servicio.id','servicio_tecnico.solicitud_servicio_id')
            ->first();
            if($servicio){
                if(!$request->visita){
                    $solicitud_residente = SolicitudesResidentes::where('solicitud_id',$request->solicitud_id)->first();
                    $solicitud_residente->delete();
                    $servicio->delete();
                    $solicitud = SolicitudServicio::find($request->solicitud_id);
                    $solicitud->estatus = 6;
                    $solicitud->save();
                    return response()->json([
                        'mensaje'   =>'La solicitud ha cambiado de estatus a reasignada'
                    ],200);
                }else{
                    $horario_programado =  Carbon::parse($servicio->horario_estimado);
                    $horario_llegada = Carbon::parse($request->fecha_llegada);
                    if(!$horario_programado->isSameDay($horario_llegada)){ //Determina si la llegada del técnico es el día indicado por el cliente.
                        throw new Exception("La fecha de llegada debe ser el día indicado por la solicitud", 500);
                    }
                    $servicio->horario_llegada = $request->fecha_llegada;
                    $servicio->save();
                    return response()->json([
                        'mensaje'   =>'Se ha actualizado la fecha de llegada'
                    ],200);
                }
            }else{
                throw new Exception("La solicitud aún no ha sido asignada a un técnico", 500);
            }
        } catch (\Throwable $th) {
            return response()->json([
                'mensaje'   =>$th->getMessage()
            ],500);
        }
    }

    public function marcar_inicio(Request $request)
    {
        $this->validate($request,[
            'solicitud_id'  =>'required',
            'fecha_inicio'  =>'required'
        ],[
            'solicitud_id.required' =>'El ID de la solicitud es requerida.',
            'fecha_inicio.required' =>'La fecha de inicio es requerida.'
        ]);
        try {
            if($request->user()->getRoleNames()[0]!="tecnico"){
                throw new Exception("El usuario no es un técnico.", 500);
            }
            $servicio = ServicioTecnico::where('solicitud_servicio_id',$request->solicitud_id)->first();
            
            if($servicio){
                $cotizacion = Cotizaciones::where([
                    ['tipo', 2],
                    ['solicitud_id',$servicio->solicitud_servicio_id]
                ])->first();
                if(!$cotizacion){
                    throw new Exception("La solicitud aún no cuenta con cotización", 500);
                }else{
                    if($cotizacion->estatus != 2 ){
                        throw new Exception("La cotización aún no ha sido aceptada por el cliente", 500);
                    }
                }
                $servicio->horario_inicio = $request->fecha_inicio;    
                $servicio->estatus = 3; //El técnico se encuentra trabajando            
                $servicio->save();
                $solicitud = SolicitudServicio::find($servicio->solicitud_servicio_id);
                $solicitud->estatus = 5;
                $solicitud->save();
                return response()->json([
                    'mensaje'   =>'Se ha actualizado la fecha de inicio'
                ],200);
            }else{
                throw new Exception("La solicitud aún no ha sido asignada a un técnico", 500);
            }
            
        } catch (\Throwable $th) {
            return response()->json([
                'mensaje'   =>$th->getMessage()
            ],500);
        }
    }

    public function perfil(Request $request)
    {
        $tecnico = User::select(DB::raw('CONCAT(users.name,IF(ISNULL(users.paterno),"",CONCAT(" ",users.paterno)), IF(ISNULL(users.materno),"",CONCAT(" ",users.materno))) as nombre'),'avatar as foto','matricula',DB::raw('IF(ISNULL(telefono),"",telefono) as telefono'))->leftJoin('datos_extras','datos_extras.user_id','users.id')->where('users.id',$request->tecnico_id)->first();
        $tecnico['foto'] = $tecnico->foto == "user.png" || is_null($tecnico->foto) ? asset("images/user.png") :  asset("storage/usuarios/".$tecnico->foto);
        $tecnico['matricula'] = is_null($tecnico->matricula) ? "" :  asset("storage/usuarios/".$tecnico->matricula);
        return response()->json($tecnico);
    }

    public function enviar_evidencias(Request $request)
    {
        $this->validate($request,[
            'solicitud_id'  =>'required',
            'evidencias'    =>'required'
        ],[
            'solicitud_id.required'     =>'La solicitud es requerida',
            'evidencias.required'       =>'Las evidencias son requeridas'
        ]);
        try {
            if($request->user()->getRoleNames()[0]!="tecnico"){
                throw new Exception("El usuario no es un técnico.", 500);
            }
            $solicitud = SolicitudServicio::find($request->solicitud_id);
            if(!$solicitud){
                throw new Exception("No existe la solicitud", 500);
            }else{
                if($solicitud->estatus != 5){
                    throw new Exception("La solicitud no se encuentra en un estatus de trabajando, revise el proceso.", 500);
                }
                $solicitud_tecnico = ServicioTecnico::where('solicitud_servicio_id',$solicitud->id)->first();
                if($solicitud_tecnico->tecnico_id != $request->user()->id){
                    throw new Exception("Esta solicitud no está asignada a este técnico", 1); 
                } 
                $evidencias = Evidencias::where('solicitud_id',$request->solicitud_id)->get();
                foreach ($evidencias as $item) {
                    Storage::delete("public/solicitudes/evidencias/".$item->imagen);
                    $item->delete();
                }
                foreach ($request->evidencias as $item => $value) {
                    $imageName = "";
                    $imageName = uniqid().'.png'; 
                    Storage::put("public/solicitudes/evidencias/".$imageName, base64_decode($value));
                    Evidencias::create([
                        'solicitud_id'  =>$request->solicitud_id,
                        'imagen'        =>$imageName,
                    ]);
                } 
                return response()->json([
                    'mensaje'   =>'Evidencias guardadas'
                ],200);
            }
        } catch (\Throwable $th) {
            return response()->json([
                'mensaje'   =>$th->getMessage()
            ],500);
        }
    }

    public function evidencias(Request $request)
    {
        $this->validate($request,[
            'solicitud_id'  =>'required'
        ],[
            'solicitud.required'    =>'La solicitud es requerida'
        ]);
        try {
            $evidencias = Evidencias::select(DB::raw("CONCAT('".asset('storage/solicitudes/evidencias/')."/',imagen) as imagen"))->where("solicitud_id",$request->solicitud_id)->get();
            return $evidencias;
        } catch (\Throwable $th) {
            return response()->json([
                'mensaje'   =>$th->getMessage()
            ],500);
        }
    } 

    public function materiales()
    {
        $materiales = Materiales::select('materiales.id as material_id','materiales.nombre','unidades_medidas.nombre as unidad')
        ->join('unidades_medidas','unidades_medidas.id','materiales.unidad_id')
        ->get();

        return $materiales;
    }
}
