<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Servicios;
use App\Models\Categorias;
use App\Models\Cotizaciones;
use App\Models\Direcciones;
use App\Models\DireccionSolicitud;
use App\Models\Evidencias;
use App\Models\GaleriaSolicitud;
use App\Models\MaterialesSolicitud;
use App\Models\ServiciosConceptos;
use Illuminate\Validation\Rule;
use App\Models\SolicitudServicio;
use App\Models\ServicioTecnico;
use App\Models\SolicitudesResidentes;
use App\Models\User;
use App\Notifications\FireBaseNotificacion;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ApiServiciosController extends Controller
{ 
    protected $FirebaseNotificacion;
    
    public function __construct() {
        $this->FireBaseNotificacion = new FireBaseNotificacion();
    }
    
    public function index(Request $request)
    { 
        $servicios = Servicios::select('id','nombre','categoria_id as categoria','costo_agendado','costo_urgente','descripcion')->get();
        return response()->json($servicios, 200);
    }

    public function categorias()
    {
        $categorias = Categorias::all();
        return response()->json($categorias, 200);
    }

    public function crear_solicitud(Request $request)
    {
        $this->validate($request,[
            'servicio'              =>'required', 
            'tipo'                  =>'required',
            'horario_programado'    =>Rule::requiredIf($request->tipo == 1),
            'direccion_id'          =>'required',
            'visita_conekta'        =>'required'
        ],[
            'servicio.required'             =>'El servicio es requerido',
            'tipo.required'                 =>'El tipo de servicio es requerido',
            'horario_programado.required'   =>'El horario para la cita es requerido',
            'direccion_id.required'         =>'El ID de la dirección es requerido',
            'visita_conekta.required'       =>'El ID de la orden de visita Conekta es requerido'
        ]);
        
        
        $usuario = $request->user(); //Se obtiene los datos del usuario que ha realizado la petición
        $direccion_usuario = Direcciones::find($request->direccion_id); //Se obtiene la dirección actual del usuario
        if(!$direccion_usuario){
            return response()->json([
                'status'    =>false,
                'message'   =>'El ID de esta dirección no existe'
            ],500);
        }
        try {
            DB::beginTransaction();
            $solicitud = SolicitudServicio::create([
                'servicio_id'       =>$request->servicio,
                'cliente_id'        =>$usuario->id,
                'tipo'              =>$request->tipo,
                'horario_programado'=>$request->horario_programado, 
                'estatus'           =>1,
                'descripcion'       =>$request->descripcion,
                'visita_conekta'    =>$request->visita_conekta
            ]); //Se crea la solicitud
           
            if($request->filled('galeria')){
                foreach ($request->galeria as $item) {
                    $imageName = "";
                    $imageName = uniqid().'.png'; 
                    Storage::put("public/solicitudes/".$imageName, base64_decode($item['imagen']));
                    GaleriaSolicitud::create([
                        'imagen'        =>$imageName,
                        'solicitud_id'  =>$solicitud->id
                    ]);
                }
            }

            DireccionSolicitud::create([
                'solicitud_id'  =>$solicitud->id,
                'estado'        =>$direccion_usuario->estado,
                'municipio'     =>$direccion_usuario->municipio,
                'localidad'     =>$direccion_usuario->localidad,
                'codigo_postal' =>$direccion_usuario->codigo_postal,
                'calle'         =>$direccion_usuario->calle,
                'num_int'       =>$direccion_usuario->num_int,
                'num_ext'       =>$direccion_usuario->num_ext,
                'latitud'       =>$direccion_usuario->latitud,
                'longitud'      =>$direccion_usuario->longitud,
                'referencias'   =>$direccion_usuario->referencias
            ]); //Se guarda la dirección de la solicitud para evitar el cambio de dirección en caso que el usuario cambie su dirección
            DB::commit();
            return response()->json(['status'=>true,'message'=>'Solicitud creada'], 200);
        } catch (\Throwable $th) {
            DB::rollback();
            return response()->json(['status'=>false,'message'=>$th->getMessage().$th->getLine()], 500);
        }
            
    }

    public function tecnicos(Request $request)
    {
        $tecnicos = User::select('users.id as tecnico_id','users.name as nombre','users.paterno','users.materno','users.email')->whereHas('roles', function ($filter) {
            $filter->where('roles.name', '=', 'tecnico');
        })
        ->get();
        return response()->json($tecnicos, 200);
    }
 
    public function solicitudes_residente(Request $request)
    {
        $solicitudes = SolicitudServicio::select("solicitud_servicio.id as solicitud_id",
        DB::raw("(SELECT CONCAT(users.name,IF(ISNULL(users.paterno),'',CONCAT(' ',users.paterno)), IF(ISNULL(users.materno),'',CONCAT(' ',users.materno))) FROM users WHERE users.id = solicitud_servicio.cliente_id) as cliente"),
        DB::raw("IF(solicitud_servicio.tipo = 1, 'Programado','Urgente') as tipo "),
        "solicitud_servicio.descripcion",
        "solicitud_servicio.horario_programado",
        'servicio_tecnico.horario_llegada',
        'servicio_tecnico.horario_inicio',
        'servicio_tecnico.horario_fin',
        DB::raw("(CASE WHEN solicitud_servicio.estatus = 1 THEN 'Pendiente' WHEN solicitud_servicio.estatus = 2 THEN 'Asignado' WHEN solicitud_servicio.estatus = 5 AND solicitudes_residentes.estatus = 1 THEN 'Trabajando' ELSE 'Finalizado' END) as estatus"),
        'servicios.nombre as servicio', 
        'direccion_solicitud.estado',
        'direccion_solicitud.municipio',
        'direccion_solicitud.localidad',
        'direccion_solicitud.calle',
        'direccion_solicitud.num_ext',
        'direccion_solicitud.num_int',
        'direccion_solicitud.codigo_postal',
        'direccion_solicitud.referencias',
        'direccion_solicitud.latitud',
        'direccion_solicitud.longitud',
        'solicitud_servicio.visita_conekta',
        'solicitud_servicio.orden_conekta',) 
        ->join('servicios','servicios.id','=','solicitud_servicio.servicio_id')
        ->leftJoin('direccion_solicitud','direccion_solicitud.solicitud_id','=','solicitud_servicio.id')
        ->leftJoin('servicio_tecnico','servicio_tecnico.solicitud_servicio_id','=','solicitud_servicio.id')
        ->leftJoin('solicitudes_residentes','solicitudes_residentes.solicitud_id','=','solicitud_servicio.id')
        ->orderBy('solicitud_servicio.estatus','ASC')
        ->orderBy('solicitud_servicio.tipo','DESC')
        ->where([
            ['solicitudes_residentes.residente_id', $request->user()->id]
        ])
        ->get(); 
        foreach ($solicitudes as $item) {
            $galeria = GaleriaSolicitud::select(DB::raw("CONCAT('".asset('storage/solicitudes/')."/',imagen) as imagen"))->where('solicitud_id',$item->solicitud_id)->get();
            $item['galeria'] = $galeria;
        }
        return response()->json($solicitudes, 200);
    }

    public function asignar(Request $request)
    {
        $this->validate($request,[
            'tecnico_id'    =>'required',
            'solicitud_id'  =>'required',
            'fecha_llegada' =>'required', 
        ],[
            'tecnico.required'      =>'El ID del técnico es necesario',
            'solicitud_id.required' =>'El ID de la solicitud es requerida',
            'fecha_llegada.required'=>'La fecha y hora de llegada del técnico es requerida',
        ]);
        
        $solicitud = SolicitudServicio::find($request->solicitud_id);
        if($solicitud->estatus == 2){
            return response()->json([
                'status'    =>false,
                'mensaje'   =>'Esta solicitud ya fue asignada anteriormente'
            ], 500);
        }
        $solicitud->estatus = 2;
        $solicitud->save();
        ServicioTecnico::create([
            'tecnico_id'            =>$request->tecnico_id,
            'solicitud_servicio_id' =>$request->solicitud_id,
            'horario_llegada'       =>$request->fecha_llegada,
            'estatus'               =>1
        ]);
        return response()->json([
            'status'    =>true,
            'message'   =>'Se ha asignado la solicitud al técnico',
        ], 200);
    }

    public function lista_eventos(Request $request)
    {
        $this->validate($request,[
            'fecha' =>'required'
        ],[
            'fecha.required'    =>'La fecha es requerida'
        ]);
        $eventos = ServicioTecnico::select('id as id_evento',DB::raw('IF(ISNULL(horario_llegada),horario_estimado,(IF( ISNULL(horario_inicio), horario_llegada, horario_inicio ))) as horario_evento'))
        ->havingRaw('DATE(horario_evento) = ?', [$request->fecha])
        ->where('estatus',1)
        ->get();
        return response()->json($eventos,200);
    }

    public function cancelar(Request $request)
    {
        try {
            $solicitud = SolicitudServicio::find($request->solicitud_id);
            if($solicitud){
                if($solicitud->cliente_id != $request->user()->id){
                    throw new \Exception("Esta solicitud no le pertenece a este cliente", 500); 
                }
                if($solicitud->estatus == 1){
                    $solicitud->estatus = 4; //Estatus de una solicitud cancelada
                    $solicitud->save();
                    return response()->json([
                        'mensaje'   =>'Solicitud cancelada'
                    ], 200);
                }else{
                    $estatus = [2=>"ya fue asignada a un técnico",3=>"ya fue finalizada", 4=>"ya se canceló anteriormente"];
                    throw new \Exception("Esta solicitud ya no puede ser cancelada porque ".$estatus[$solicitud->estatus], 500); 
                }
            }else{
                throw new \Exception("No existe esta solicitud",500); 
            }
            
        } catch (\Throwable $th) {
            return response()->json([
                'mensaje'   =>$th->getMessage()
            ],500);
        }
        
    }

    public function finalizar(Request $request)
    {
        $this->validate($request, [
            'solicitud_id' =>'required'
        ],[
            'solicitud_id.required' =>'El ID de la solicitud es requerida'
        ]);
        try {
            $rol = $request->user()->getRoleNames()[0]; 
            if($rol != "tecnico" && $rol != "residente"){
                throw new Exception("El usuario no es un técnico o residente.", 500);
            }
            $solicitud_residente = SolicitudesResidentes::where('solicitud_id',$request->solicitud_id)->first();
            DB::beginTransaction();  
            if ($rol == "tecnico") {
                if($solicitud_residente->estatus == 1){
                    throw new Exception("El residente aún no ha marcado como finalizado", 500);
                }
                $solicitud_tecnico = ServicioTecnico::where('solicitud_servicio_id',$request->solicitud_id)->first();
                if(!$solicitud_tecnico){
                    throw new Exception("Esta solicitud no existe o no se encuentra asignada a un técnico", 500);
                }         
                     
                $solicitud = SolicitudServicio::find($request->solicitud_id);
                $solicitud_tecnico->horario_fin = Carbon::now();
                $solicitud_tecnico->estatus = 2; //Finalizado para el técnico
                $solicitud_tecnico->save();
                $solicitud->estatus = 3; //Finalizado total
                $solicitud->estatus_orden = "paid";
                $solicitud->save();
                DB::commit();
                return response()->json([
                    'mensaje'   =>'Proceso finalizado'
                ],200);
            }else{
                $evidencia = Evidencias::where('solicitud_id',$request->solicitud_id)->get();
                if(!$evidencia){
                    throw new Exception("El técnico aún no ha subido evidencias",500);
                }
                if($solicitud_residente->residente_id != $request->user()->id){
                    throw new Exception("Esta solicitud no está asignada a este residente", 500);
                }
                if($solicitud_residente->estatus == 2){
                    throw new Exception("El proceso de la solicitud ya fue finalizada para el residente anteriormente",500);
                }
                $solicitud_residente->horario_fin = Carbon::now();
                $solicitud_residente->estatus = 2; //Finalizado para el residente
                $solicitud_residente->save();
                DB::commit();
                return response()->json([
                    'mensaje'   =>'Proceso finalizado para el residente'
                ],200);
            }
        } catch (\Throwable $th) {
            DB::rollback();
            return response()->json([
                'mensaje'   =>$th->getMessage()
            ],500);
        } 
    }

    public function cancelar_evidencias(Request $request)
    {
        $this->validate($request,[
            'solicitud_id'  =>'required'
        ],[
            'solicitud_id.required' =>'El id de la solicitud es requerida'
        ]);
        $evidencias = Evidencias::where('solicitud_id',$request->solicitud_id)->get();
        foreach ($evidencias as $item) {
            $item->delete();
        }
        return response()->json([
            'mensaje'   =>'Evidencias rechazadas'
        ],200);
    }

    public function conceptos(Request $request)
    {
        $servicios = ServiciosConceptos::where('servicio_id',$request->servicio_id)->get();
        return response()->json($servicios, 200);
    }
}
