<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Configuraciones;
use App\Models\Cotizaciones;
use App\Models\DatosExtras;
use Illuminate\Http\Request;
use App\Models\Direcciones;
use App\Models\GaleriaSolicitud;
use App\Models\MensajeTecnico;
use App\Models\Servicios;
use App\Models\ServicioTecnico;
use App\Models\SolicitudServicio;
use Exception;
use Illuminate\Support\Facades\DB;

class ApiClientesController extends Controller
{
    public function agregar_direccion(Request $request)
    {
        $this->validate($request,[
            'estado'        =>'required',
            'municipio'     =>'required',
            'localidad'     =>'required',
            'codigo_postal' =>'required',
            'calle'         =>'required',
            'latitud'       =>'required',
            'longitud'      =>'required',
            'alias'         =>'required'
        ],[
            'estado.required'       =>'El estado es necesario',
            'municipio.required'    =>'El municipio es requerido',
            'localidad.required'    =>'La localidad es requerida',
            'codigo_postal.required'=>'El código postal es requerido',
            'calle.required'        =>'La calle es requerida',
            'latitud.required'      =>'La latitud es requerida',
            'longitud.required'     =>'La longitud es requerida',
            'alias.required'        =>'El alias es requerido'
        ]);
        $usuario = $request->user();
        $direcciones = Direcciones::where('user_id', $usuario->id)->get();
        if($direcciones->count() < 5){
            $alias = $direcciones->where('alias',$request->alias)->first();
            if($alias){
                return response()->json(["message"=>"The given data was invalid.",
                    "errors"=> ["alias"=>["Este alias ya existe entre sus direcciones, intente con otro."]]
                ], 422);
            }
            Direcciones::create([
                'user_id'       =>$usuario->id,
                'estado'        =>$request->estado,
                'municipio'     =>$request->municipio,
                'localidad'     =>$request->localidad,
                'codigo_postal' =>$request->codigo_postal,
                'calle'         =>$request->calle,
                'num_int'       =>$request->num_int,
                'num_ext'       =>$request->num_ext,
                'latitud'       =>$request->latitud,
                'longitud'      =>$request->longitud,
                'referencias'   =>$request->referencias,
                'alias'         =>$request->alias
            ]);
            return response()->json(['status'=>true,'message'=>'Dirección guardada'], 200);
        }else{
            return response()->json(['status'=>false,'message'=>'Solo se permiten un máximo de 5 direcciones'], 500);
        } 
    }

    public function actualizar_direccion(Request $request)
    {
        $this->validate($request,[
            'direccion_id'  =>'required',
            'estado'        =>'required',
            'municipio'     =>'required',
            'localidad'     =>'required',
            'codigo_postal' =>'required',
            'calle'         =>'required',
            'latitud'       =>'required',
            'longitud'      =>'required',
            'alias'         =>'required'
        ],[
            'estado.required'       =>'El estado es necesario',
            'municipio.required'    =>'El municipio es requerido',
            'localidad.required'    =>'La localidad es requerida',
            'codigo_postal.required'=>'El código postal es requerido',
            'calle.required'        =>'La calle es requerida',
            'latitud.required'      =>'La latitud es requerida',
            'longitud.required'     =>'La longitud es requerida',
            'alias.required'        =>'El alias es requerido',
            'direccion_id.required' =>'El ID de la dirección es requerido'
        ]);
        $usuario = $request->user();
        $direccion = Direcciones::find($request->direccion_id);
        if(!$direccion){
            return response()->json([
                'mensaje'   =>'Esta dirección no existe, verifique el ID de la dirección'
            ],500);
        }
        if($direccion->user_id != $usuario->id){
            return response()->json([
                'mensaje'   =>'Esta dirección no corresponde a este usuario, verifique de nuevo.'
            ],500);
        }
        $direccion_alias = Direcciones::where('alias',$request->alias)->where('user_id',$usuario->id)->first();
        
        if($direccion_alias && $direccion_alias->id != $request->direccion_id){
            return response()->json(["message"=>"The given data was invalid.",
                    "errors"=> ["alias"=>["Este alias ya existe entre sus direcciones, intente con otro."]]
                ], 422);
        }
        $direccion->alias = $request->alias;
        $direccion->estado = $request->estado;
        $direccion->municipio = $request->municipio;
        $direccion->localidad = $request->localidad;
        $direccion->codigo_postal = $request->codigo_postal;
        $direccion->calle = $request->calle;
        $direccion->num_int = $request->num_int;
        $direccion->num_ext = $request->num_ext;
        $direccion->referencias = $request->referencias;
        $direccion->latitud = $request->latitud;
        $direccion->longitud = $request->longitud;
        $direccion->save();
        return response()->json([
            'mensaje'   =>'Dirección actualizada correctamente'
        ],200);
    }

    public function eliminar_direccion(Request $request)
    {
        $this->validate($request,[
            'direccion_id'  =>'required'
        ],[
            'direccion_id.required' =>'El id de la dirección es requerido'
        ]);
    }

    public function direcciones(Request $request)
    {
        $direccion = Direcciones::select('id as direccion_id','alias','estado','municipio','localidad','codigo_postal','calle','num_int','num_ext','latitud','longitud','referencias')->where('user_id',$request->user()->id)->get();
        return response()->json($direccion, 200);
    }

    public function solicitudes(Request $request)
    {
        try {
            $servicios =SolicitudServicio::select("solicitud_servicio.id as solicitud_id",
            DB::raw('CONCAT(users.name,IF(ISNULL(users.paterno),"",CONCAT(" ",users.paterno)), IF(ISNULL(users.materno),"",CONCAT(" ",users.materno))) as cliente'),
            DB::raw("IF(solicitud_servicio.tipo = 1, 'Programado','Urgente') as tipo "),
            "solicitud_servicio.horario_programado",
            "servicio_tecnico.tecnico_id",
            "servicio_tecnico.horario_estimado",
            'servicio_tecnico.horario_llegada',
            'servicio_tecnico.horario_inicio',
            'servicio_tecnico.horario_fin',
            DB::raw("(CASE WHEN solicitud_servicio.estatus = 1 THEN 'Pendiente' WHEN solicitud_servicio.estatus = 2 THEN 'Asignado' ELSE 'Finalizado' END) as estatus"),            
            'servicios.nombre as servicio',
            'direccion_solicitud.municipio',
            'direccion_solicitud.estado',
            'direccion_solicitud.localidad',
            'direccion_solicitud.codigo_postal',
            'direccion_solicitud.calle',
            'direccion_solicitud.num_ext',
            'direccion_solicitud.num_int',
            'direccion_solicitud.referencias',
            'direccion_solicitud.latitud',
            'direccion_solicitud.longitud',
            'solicitud_servicio.visita_conekta',
            'solicitud_servicio.orden_conekta',
            'solicitud_servicio.estatus_orden')
            ->join('users','users.id','=','solicitud_servicio.cliente_id')
            ->join('servicios','servicios.id','=','solicitud_servicio.servicio_id')
            ->leftJoin('direccion_solicitud','direccion_solicitud.solicitud_id','=','solicitud_servicio.id')
            ->leftJoin('servicio_tecnico','servicio_tecnico.solicitud_servicio_id','=','solicitud_servicio.id')
            ->orderBy('solicitud_servicio.estatus','ASC')
            ->orderBy('solicitud_servicio.tipo','DESC')
            ->where('cliente_id',$request->user()->id)
            ->get();
            foreach ($servicios as $item) {
                $paso = 1;
                $galeria = GaleriaSolicitud::select(DB::raw("CONCAT('".asset('storage/solicitudes/')."/',imagen) as imagen"))->where('solicitud_id',$item->solicitud_id)->get();
                $item['galeria'] = $galeria;
                $cotizacion = Cotizaciones::where([
                    ['solicitud_id',$item->solicitud_id],
                    ['tipo',2]
                ])->first();
                $estimacion = Cotizaciones::where([
                    ['solicitud_id',$item->solicitud_id],
                    ['tipo',1]
                ])->first();
                if($cotizacion){
                    $item['cotizacion'] = asset('storage/solicitudes/cotizaciones/'.$cotizacion->archivo);
                    $item['costo'] = $cotizacion->costo;
                    if($cotizacion->estatus != 1){
                        $paso = 2;
                    }
                }else{
                    if($estimacion){
                        $item['estimacion'] = asset('storage/solicitudes/estimaciones/'.$estimacion->archivo);
                        $item['costo']  = $estimacion->costo;
                    }
                }
                $item['paso'] = $paso;
            }
            return response()->json($servicios, 200);
        } catch (\Throwable $th) {
            return response()->json([
                'mensaje'   =>$th->getMessage()
            ],$th->getCode() ? $th->getCode() : 500);
        }
        
    }

    public function mensaje_tecnico(Request $request)
    {
        $this->validate($request,[
            'solicitud_id'  =>'required'
        ]);
        
        try {
            $solicitud = SolicitudServicio::find($request->solicitud_id);
            $solicitud_tecnico = ServicioTecnico::where("solicitud_servicio_id",$request->solicitud_id)->first();
            if($request->user()->getRoleNames()[0]!="cliente"){
                throw new Exception("El usuario no es un cliente.", 500);
            }
            if(!$solicitud){
                throw new Exception("La solicitud no existe, verifique la petición.", 500);
            }else{
                if(!$solicitud_tecnico){
                    throw new Exception("La solicitud aún no ha sido asignada a un técnico", 500);
                }
            }
            $mensaje = MensajeTecnico::where('solicitud_id',$request->solicitud_id)->first();
            if($mensaje){
                return response()->json([
                    'mensaje'   =>$mensaje->mensaje,
                    'fecha_hora'=>$mensaje->created_at
                ], 200);
            }else{
                return response()->json([
                    'mensaje'   =>''
                ], 200);
            }
            
        } catch (\Throwable $th) {
            return response()->json([
                'mensaje'   =>$th->getMessage()
            ],500);
        }
    }

    public function aceptar_cotizacion(Request $request)
    {
        $this->validate($request,[
            'aceptado'      =>'required',
            'solicitud_id'  =>'required',
             
        ],[
            'aceptado.required'     =>'Falta indicar si la cotización es aceptada o rechazada',
            'solicitud_id.required' =>'El ID de la solicitud es requerido', 
        ]);
        $solicitud = SolicitudServicio::find($request->solicitud_id);
        try {
            if(!$solicitud){
                throw new Exception("No existe la solicitud, verifica el ID", 500);
            }else{
                $mensaje = "";
                $cotizacion = Cotizaciones::where([
                    ['solicitud_id',$solicitud->id],
                    ['tipo',2]
                ])->first();
                if($request->aceptado){
                    $cotizacion->estatus = 2;
                    $solicitud->orden_conekta = $request->orden_conekta;         
                    $mensaje = "Cotización aceptada correctamente";
                }else{
                    $cotizacion->estatus = 3; //Cotización rechazada
                    $solicitud->estatus = 3; //Finalizado por cotización rechazada 
                    $mensaje = "Cotización rechazada correctamente";           
                }
                $solicitud->estatus_orden = $request->estatus_orden;
                $solicitud->save();
                $cotizacion->save();
                return response()->json([
                    'mensaje'   =>$mensaje
                ],200);
            }
        } catch (\Throwable $th) {
            return response()->json([
                'mensaje'   =>$th->getMessage()
            ],500);
        }
    }

    public function guardar_id_conekta(Request $request)
    {
        $this->validate($request,[
            'conekta_cliente'   =>'required'
        ],[
            'conekta_cliente.required'  =>'El ID del cliente de Conekta es requerido'
        ]);
        try {            
            $datos_extras = DatosExtras::where('user_id',$request->user()->id)->first();
            if(!$datos_extras){
                DatosExtras::create([
                    'user_id'           =>$request->user()->id,
                    'cliente_conekta'   =>$request->conekta_cliente
                ]);
            }else{
                $datos_extras->cliente_conekta = $request->conekta_cliente;
                $datos_extras->save();
            }
            return response()->json([
                'mensaje'   =>'Se ha guardado el cliente de Conekta'
            ],200);
        } catch (\Throwable $th) {
            return response()->json([
                'mensaje'   =>$th->getMessage()
            ],500);
        }
        
    }

    public function key_conekta(Request $request)
    {
        $conekta = Configuraciones::select("pvk_conekta as llave_privada","puk_conekta as llave_publica")->where("id",1)->first();
        return response()->json($conekta);
    }
}
