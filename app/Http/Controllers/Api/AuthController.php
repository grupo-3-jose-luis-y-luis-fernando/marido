<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Clientes;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\DatosExtras;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class AuthController extends Controller
{
    public function login(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'email'                 => 'required|string|email',
			'password'              => 'required|string',
			'token_notificacion'    => 'required|string'
        ],[
            'email.required'                =>'El correo es requerido',
            'email.email'                   =>'El correo debe tener un formato válido',
            'password.required'             =>'La contraseña es requerida',
			'token_notificacion.required'   =>'El token para las notificaciones es requerido'
        ]);

		if ($validator->fails())
		{
			return response([
                'result'    =>null,
                'message'   =>$validator->errors()->all(),
                'count'     =>0
            ], 422);
		}

		$credentials = request(['email', 'password']);
		if (!Auth::attempt($credentials))
		{
			return response()->json([
                'result'    =>null,
                'message' => 'Unauthorized',
                'count'     =>0				
			], 401);
		}else{
			$user = Auth::user();
			$tokenResult = $user->createToken('Personal Access Token');
			$token = $tokenResult->token;
			$token->expires_at = Carbon::now()->addWeeks(1);
			$token->save();
			
			//Guardar el token de firebase
			User::where('id', Auth::id())->update(['token_notificacion' => $request->token_notificacion]);
			
			return response()->json([
				'result'    =>[
					'access_token'	=>$tokenResult->accessToken,
					'token_type'	=>'Bearer',
					'expires_at'	=>Carbon::parse($tokenResult->token->expires_at)->toDateTimeString(),
					'tipo'			=>Auth::user()->roles[0]->name
				],
				'message'   =>NULL,
				'count'     =>1
			]);
		}
	}

    public function register(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'nombre'		=>'required',
			//'paterno'		=>'required',
			//'materno'		=>'required',
			'email'     	=>'required|string|email|unique:users,email',
			'password'		=>'required|string',
        ],[
			'nombre.required'		=>'El nombre es requerido',
			//'paterno.required'		=>'El apellido paterno es requerido',
			//'materno.required'		=>'El apellido materno es requerido',
            'email.required'    	=>'El correo es requerido',
			'email.email'       	=>'El correo debe tener un formato válido',
			'email.unique'			=>'El correo ya está en uso, intente con otro',
            'password.required' 	=>'La contraseña es requerida'
        ]);

		if ($validator->fails())
		{
			return response([
                'result'    =>null,
                'message'   =>$validator->errors()->all(),
                'count'     =>0
            ], 422);
		}
		try {
			$user = User::create([
				'name' 	    =>$request->nombre,
				'paterno'	=>$request->paterno,
				'materno'	=>$request->materno,
				'email' 	=>$request->email,
				'password'	=>bcrypt($request->password),
            ]);
            $user->assignRole('cliente');

			return response()->json([
				'result'	=>Arr::except($user, ['roles']),
				'message'	=>NULL,
				'count'		=>1
			]);
		} catch (\Throwable $th) {
			return response([
                'result'    =>null,
                'message'   =>$th->getMessage(),
                'count'     =>0
            ], 422);
		}
	}
	
	public function perfil(Request $request)
	{
		$data = [];
		$usuario = $request->user();
		$datos_extras = DatosExtras::where('user_id',$usuario->id)->first();
		$data['nombre'] = $usuario->name;
		$data['apellido_paterno'] = $usuario->paterno;
		$data['apellido_materno'] = $usuario->materno;
		$data['correo'] = $usuario->email;
		$data['cliente_conekta'] = "";
		if($datos_extras) {
			$data['avatar'] = is_null($datos_extras->avatar) ? asset("images/user.png") :  asset("storage/usuarios/".$datos_extras->avatar);
			$data['cliente_conekta'] = $datos_extras->cliente_conekta;
			$data['telefono'] = is_null($datos_extras->telefono) ? '' : $datos_extras->telefono ; 
			$data['matricula'] = is_null($datos_extras->matricula) ? asset("images/user.png") :  asset("storage/usuarios/".$datos_extras->matricula);
		}
		return $data;
	}

	public function actualizar(Request $request)
	{
		$this->validate($request,[
			'nombre'	=>'required',
			'email'		=>'required|unique:users,email,'.$request->user()->id,
		],[
			'nombre.required'	=>'El nombre es requerido',
			'email.required'	=>'El correo es requerido',
			'email.unique'		=>'Este correo ya ha sido registrado, intente con otro'
		]);
		try {
			$datos_extras = DatosExtras::where('user_id',$request->user()->id)->first();
			if(!$datos_extras){
				$datos_extras = DatosExtras::create([
					'user_id'	=>$request->user()->id,
					'avatar'	=>NULL,
					'rfc'		=>NULL,
					'telefono'	=>NULL
				]);
			}
			if($request->filled('avatar')){
				$imageName = "";
				$imageName = uniqid().'.png'; 
				Storage::put("public/usuarios/".$imageName, base64_decode($request->avatar)); 
				Storage::delete("public/usuarios/".$datos_extras['avatar']);
				$datos_extras->avatar = $imageName;
			}

			if($request->filled('matricula')){
				$imageName = "";
				$imageName = uniqid().'.png'; 
				Storage::put("public/usuarios/".$imageName, base64_decode($request->matricula)); 
				Storage::delete("public/usuarios/".$datos_extras['matricula']);
				$datos_extras->matricula = $imageName;
			}

			$datos_extras->telefono = $request->telefono;
			$request->user()->name = $request->nombre;
			$request->user()->paterno = $request->paterno;
			$request->user()->materno = $request->materno;
			$request->user()->email = $request->email; 
			$request->user()->save();
			$datos_extras->save(); 
			return response()->json([
				'mensaje'	=>'Perfil actualizado'
			],200);
		} catch (\Throwable $th) {
			return response()->json([
				'mensaje'	=>$th->getMessage()
			],500);
		}
		
	}
}
