<?php

namespace App\Notifications;

use Illuminate\Support\Facades\Http; 

class FireBaseNotificacion
{
    private $url;

    function __construct()
    {
        $this->url = env('FIREBASE_URL', NULL);
    }

    public function enviar_ahora($titulo, $mensaje, $destinatario, $broadcast = false)
    {
        try {
            if($broadcast) {        //Mensaje para varios usuarios
                Http::withHeaders([
                    'Authorization'     => 'key='.env('FIREBASE_KEY', NULL),
                    'Content-Type'      => 'application/json'
                ])->post ($this->url, [
                    'registration_ids'=>$destinatario,
                    'notification' =>[
                        'title'             => $titulo,
                        'body'              => $mensaje,
                        'content_available' => true,
                        'priority'          => 'high'
                    ],
                    'data' =>[
                        "scheduled"         => false,
                        'title'             => $titulo,
                        'body'              => $mensaje,
                        'content_available' => true,
                        'priority'          => 'high'
                    ]
                ]);
                return true;
            } else{                 //Mensaje para un usuario
                Http::withHeaders([
                    'Authorization'     => 'key='.env('FIREBASE_KEY', NULL),
                    'Content-Type'      => 'application/json'
                ])->post ($this->url, [
                    'to'            =>  $destinatario,
                    'notification'  =>[
                        'title'             => $titulo,
                        'body'              => $mensaje,
                        'content_available' => true,
                        'priority'          => 'high'
                    ],
                    'data' =>[
                        "scheduled"         => false,
                        'title'             => $titulo,
                        'body'              => $mensaje,
                        'content_available' => true,
                        'priority'          => 'high'
                    ]
                ]);
                return true;
            }
        } catch (\Throwable $th) {
            return false;
        }
    }

    public function programar_envio($titulo, $mensaje, $destinatario, $fecha, $id)
    {
        try {                   //Mensaje programado para varios usuarios
            Http::withHeaders([
                'Authorization'     => 'key='.env('FIREBASE_KEY', NULL),
                'Content-Type'      => 'application/json'
            ])->post ($this->url, [
                'registration_ids'  => $destinatario,
                    'data' =>[
                        "schedule"          => true,
                        "id"                => $id,
                        'title'             => $titulo,
                        'body'              => $mensaje,
                        'content_available' => true,
                        'priority'          => 'high',
                        "date"              => $fecha
                    ]
            ]);
            return true;
        } catch (\Throwable $th) {
            return false;
        }
    }
    
    public function eliminar($destinatario, $id)
    {
        try {                   //Mensaje programado para varios usuarios
            Http::withHeaders([
                'Authorization'     => 'key='.env('FIREBASE_KEY', NULL),
                'Content-Type'      => 'application/json'
            ])->post ($this->url, [
                'registration_ids'  => $destinatario,
                    'data' =>[
                        "schedule"  => false,
                        "id"        => $id
                    ]
            ]);
            return true;
        }catch (\Throwable $th) {
            return false;
        }
    }
}
