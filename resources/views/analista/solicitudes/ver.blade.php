@extends('layouts.panel')

@section('titulo','Datos de la solicitud :: '.env('APP_NAME'))    
@section('area','Datos de la solicitud')

@section('contenido')
<div class="container">
    <table class="table table-bordered"> 
        <tbody>
            <tr>
                <td><strong>Cliente</strong></td>
                <td>{{ $solicitud->name }} {{ $solicitud->paterno }} {{ $solicitud->materno }}</td>
                <td><strong>Estatus</strong></td>
                <td>
                    @php
                        $estatus = [
                            '1'=>"<span class='badge badge-warning p-2'>Pendiente</span>",
                            '2'=>"<span class='badge badge-primary p-2'>Asignado</span>",
                            '3'=>"<span class='badge badge-success p-2'>Finalizado</span>",
                            '4'=>"<span class='badge badge-danger p-2'>Cancelado</span>",
                            '5'=>"<span class='badge badge-info p-2'>Trabajando</span>"
                        ]                        
                    @endphp
                    {!! $estatus[$solicitud->estatus] !!}
                </td>
            </tr>
            <tr>
                <td><strong>Dirección</strong></td>
                <td>{{ $solicitud->calle.($solicitud->num_ext ? " ".$solicitud->num_ext : "").($solicitud->num_int ? " ".$solicitud->num_int : "").",".$solicitud->localidad.', C.P. '.$solicitud->codigo_postal.' '.$solicitud->municipio.', '.$solicitud->estado.'.' }}</td>
                <td><strong>Referencias</strong></td>
                <td>{{ $solicitud->referencias }}</td>
            </tr>
            <tr>
                <td><strong>Nombre común del servicio</strong></td>
                <td>{{ $solicitud->nombre }}</td> 
                <td><strong>Nombre técnico del servicio<strong></td>
                <td>{{ $solicitud->nombre_tecnico ?? "Dato pendiente por el técnico" }}</td>
            </tr>
            <tr>
                <td><strong>Tipo:</strong></td>
                <td>{!! $solicitud->tipo == 2 ? "<span class='p-2 badge badge-danger'>Urgente</span>" : "<span class='p-2 badge badge-primary'>Programado</span>" !!}</td>
                <td>{!! $solicitud->tipo == 1 ? "<strong>Fecha y hora progamada:</strong></td><td> ".$solicitud->horario_programado : "<strong>Fecha y hora en la que se creó la solicitud:</strong></td><td> ".$solicitud->created_at !!}</td>
            </tr>
            <tr>
                <td><strong>Residente asignado</strong></td>
                <td>{{ $solicitud->residente }}</td>
                <td><strong>Técnico asignado</strong></td>
                <td>{{ $solicitud->tecnico }}</td>
            </tr>           
            <tr>
                <td><strong>Estimación</strong></td>
                <td>{{ $estimacion ? "$".number_format($estimacion->costo, 2, '.', ',') : "Sin estimación" }}</td>

                <td><strong>Cotización</strong></td>
                <td>{{ $cotizacion ? "$".number_format($cotizacion->costo, 2, '.', ',') : "Aún sin cotización" }}</td>
            </tr>
            @if ($galeria)
            <tr>
                <td><strong>Galería</strong></td>
                <td colspan="3">
                    <div class="row justify-content-center">
                    @foreach ($galeria as $item)
                        <div class="col-3">
                            <a href="{{ asset('storage/solicitudes/'.$item->imagen) }}" data-fancybox="galeria">
                                <img src="{{ asset('storage/solicitudes/'.$item->imagen) }}" alt="{{ $item->imagen }}" class="img-fluid" >
                            </a>
                        </div>
                    @endforeach
                    </div>
                </td>
            </tr>
        @endif   
        </tbody>
    </table>
    <div class="form-group text-right">
        <a class="btn btn-info" href="{{ route('analista.solicitudes') }}">Regresar</a>
    </div>
</div>
@endsection

@section('post_script')
<script src="{{ asset('js/panel/fancybox.umd.js') }}"></script>
<script>
    Fancybox.bind("[data-fancybox]", {
        Image: {
            zoom:true,
        },
    }); 
</script>
@endsection

@section('post_styles')
<link rel="stylesheet" href="{{ asset('css/panel/fancybox.css') }}"/>
@endsection