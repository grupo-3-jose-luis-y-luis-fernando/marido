@extends('layouts.panel')

@section('titulo')
    Solicitudes de Servicios
@endsection

@section('area')
    Solicitudes de Servicios
@endsection

@section('contenido') 
    <table class="table text-center" data-url="{{ route('analista.dt_solicitudes') }}" id="dt_solicitudes">
        <thead>
            <tr>
                <th>Folio</th>
                <th>Servicio</th>
                <th>Cliente</th>
                <th>Tipo</th>
                <th>Horario Programado</th>
                <th>Dirección</th>
                <th>Estatus</th>
                <th>Acciones</th>
            </tr>
        </thead>
    </table>
@endsection