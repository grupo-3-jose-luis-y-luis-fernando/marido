@extends('layouts.panel')

@section('titulo')
    Asignación de solicitud - Marido
@endsection

@section('area')
    Asignación de solicitud
@endsection

@section('contenido')
    <div class="row justify-content-center">
        <div class="col-5">
            <div class="alert alert-warning">
                Los campos con <span class="text text-danger">*</span> son requeridos
            </div>
            <div class="row">
                <div class="col-12">
                    <table class="table table-bordered">
                        <tr>
                            <td><strong>Cliente:</strong></td>
                            <td colspan="3">{{ $solicitud->name }} {{ $solicitud->paterno }} {{ $solicitud->materno }}</td>
                        </tr>
                        <tr>
                            <td><strong>Servicio:</strong></td>
                            <td colspan="3">{{ $solicitud->nombre }}</td> 
                        </tr>
                        <tr>
                            <td><strong>Tipo:</strong></td>
                            <td>{!! $solicitud->tipo == 2 ? "<span class='btn btn-danger'>Urgente</span>" : "<span class='btn btn-primary'>Programado</span>" !!}</td>
                            <td>{!! $solicitud->tipo == 1 ? "<strong>Fecha y hora progamada:</strong></td><td> ".$solicitud->horario_programado : "<strong>Fecha y hora en la que se creó la solicitud:</strong></td><td> ".$solicitud->created_at !!}</td>
                        </tr>   
                        <tr>
                            <td><strong>Descripción</strong></td>
                            <td colspan="3">{{ $solicitud->descripcion }}</td>
                        </tr> 
                        @if ($galeria)
                            <tr>
                                <td><strong>Galería</strong></td>
                                <td colspan="3">
                                    <div class="row justify-content-center">
                                    @foreach ($galeria as $item)
                                        <div class="col-3">
                                            <a href="{{ asset('storage/solicitudes/'.$item->imagen) }}" data-fancybox="galeria">
                                                <img src="{{ asset('storage/solicitudes/'.$item->imagen) }}" alt="{{ $item->imagen }}" class="img-fluid" >
                                            </a>
                                        </div>
                                    @endforeach
                                    </div>
                                </td>
                            </tr>
                        @endif                    
                    </table> 
                </div>
                <div class="col-12">
                <form class="row form" data-alert="true" action="{{ route('analista.asignar_solicitud') }}" id="form_asignar_solicitud" method="POST" enctype="multipart/form-data" data-archivos="true">
                    @csrf
                    <input type="hidden" name="solicitud" value="{{ $solicitud->id }}">
                    <div class="col-6 form-group">
                        <label for="residente">Residente<span class="text text-danger">*</span></label>
                        <select name="residente" id="residente" class="form-control">
                            <option value="">-- Seleccione un residente --</option>
                            @foreach ($residentes as $item)
                                <option value="{{ $item->id }}">{{ $item->name }} {{ $item->paterno }} {{ $item->materno }}</option>
                            @endforeach
                        </select>
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="w-100"></div>
                    <div class="col-6 form-group">
                        <label for="tecnico">Técnico<span class="text text-danger">*</span></label>
                        <select name="tecnico" id="tecnico" class="form-control">
                            <option value="">-- Seleccione un técnico --</option>
                            @foreach ($tecnicos as $item)
                                <option value="{{ $item->id }}">{{ $item->name }} {{ $item->paterno }} {{ $item->materno }} (Servicios asignados: {{ $item->servicios }})</option>
                            @endforeach
                        </select>
                        <span class="invalid-feedback"></span>
                    </div>
                     <div class="col-4 form-group">
                        <label for="fecha_llegada">Horario de llegada estimada<span class="text text-danger">*</span></label>
                        <input type="text" id="fecha_llegada" name="fecha_llegada" class="form-control">
                        <span class="invalid-feedback"></span>
                    </div> 
                    @if ($galeria->count() > 0 && $solicitud->estatus == 1)
                    <div class="col-6 form-group">
                        <label for="estimacion">Estimación(PDF)</label>
                        <input type="file" name="estimacion" class="form-control" id="estimacion">
                        <span class="invalid-feedback"></span>
                    </div>    
                    <div class="col-4 input-group mb-3">
                        <label for="costo_estimado" class="w-100">Costo estimado</label>
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">$</span>
                        </div>
                        <input type="text" class="form-control onlynumber" name="costo_estimado" id="costo_estimado" aria-describedby="basic-addon1">
                        <span class="invalid-feedback"></span>
                    </div>
                    @endif
                    <div class="col-12 form-group">
                        <button type="submit" class="btn btn-success">Asignar</button>
                        <a href="{{ route('analista.solicitudes') }}" class="btn btn-danger">Cancelar</a>
                    </div>
                </form>
                </div>
            </div>
        </div>
        <div id="calendar" class="col-7 mx-auto mt-3"></div>        
    </div> 
@endsection

@section('post_script')
    <script src="{{ asset('calendar/main.min.js') }}"></script>
    <script src="{{ asset('calendar/locales/es.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
    <script src="{{ asset('js/panel/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('js/panel/es.js') }}"></script>
    <script src="{{ asset('js/panel/fancybox.umd.js') }}"></script>
    <script>
        var calendar 
        document.addEventListener('DOMContentLoaded', function() {
            var calendarEl = document.getElementById('calendar');
            calendar = new FullCalendar.Calendar(calendarEl, {
                initialView: 'timeGridDay',
                headerToolbar: {
                    left: 'prev,next today',
                    center: 'title',
                    right: ''
                },
                locale:'es'
            }); 
            calendar.render();
            calendar.gotoDate("{{ $solicitud->tipo == 1 ? $solicitud->horario_programado : $solicitud->created_at }}");
        }); 
        $(document).ready(function(){
            $("#tecnico").change(function(){
                $.ajax({
                    url:"{{ route('analista.asignados') }}",
                    type:"POST",
                    data:{_token:$("meta[name='csrf-token']").attr('content'),tecnico:$(this).val()}
                }).done(function(response){ 
                    removeEvents = calendar.getEvents(); 
                    removeEvents.forEach(event => {
                        event.remove();
                    });
                    $.each(response, function(index, value){ 
                        console.log(value);
                        if (value.horario_inicio !== null && value.horario_fin == null) {
                            calendar.addEvent({
                                title: "Técnico ocupado",
                                start:value.horario_inicio,
                                end:"{{ \Carbon\Carbon::now()->addYear(1) }}",
                                allDay: false,
                                backgroundColor:"#296aab"
                            });     
                        }else{
                            if(value.horario_fin !== null){
                                calendar.addEvent({
                                    title: "Solicitud finalizada",
                                    start:value.horario_inicio,
                                    end:value.horario_fin,
                                    allDay: false,
                                    backgroundColor:"#b51112"
                                }); 
                            }else{
                                calendar.addEvent({
                                    title: "Fecha y hora de llegada estimada",
                                    start:value.horario_estimado,                                
                                    allDay: false
                                });
                            }
                            
                        }
                    }); 
                });
            });
           
        });
        $(function () {
            var inicio = new Date("{{ $solicitud->tipo == 1 ? $solicitud->horario_programado : $solicitud->created_at }}"); 
            $("#fecha_llegada").datetimepicker({
                locale: 'es',
                useCurrent:false,
                format:"YYYY-MM-DD HH:mm",
                defaultDate:inicio
            }); 
        });
        
        Fancybox.bind("[data-fancybox]", {
            Image: {
                zoom:true,
            },
        });  
    </script>
@endsection

@section('post_styles')
    <link href='{{ asset('calendar/main.min.css') }}' rel='stylesheet' />
    <link rel="stylesheet" href="{{ asset('css/panel/bootstrap-datetimepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/panel/fancybox.css') }}"/>
    <style>
        #calendar{
            max-height: 650px;
        }
    </style>
@endsection