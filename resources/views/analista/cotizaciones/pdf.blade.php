<table style="width: 190mm;" cellspacing="0" cellpadding="1">
    <tr>
        <td>
            <table style="padding:0;">
                <tr>
                    <td>
                        <h3 style="text-transform: uppercase; font-size:10px; line-height:8px; font-weight: 100">TUXTLA GUTIERREZ, CHIAPAS A {{ $fecha->day }} DE {{ $fecha->monthName }} DE {{ $fecha->year }}</h3>
                    </td>
                </tr>
                <tr>
                    <td>
                        <h3 style="text-align: right; font-size:10px;line-height:10px; margin-bottom:0; padding-bottom:0; font-weight: 100"><strong>ASUNTO: COTIZACIÓN <span style="color: #a82121;">{{ $nota }}</span></strong></h3>
                    </td>
                </tr>
                <tr>
                    <td style="line-height: -20px">
                        <h3 style="font-size:10px;padding:0;line-height: 12px; font-weight: 100">{{ $cliente->name }} {{ $cliente->paterno }} {{ $cliente->materno }}<br>{!! $direccion->calle.($direccion->num_ext ? " #".$direccion->num_ext : "").($direccion->num_int ? " #".$direccion->num_int : "").", ".$direccion->localidad.".<br>C.P. ".$direccion->codigo_postal." ".$direccion->municipio.", ".$direccion->estado !!}</h3>
                    </td>
                </tr>
                <tr>
                    <td>
                        <h3 style="font-size:10px;line-height:10px; margin-bottom:0; padding-bottom:0; font-weight: 100">De acuerdo a lo solicitado, ponemos a su disposición el siguiente presupuesto de trabajos consistente en:</h3>
                    </td>
                </tr>
            </table>
        </td>
    </tr> 
</table> 

<table cellpadding="5" style="width: 190mm;">
    <tr>
        <td style="line-height: 5px;"></td>
    </tr>
</table>

<table style="width: 190mm;"  cellspacing="0" cellpadding="5">
    <tr>
        <td style="font-size:10px; border:0.1mm solid black; text-align:center; width:22mm;"><strong>Cve.</strong></td>
        <td style="font-size:10px; border:0.1mm solid black; text-align:center; width:80mm;"><strong>Concepto</strong></td>
        <td style="font-size:10px; border:0.1mm solid black; text-align:center; width:22mm;"><strong>Unidad</strong></td>
        <td style="font-size:10px; border:0.1mm solid black; text-align:center; width:22mm;"><strong>Cantidad</strong></td>
        <td style="font-size:10px; border:0.1mm solid black; text-align:center; width:22mm;"><strong>Precio</strong></td>
        <td style="font-size:10px; border:0.1mm solid black; text-align:center; width:22mm;"><strong>Importe</strong></td>
    </tr>
    @foreach ($conceptos as $item)
    @php
        $importe = str_replace(',','',$importes[$item->solicitud_concepto]);
        $subtotal = $subtotal + $importe;
    @endphp
    <tr>
        <td style="font-weight: 100; font-size:7px; border:0.1mm solid black; text-align:center;">{{ $item->clave }}</td>
        <td style="font-weight: 100; font-size:7px; border:0.1mm solid black; text-align:center;">{{ $item->nombre }}</td>
        <td style="font-weight: 100; font-size:7px; border:0.1mm solid black; text-align:center;">PZA</td>
        <td style="font-weight: 100; font-size:7px; border:0.1mm solid black; text-align:center;">1</td>
        <td style="font-weight: 100; font-size:7px; border:0.1mm solid black; text-align:center;">${{ $importe }}</td>
        <td style="font-weight: 100; font-size:7px; border:0.1mm solid black; text-align:center;">${{ $importe }}</td>
    </tr> 
    @endforeach   
    @php
        $total_descuento = $descuento > 0 ? ($subtotal * ($descuento / 100)) : 0;
        $subtotal_con_descuento = $total_descuento + $subtotal;
        $iva = $subtotal_con_descuento * 0.16;
        $total = $iva+$subtotal_con_descuento;
    @endphp
    <tr>
        <td style="border:0.1mm solid black;"></td>
        <td style="border:0.1mm solid black;"></td>
        <td style="border:0.1mm solid black;"></td> 
        <td style="border:0.1mm solid black;"></td>
        <td style="font-weight: 100; font-size:9px; border:0.1mm solid black; text-align:center;">Importe</td>
        <td style="font-weight: 100; font-size:9px; border:0.1mm solid black; text-align:center;">${{ number_format($subtotal,2) }}</td>
    </tr>
    <tr>
        <td style="border:0.1mm solid black;"></td>
        <td style="border:0.1mm solid black;"></td>
        <td style="border:0.1mm solid black;"></td> 
        <td style="border:0.1mm solid black;"></td>
        <td style="font-weight: 100; font-size:9px; border:0.1mm solid black; text-align:center;">Descuento({{ $descuento }}%)</td>
        <td style="font-weight: 100; font-size:9px; border:0.1mm solid black; text-align:center;">${{ number_format($total_descuento,2) }}</td>
    </tr>
    <tr>
        <td style="border:0.1mm solid black;"></td>
        <td style="border:0.1mm solid black;"></td>
        <td style="border:0.1mm solid black;"></td> 
        <td style="border:0.1mm solid black;"></td>
        <td style="font-weight: 100; font-size:9px; border:0.1mm solid black; text-align:center;">Subtotal</td>
        <td style="font-weight: 100; font-size:9px; border:0.1mm solid black; text-align:center;">${{ number_format($subtotal_con_descuento,2) }}</td>
    </tr>
    <tr>
        <td style="border:0.1mm solid black;"></td>
        <td style="border:0.1mm solid black;"></td>
        <td style="border:0.1mm solid black;"></td> 
        <td style="border:0.1mm solid black;"></td>
        <td style="font-weight: 100; font-size:9px; border:0.1mm solid black; text-align:center;">IVA</td>
        <td style="font-weight: 100; font-size:9px; border:0.1mm solid black; text-align:center;">${{ number_format($iva,2) }}</td>
    </tr>
    <tr>
        <td style="border:0.1mm solid black;"></td>
        <td style="border:0.1mm solid black;"></td>
        <td style="border:0.1mm solid black;"></td> 
        <td style="border:0.1mm solid black;"></td>
        <td style="font-weight: 100; font-size:9px; border:0.1mm solid black; text-align:center;">Total</td>
        <td style="font-weight: 100; font-size:9px; border:0.1mm solid black; text-align:center;">${{ number_format($total,2) }}</td>
    </tr>
</table> 