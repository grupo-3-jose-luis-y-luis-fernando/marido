@extends('layouts.panel')
@section('titulo')
    Cotización :: Sistema Marido
@endsection

@section('area')
    Creación de cotización
@endsection

@section('contenido')
<div class="container">
    <div class="row">
        <div class="col-12">
            <h2 class="text-center">Servicios y materiales solicitados por el técnico</h2>
            <form action="{{ route('analista.guardar_cotizacion', $solicitud_id) }}" method="POST" class="form" id="form_cotizacion">
                @csrf
                <table class="table">
                    <thead>
                        <tr>
                            <th>Servicio</th>
                            <th>Aceptado</th>
                            <th colspan="3">Información</th>
                        </tr>    
                    </thead>    
                    <tbody> 
                        @php
                            $total = 0;
                        @endphp
                        @foreach ($conceptos as $concepto)
                        @php
                            $subtotal = 0;
                            $mano_obra = 0;
                            $importe = 0;
                        @endphp
                        <tr>
                            <td>
                                {{ $concepto->nombre }}
                            </td>
                            <td>
                                <i class="fa fa-2x {{ $concepto->aceptado == 1 ? "fa-check-circle text-success" : "fa-times-circle text-danger" }}"></i>
                            </td>
                            <td colspan="3" class="p-0">
                                <table class="table bg-transparent"> 
                                    @if ($concepto->materiales->count() > 0)
                                        <tr>
                                            <td class="border-0">Material</td>
                                            <td class="border-0">Cantidad</td>
                                            <td class="border-0">Costo</td>
                                            <td class="border-0">Disponibilidad</td>
                                        </tr>
                                        @foreach ($concepto->materiales as $item)
                                            @php
                                                $subtotal+= $item->cantidad * $item->costo;
                                            @endphp
                                            <tr>
                                                <td class="border-0">{{ $item->nombre }}</td>
                                                <td class="border-0">{{ $item->cantidad }}</td>
                                                <td class="border-0">${{ number_format($item->costo,2) }}</td>
                                                <td class="border-0">{{ $item->disponibilidad ?? 0 }}</td>
                                            </tr>
                                        @endforeach
                                        @php
                                            $mano_obra = $subtotal * ($concepto->costo_mano_obra / 100);
                                            $importe = $subtotal + $mano_obra;
                                            if($concepto->aceptado == 1){
                                                $total+=$importe;
                                            }
                                        @endphp
                                        <tr>
                                            <td>
                                                Subtotal
                                            </td>
                                            <td colspan="2">
                                                ${{ number_format($subtotal,2) }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Costo Mano de Obra
                                            </td>
                                            <td colspan="2">
                                                ${{ number_format($mano_obra,2) }} <span data-toggle="tooltip" data-placement="top" title="{{ $concepto->costo_mano_obra }}% del costo del servicio"><i class="fa fa-question-circle"></i></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Importe</td>
                                            <td colspan="2">
                                                @if ($concepto->aceptado == 1)
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                    <div class="input-group-text" id="btnGroupAddon">$</div>
                                                    </div>
                                                    <input type="text" class="form-control onlynumber importes" id="importes" name="importes[{{ $concepto->solicitud_concepto }}]" value="{{ number_format($importe,2) }}" aria-describedby="btnGroupAddon" aria-label="importes">
                                                    <span class="invalid-feedback"></span>
                                                </div>
                                                @else
                                                ${{ number_format($importe,2) }}
                                                @endif 
                                            </td>
                                        </tr>
                                    @endif 
                                </table> 
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            @php
                                $iva = $total * 0.16;
                                $total_importe = $total + $iva;
                            @endphp
                            <td colspan="5" align="right">
                                <div class="col-md-4 col-12 row">
                                    <label for="importe" class="col-12 col-md-6">Importe:</label>
                                    <div id="importe" class="col-12 col-md-6">${{ number_format($total,2) }}</div>
                                </div>
                                <div class="col-12 m-1"></div>
                                <div class="col-md-4 col-12 row">
                                    <label for="" class="col-12 col-md-6">Descuento:</label> 
                                    <div class="col-12 col-md-6">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text" id="btnGroupAddon">%</div>
                                            </div>
                                            <input type="text" class="form-control onlynumber" id="descuento_porcentaje" name="descuento" value="0" aria-describedby="btnGroupAddon" aria-label="descuento">
                                            <span class="invalid-feedback"></span>
                                        </div>
                                    </div>
                                    <div class="col-12 text-right">
                                        <div id="descuento">${{ number_format(0,2) }}</div>
                                    </div> 
                                </div> 
                                <div class="col-12 m-1"></div>
                                <div class="col-md-4 col-12 row">
                                    <label for="subtotal" class="col-12 col-md-6">Subtotal:</label>
                                    <div id="subtotal" class="col-12 col-md-6">${{ number_format($total,2) }}</div>
                                </div>
                                <div class="col-md-4 col-12 row">
                                    <label for="iva" class="col-12 col-md-6">Iva:</label>
                                    <div id="iva" class="col-12 col-md-6">${{ number_format($iva,2) }}</div>
                                </div>
                                <div class="col-md-4 col-12 row">
                                    <label for="total" class="col-12 col-md-6">Total:</label>
                                    <div id="total" class="col-12 col-md-6">${{ number_format($total_importe,2) }}</div>
                                </div>
                            </td> 
                        </tr> 
                        <tr>
                            <td colspan="5" align="right">
                                <a href="{{ route('analista.previsualizacion_cotizacion',$solicitud_id) }}" target="_blank" class="btn btn-primary previsualizacion">Visualizar</a>
                                <button class="btn btn-success">Enviar</button>
                            </td>
                        </tr>
                    </tfoot>
                </table>    
            </form> 
        </div> 
        {{-- <div class="col-12">
            <form action="{{ route('analista.guardar_cotizacion', $solicitud_id) }}" method="post" class="form row" id="form_cotizacion" data-archivos="true">
                @csrf
                <div class="col-6 form-group">
                    <label for="archivo">Cotización(PDF)</label>
                    <input type="file" name="archivo" class="form-control" id="archivo">
                    <span class="invalid-feedback"></span>
                </div>    
                <div class="col-4 input-group mb-3">
                    <label for="costo" class="w-100">Costo</label>
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">$</span>
                    </div>
                    <input type="text" class="form-control onlynumber" name="costo" id="costo" aria-describedby="basic-addon1">
                    <span class="invalid-feedback"></span>
                </div>
                <div class="form-group col-12">
                    <button class="btn btn-success" type="submit">Guardar</button>
                </div>
            </form>
        </div> --}}
    </div>    
</div>

@endsection
 
@section('post_script') 
    <script>
        $(document).ready(function(){
            $(".previsualizacion").on('click',function(ev){
                var $form = $("#form_cotizacion");
                ev.preventDefault();
                $.ajax({
                    url: $(this).attr('href'),
                    data: $form.serialize(),
                    type: "POST"                    
                }).done(function(response){
                    if(response.pdf){
                        window.open(response.pdf,'_blank');
                    }
                })                
                .fail(function(response){
                    if (response.status == 422) {
                        $form.find('.is-invalid').removeClass('is-invalid');
                        Swal.fire({
                            icon: "error",
                            title: "Error",
                            text: "Faltan datos por rellenar o existe algún detalle con éstos"
                        });
                        $.each(response.responseJSON.errors, function (index, value) {
                            $form.find("#" + index).addClass('is-invalid').parent().find('.invalid-feedback').html(value);
                        });
                    } else {
                        Swal.fire({
                            icon: "error",
                            title: "Error",
                            text: response.responseJSON.message
                        });
                    }
                });
            });
            $(".importes").on('change keyup', function(){
                var total = 0;
                var descuento = 0;
                var total_descuento = 0;
                var subtotal = 0;
                var iva = 0;
                $(".importes").each(function(index,value){         
                    total+=parseFloat(moneda($(value).val()).replace(',', ''));
                });
                $("#importe").text("$"+total.toFixed(2));
                descuento = $("#descuento_porcentaje").val().replace(',', '');
                if(descuento != 0 || descuento != ""){                    
                    total_descuento = total * (parseFloat(descuento) / 100)
                    total = total + total_descuento;
                }                

                $("#descuento").text("$"+total_descuento.toFixed(2));
                $("#subtotal").text("$"+(total).toFixed(2));
                iva = total * 0.16;
                $("#iva").text("$"+iva.toFixed(2));
                total = total + iva;
                $("#total").text("$"+total.toFixed(2));
            });
            $("#descuento_porcentaje").on('change keyup', function(){
                $(".importes").trigger('change');
            });
        });
    </script>
@endsection