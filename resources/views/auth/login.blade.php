@extends('layouts.sesion')
@section('title', 'Iniciar Sesión')
@section('links')
    <a href="{{route('login')}}" class="active">Iniciar Sesión</a>
    <a href="{{route('register')}}">Registrarse</a>
@endsection
@section('formulario')
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div><br/>
    @endif
    <form method="post" action="{{ route('login') }}">
        @csrf
        <input type="email" class="form-control" name="email" placeholder="Correo" value="{{old('email')}}" autocomplete="email" required>
        <input type="password" class="form-control" name="password" placeholder="Contraseña" autocomplete="current-password" required>
        <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
        <label for="remember">Mantener sesión activa </label>
        <div class="form-button">

            <button id="submit" type="submit" class="btn btn-success">Iniciar Sesión</button>
        </div>
    </form>
@endsection