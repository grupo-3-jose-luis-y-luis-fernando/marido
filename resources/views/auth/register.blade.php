@extends('layouts.sesion')
@section('title', 'Registrarse')
@section('links')
    <a href="{{route('login')}}">Iniciar Sesión</a>
    <a href="{{route('register')}}" class="active">Registrarse</a>
@endsection
@section('formulario')
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div><br/>
    @endif
    <form method="post" action="{{ route('register') }}">
        @csrf
        <input type="text" class="form-control" name="name" placeholder="Nombre" value="{{ old('name') }}" autocomplete="name" required autofocus>
        <input type="text" class="form-control" name="paterno" placeholder="Apellido Paterno" value="{{ old('paterno') }}" autocomplete="paterno" required>
        <input type="text" class="form-control" name="materno" placeholder="Apellido Materno" value="{{ old('materno') }}" autocomplete="materno" required>
        <input type="email" class="form-control" name="email" placeholder="Correo" value="{{old('email')}}" autocomplete="email" required>
        <input type="password" class="form-control" name="password" placeholder="Contraseña" autocomplete="current-password" required>
        <input type="password" class="form-control" name="password_confirmation" placeholder="Confirmar Contraseña" required autocomplete="new-password">
        <div class="form-button">
            <button id="submit" type="submit" class="btn btn-success">Registrarse</button>
        </div>
    </form>
@endsection