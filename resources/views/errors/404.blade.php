@extends('layouts.web')
@section('titulo', 'Página no encontrada - Marido')
@section('contenido')
<div class="text-center animate__animated animate__bounce" style="padding: 60px 0;">
    <h1>¡Ups! Lo sentimos, la página no se encontró.</h1>
    <img src="{{ asset('images/error-404.png') }}" alt="" class="img-fluid"><br>
    <a href="{{ route('index') }}" class="sppb-btn sppb-btn-default">Regresar al inicio</a>
</div> 
@endsection

@section('js')
    <script>
        $(".sppb-btn").hover(function() {
                $(this).addClass('animate__animated animate__pulse');
            }, function() {
                $(this).removeClass('animate__animated animate__pulse');
            }
        );
    </script>
@endsection