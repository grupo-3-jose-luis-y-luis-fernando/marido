<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>@yield('titulo')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('favicon/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('favicon/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('favicon/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('favicon/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('favicon/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('favicon/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('favicon/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('favicon/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicon/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('favicon/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('favicon/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('favicon/manifest.json') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ asset('favicon/ms-icon-144x144.png') }}">
    <meta name="theme-color" content="#ffffff">

    <link rel="shortcut icon" href="{{asset('images/favicon.ico')}}">
    <link href="{{ asset('css/panel/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/panel/icons.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/panel/style.css?v=1') }}" rel="stylesheet" type="text/css" /> 
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.6/css/responsive.dataTables.min.css"> 
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-TR9NYJV9GK"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-TR9NYJV9GK');
    </script>
    @yield('post_styles')
</head>
<body class="fixed-left">
    <div id="preloader">
        <div id="status">
            <div class="spinner"></div>
        </div>
    </div> 
    <div id="wrapper">
        <div class="left side-menu">
            <div class="topbar-left">
                <div class="">
                    <a href="{{route('administrador')}}" class="logo"><img src="{{asset('images/logo1.jpeg')}}" height="60" alt="logo"></a>
                </div>
            </div> 
            @include('includes.admin.menu') 
        </div>
        <div class="content-page">
            <div class="content">
                @include('includes.topbar')
                <div class="page-content-wrapper">
                    <div class="container-fluid">
                        @yield('contenido')
                    </div>
        
                </div>
            </div>
            @include('includes.footer')
        </div>
    </div>
    <script src="{{ asset('js/panel/jquery.min.js')}}"></script>
    <script src="{{ asset('js/panel/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('js/panel/modernizr.min.js')}}"></script>
    <script src="{{ asset('js/panel/jquery.slimscroll.js')}}"></script>
    <script src="{{ asset('js/panel/waves.js')}}"></script>
    <script src="{{ asset('js/panel/jquery.nicescroll.js')}}"></script>
    <script src="{{ asset('js/panel/jquery.scrollTo.min.js')}}"></script>  
    <script src="{{ asset('js/panel/app.js')}}"></script> 
    <script src="{{asset('js/panel/tablas.js?v='.uniqid())}}"></script>
    <script src="{{asset('js/panel/modal.js')}}"></script> 
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.6/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

    @yield('post_script')
</body>
</html>