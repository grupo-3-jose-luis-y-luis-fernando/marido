<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('titulo')</title>
    
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('favicon/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('favicon/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('favicon/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('favicon/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('favicon/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('favicon/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('favicon/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('favicon/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicon/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('favicon/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('favicon/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('favicon/manifest.json') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ asset('favicon/ms-icon-144x144.png') }}">
    <meta name="theme-color" content="#ffffff">

    <link href="{{asset('images/logo.jpeg')}}" rel="shortcut icon" type="image/vnd.microsoft.icon" />
    {{-- All CSS --}}
    <link rel="stylesheet" href="{{asset('css/web/bootstrap.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('css/web/font-awesome.min.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('css/web/icon-font.css')}}" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{asset('css/web/animate.min.css')}}">
    {{-- Revolution slider --}}
    <link rel="stylesheet" href="{{asset('css/web/settings.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('css/web/dynamic-captions.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('css/web/static-captions.css')}}" type="text/css" />
    {{-- Portfolio --}}
    <link rel="stylesheet" href="{{asset('css/web/featherlight.min.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('css/web/spsimpleportfolio.css')}}" type="text/css" />
    {{-- Owl Carousel --}}
    <link rel="stylesheet" href="{{asset('css/web/owl.carousel.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('css/web/owl.theme.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('css/web/owl.transitions.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('css/web/sp-flickr-gallery.css')}}" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{asset('css/web/style.css?v='.uniqid())}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    {{-- Jquery --}}
    <script src="{{asset('js/web/jQuery.js')}}" type="text/javascript"></script>
    <link rel="stylesheet" href="{{ asset('css/web/fancybox.css') }}">
    
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-TR9NYJV9GK"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-TR9NYJV9GK');
    </script>

    <!-- Google Tag Manager MARIDO-->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-WCVQJ95');</script>
    <!-- End Google Tag Manager --> 

    <!-- Google Tag Manager CYBAC-->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-TMX4H9C');</script>
    <!-- End Google Tag Manager -->
    
    
    
</head>
<body>
    <!-- Google Tag Manager (noscript) MARIDO -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WCVQJ95"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <!-- Google Tag Manager (noscript) CYBAC -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TMX4H9C"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
    <div class="body-innerwrapper"> 
        @include('includes.web.topbar')
        @include('includes.web.header')
        @yield('contenido')
        @include('includes.web.sidemenu')
        @include('includes.web.footer')
    </div>
    {{-- All Scripts --}}
    <script src="{{asset('js/web/bootstrap.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('js/web/appear.js')}}"></script>
    <script src="{{asset('js/web/jquery.themepunch.tools.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/web/jquery.themepunch.revolution.min.js')}}" type="text/javascript"></script>
    {{-- Portfolio --}}
    <script src="{{asset('js/web/jquery.shuffle.modernizr.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/web/featherlight.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/web/spsimpleportfolio.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/web/jquery.parallax-1.1.3.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/web/smoothscroll.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/web/owl.carousel.js')}}" type="text/javascript"></script> 
    <script src="{{asset('js/web/main.js')}}" type="text/javascript"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script src="{{ asset('js/web/fancybox.js') }}"></script>
    <script>
        AOS.init();
        Fancybox.bind("[data-fancybox]", {
        // Your options go here
        });
      </script>
    @yield('js')
</body>
</html>