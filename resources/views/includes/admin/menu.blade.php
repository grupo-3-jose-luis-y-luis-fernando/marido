<div class="sidebar-inner slimscrollleft">
    <div id="sidebar-menu">
        <ul>
            <li class="menu-title">Bienvenido</li>
            <li>
                <a href="{{route('administrador')}}" style="background-color: transparent">
                    <i class="mdi mdi-home"></i> 
                    <span class="mi-menu"> Inicio</span>
                </a>
            </li>
            <li class="has_sub">
                <a href="javascript:void(0);" style="background-color: transparent"><i class="mdi mdi-store"></i><span class="mi-menu"> Materiales <span class="pull-right"><i class="mdi mdi-chevron-right"></i></span> </span></a>
                <ul class="list-unstyled">
                    <li><a href="{{ route('root.unidades_medidas') }}"><i class="mdi mdi-ruler"></i>Unidades de medida</a></li>
                    <li><a href="{{ route('root.materiales') }}"><i class="mdi mdi-view-list"></i>Listado</a></li> 
                    <li><a href="{{ route('root.inventario') }}"><i class="mdi mdi-box-shadow"></i>Inventario</a></li>
                </ul>
            </li>
            <li class="has_sub">
                <a href="javascript:void(0);" style="background-color: transparent"><i class="mdi mdi-worker"></i><span class="mi-menu"> Servicios <span class="pull-right"><i class="mdi mdi-chevron-right"></i></span></span></a>
                <ul class="list-unstyled">
                    <li><a href="{{route('root.categorias')}}"><i class="mdi mdi-tag"></i>Categorias</a></li>
                    <li><a href="{{route('root.servicios')}}"><i class="mdi mdi-view-list"></i>Listado</a></li>
                    <li><a href="{{ route('analista.solicitudes') }}"><i class="mdi mdi-check-all"></i>Solicitudes</a></li>
                </ul>
            </li>
            <li>
                <a href="{{route('root.empleados')}}" style="background-color: transparent">
                    <i class="mdi mdi-account-multiple"></i> 
                    <span class="mi-menu"> Empleados</span>
                </a>
            </li>
            <li>
                <a href="{{route('root.proveedores')}}" style="background-color: transparent">
                    <i class="mdi mdi-truck"></i> 
                    <span class="mi-menu"> Proveedores</span>
                </a>
            </li>
            <li>
                <a href="{{route('root.clientes')}}" style="background-color: transparent">
                    <i class="mdi mdi-face"></i> 
                    <span class="mi-menu"> Clientes</span>
                </a>
            </li> 
            <li class="has_sub">
                <a href="javascript:void(0);" style="background-color: transparent"><i class="mdi mdi-store"></i><span class="mi-menu"> Configuraciones <span class="pull-right"><i class="mdi mdi-chevron-right"></i></span> </span></a>
                <ul class="list-unstyled">
                    <li><a href="{{route('root.conekta')}}"><i class="mdi mdi-tag"></i>Conexión Conekta</a></li> 
                </ul>
            </li>

            <li class="has_sub">
                <a href="javascript:void(0);" style="background-color: transparent">
                    <i class="mdi mdi-store"></i>
                    <span class="mi-menu"> Página Web 
                        <span class="pull-right">
                            <i class="mdi mdi-chevron-right"></i>
                        </span> 
                    </span>
                </a>
                <ul class="list-unstyled">
                    <li><a href="{{ route('root.slider') }}"><i class="mdi mdi-archive"></i>Slider</a></li>
                    <li><a href="{{ route('root.nosotros') }}"><i class="mdi mdi-information"></i>Nosotros</a></li>
                    <li><a href="{{ route('root.galeria') }}"><i class="mdi mdi-album"></i>Galería</a></li>
                </ul>
            </li>
        </ul>
    </div>
    <div class="clearfix"></div>
</div>