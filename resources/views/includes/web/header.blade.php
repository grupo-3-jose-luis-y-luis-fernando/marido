<header id="sp-header">
    <div class="container">
        <div class="row">
            <div id="sp-menu" class="col-sm-12 col-md-12">
                <div class="sp-column ">
                    <div class='sp-megamenu-wrapper'>
                        <a id="offcanvas-toggler" href="#"><i class="fa fa-bars"></i></a>
                        <ul class="sp-megamenu-parent menu-fade-up hidden-xs">
                            <li class="sp-menu-item current-item active">
                                <a href="{{route('index')}}">Inicio</a>
                            </li>
                            <li class="sp-menu-item">
                                <a href="#servicios">Servicios</a>
                            </li>
                            <li class="sp-menu-item">
                                <a href="#galeria">Galería</a>
                            </li>
                            <li class="sp-menu-item">
                                <a href="#nosotros">Acerca de</a>
                            </li>
                            <li class="sp-menu-item">
                                <a href="#app">Contactanos</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>