<section id="galeria" class="space bg-color">
    <div class="container">
        <!--Main Heading-->
        <div class="col-sm-12 no-padding main-heading text-center">
            <h2>Proyectos destacados</h2>
        </div>
        <div class="row">
            <div id="mod-sp-simpleportfolio">
                @foreach ($galeria as $item)
                <div class="service-block" data-aos="fade-up">
                    <img class="sppb-img-responsive" src="{{ asset('storage/galeria/'.$item->imagen) }}" alt="{{ $item->nombre }}" data-fancybox="fancybox" style="cursor: pointer;"> 
                    <h3 class="title">{{ $item->nombre }}</h3> 
                    <div>{{ $item->descripcion }}</div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</section>