<section id="sp-top-info">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 top-base no-padding">
                <div class="col-sm-6 top-block">
                    <p><i class="fa fa-map-marker"></i> Tercera Pte. Sur 334, San José Terán, 29057 Tuxtla Gutiérrez, Chis.</p>
                </div>
                <div class="col-sm-6 top-block text-right">
                    <ul class="social-icons">
                        <li><a target="_blank" href="https://www.facebook.com/MaridoTuxtla/"><i class="fa fa-facebook"></i></a></li> 
                        <li><a target="_blank" href="https://www.instagram.com/marido_multiservicios/"><i class="fa fa-instagram"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="sp-top-bar">
    <div class="container">
        <div class="row">
            <div id="sp-logo" class="col-sm-3 col-md-3">
                <a class="logo" href="{{route('index')}}"><h1><img class="sp-default-logo" src="{{asset('images/head_logo_marido.png')}}" alt="logo"></h1></a>
            </div>
            <div id="sp-top2" class="col-sm-9 col-md-9 hidden-xs">
                <ul class="sp-contact-info"> 
                    <li class="sp-contact-phone"><i class="icon-call-in"></i>
                        <p class="contact-content"> <span class="contact-title">Numero de contacto:</span> <span>(+52) 961-123-4567</span></p>
                    </li>
                    <li class="sp-contact-email"><i class="icon-envelope-letter"></i>
                        <p class="contact-content"> <span class="contact-title">Email:</span> <span>contacto@marido.com</span></p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>