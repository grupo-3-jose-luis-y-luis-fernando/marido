<section id="nosotros" class="space">
    <div class="container">
        <div class="row">
            <div class="about-block col-sm-5">
                <img class="sppb-img-responsive" src="{{ asset('storage/nosotros/'.$nosotros->imagen) }}" onerror="this.src='{{ asset('images/error-404.png') }}' " alt="nosotros"> 
            </div>
            <div class="about-block col-sm-7">
                <div class="heading">
                    <h2 class="title">¿Quiénes somos?</h2>
                    <p>{{ $nosotros->nosotros }}</p>
                </div>
                <div class="about-item">
                    <div class="col-sm-12 no-padding">
                        <div class="about-list">
                            <div class="pull-left">
                                <div class="sppb-icon">
                                    <span class="icon-badge"></span>
                                </div>
                            </div>
                            <div class="sppb-media-body">
                                <h4 class="title">Misión</h4>
                                <p>{{ $nosotros->mision }}</p>
                            </div>
                        </div> 
                    </div>
                    <div class="col-sm-12 no-padding">
                        <div class="about-list">
                            <div class="pull-left">
                                <div class="sppb-icon">
                                    <span class="icon-eye"></span>
                                </div>
                            </div>
                            <div class="sppb-media-body">
                                <h4 class="title">Visión</h4>
                                <p>{{ $nosotros->vision }}</p>
                            </div>
                        </div> 
                    </div>
                    <div class="col-sm-12 no-padding">
                        <div class="about-list">
                            <div class="pull-left">
                                <div class="sppb-icon">
                                    <span class="icon-trophy"></span>
                                </div>
                            </div>
                            <div class="sppb-media-body">
                                <h4 class="title">Valores</h4>
                                <p>{{ $nosotros->valores }}</p>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>