<section id="sp-bottom">
    <div class="container">
        <div class="row">

            <div id="sp-bottom1" class="col-sm-6 col-md-3">
                <a href="{{route('index')}}">
                    <img src="{{asset('images/footer_logomarido.png')}}">
                </a>
            </div>

            <div id="sp-bottom1" class="col-sm-6 col-md-3">
                <h3 class="sp-module-title">Datos de contacto</h3>
                <ul class="textwidget">
                    <li><i class="fa fa-map-marker"></i> Tercera Pte. Sur 334, San José Terán, 29057 Tuxtla Gutiérrez, Chis.</li>
                    <li><a href="tel:529612616240" target="_blank"><i class="fa fa-phone"></i>9612616240</a></li>
                    <li><a href="mailto:contacto@marido.mx"><i class="fa fa-envelope"></i>contacto@marido.mx</a></li> 
                </ul>
            </div>
            <div id="sp-bottom2" class="col-sm-6 col-md-3">
                <h3 class="sp-module-title">Enlaces rápidos</h3>
                <ul class="textwidget">
                    <li><i class="ti-arrow-right"></i><a href="#nosotros" target="_parent"> Nosotros</a></li>
                    <li><i class="ti-arrow-right"></i><a href="#servicios" target="_parent"> Servicios</a></li>
                    <li><i class="ti-arrow-right"></i><a href="#galeria" target="_parent"> Proyectos</a></li> 
                </ul>
            </div>
            <div id="sp-bottom3" class="col-sm-6 col-md-3">
                <h3 class="sp-module-title">Redes Sociales</h3>
                <div class="tagspopular">
                    <ul>
                        <li> <a href="https://www.facebook.com/MaridoTuxtla/" target="_blank">Facebook</a></li>
                        <li> <a href="https://www.instagram.com/marido_multiservicios/" target="_blank">Instagram</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!--footer copyrights-->
<footer id="sp-footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <a href="https://grupocybac.com" target="_blank" style="color: white;">Hosting &amp; Diseño <i class="fa fa-heart-o"></i> Grupo Cybac</a>
            </div>
        </div>
    </div>
</footer>