<section id="servicios" class="space">
    <div class="container">
        <div class="row" style="display: flex; flex-wrap:wrap;">
            <div class="col-sm-12 no-padding main-heading text-center">
                <h2>Nuestros Servicios</h2>
            </div>
            @foreach ($categorias as $item)
            <div class="service-block col-sm-4" data-aos="fade-up">
                <img class="sppb-img-responsive" src="{{ asset('storage/categorias/'.$item->imagen) }}" alt="{{ $item->nombre }}"> 
                <h3 class="title">{{ $item->nombre }}</h3> 
                <div>{{ $item->descripcion }}</div>
            </div>     
            @endforeach
        </div>
    </div>
</section>