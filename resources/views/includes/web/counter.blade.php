<section id="app" class="space blue-overlay parallax1">
    <div class="container">
        <div class="row">
            <div class="counter-heading col-sm-7">
                <div class="heading">
                    <h2 class="title">Ofrecemos a los mejores técnicos especializados 
                        <span> para hacer el servicio que necesites</span></h2>
                </div>
            </div>
            <div class="counter col-sm-5">
                <img class="img-responsive" src="{{asset('images/banner_appmarido02.png')}}" alt="Counter">
            </div>
        </div>
    </div>
</section>