<div class="offcanvas-menu">
    <a href="#" class="close-offcanvas"><i class="fa fa-remove"></i></a>
    <div class="offcanvas-inner">
        <div class="sp-module ">
            <h3 class="sp-module-title">Marido</h3>
            <div class="sp-module-content">
                <ul class="nav menu">
                    <li class="item-101 current active"><a href="{{route('index')}}">Inicio</a></li>
                    <li class="item-165 deeper parent"><a href="#">Servicios</a> 
                    </li>
                    <li class="item-111"><a href="#">Proyectos</a></li>
                    <li class="item-166"><a href="#">Acerca de</a></li>
                    <li class="item-110"><a href="#">Contact</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>