@extends('layouts.panel')
@section('titulo', "Empleados :: Marido Administrador")
@section('area',  $form_edit ? "Editar Empleado" : "Nuevo Empleado")

@section('contenido') 
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="alert alert-warning">
                    Los campos con <span class="text-danger">*</span> son requeridos
                    @if ($form_edit)
                    <p><strong>Nota:</strong> Si no desea cambiar la contraseña actual deje el campo contraseña vacío, de lo contrario se actualizará la contraseña.</p>
                    @endif
                </div>
                <form action="{{ $form_edit ? route('root.actualizar_empleado', $empleado->id) : route('root.guardar_empleado') }}" method="POST" class="form row" {{ $form_edit ? "data-alert=true" : "" }} data-archivos="true" id="form_empleado">
                    @csrf
                    <div class="form-group col-4 mx-auto">
                        <label for="foto">Foto</label>
                        @if ($form_edit)
                        <div class="text-center my-4">
                            <img src="{{ asset('storage/usuarios/'.$empleado->avatar) }}" alt="foto" class="col-6">
                        </div>
                        @endif
                        <input type="file" class="form-control" id="foto" name="foto">
                    </div>
                    <div class="col-12"></div>
                    <div class="form-group col-12 col-md-4">
                        <label for="nombre">Nombre <span class="text-danger">*</span></label>
                        <input type="text" class="form-control form-control-lg" name="nombre" id="nombre" value="{{ $form_edit ? $empleado->name : "" }}">
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group col-12 col-md-4">
                        <label for="paterno">Apellido Paterno <span class="text-danger">*</span></label>
                        <input type="text" class="form-control form-control-lg" name="paterno" id="paterno" value="{{ $form_edit ? $empleado->paterno : "" }}">
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group col-12 col-md-4">
                        <label for="materno">Apellido Materno <span class="text-danger">*</span></label>
                        <input type="text" class="form-control form-control-lg" name="materno" id="materno" value="{{ $form_edit ? $empleado->materno : "" }}">
                        <span class="invalid-feedback"></span>
                    </div>
                    
                    <div class="form-group col-12 col-md-4">
                        <label for="email">Correo </label>
                        <input type="email" class="form-control form-control-lg" name="email" id="email" value="{{ $form_edit ? $empleado->email : "" }}">
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group col-12 col-md-4">
                        <label for="password">Contraseña {!! !$form_edit ? '<span class="text-danger">*</span>' : ''!!}</label>
                        <input type="password" class="form-control form-control-lg" name="password" id="password" value="">
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group col-12 col-md-4">
                        <label for="rol">Rol <span class="text-danger">*</span></label>
                        <select name="rol" id="rol" class="form-control form-control-lg">
                            <option value="">Seleccione el rol</option>
                            @foreach ($roles as $item)
                                <option value="{{ $item->name }}" {{ $form_edit && $item->name == $empleado->getRoleNames()[0]  ? "selected" : "" }}>{{ $item->name }}</option>
                            @endforeach
                        </select>
                        <span class="invalid-feedback"></span>
                    </div>

                    <div class="form-group text-center col-12">
                        <button class="btn btn-success btn-lg mb-2 submit" type="submit"> {{ $form_edit ? "Actualizar" : "Guardar" }}</button>
                        <a href="{{ route('root.empleados') }}" class="btn btn-danger btn-lg mb-2 cancelar">Cancelar</a>
                    </div>
                   
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
