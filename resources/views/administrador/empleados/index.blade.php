@extends('layouts.panel')
@section('titulo', "Empleados :: Marido Administrador")
@section('area',  'Empleados')
@section('contenido')
<div>
    <div class="card w-100 p-5"> 
        <div class="w-100 text-right form-group">
            <a class="btn btn-success col-md-2" href="{{ route('root.crear_empleado') }}">Nuevo</a>
        </div>
        <table class="table text-center" id="dt_empleados" data-url="{{ route('root.dt_empleados') }}" style="width: 100%">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Correo</th>
                    <th>Rol</th>
                    <th>Acciones</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection