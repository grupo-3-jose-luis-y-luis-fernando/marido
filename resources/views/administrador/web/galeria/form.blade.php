<div class="modal-body">
    <form class="form" action="{{ $form_edit ? route('root.actualizar_galeria') : route('root.guardar_galeria') }}" method="POST" enctype="multipart/form-data" id="form_galeria" data-archivos="true">
        @csrf
        <div class="form-group">
            <label for="imagen">Imagen</label>
            @if ($form_edit)
            <input type="hidden" name="galeria" value="{{ $galeria->id }}">
                <img src="{{ asset('storage/galeria/'.$galeria->imagen) }}" alt="{{ $galeria->imagen }}" class="img-fluid">
            @endif
            <input type="file" class="form-control" id="image" name="imagen">
        </div>
        <div class="form-group">
            <button class="btn btn-success" type="submit">{{ $form_edit ? "Actualizar" : "Guardar"}}</button>
        </div>
    </form>
</div>