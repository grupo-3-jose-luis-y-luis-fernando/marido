@extends('layouts.panel')
@section('titulo','Galería :: Marido Administrador')

@section('contenido')
<div>
    <div class="card w-100 p-5"> 
        <form action="{{ route('root.crear_galeria') }}" class="w-100 text-right form-group form" method="POST">
            @csrf
            <button class="btn btn-success col-md-2">Nuevo</button>
        </form>
        <table class="table" id="dt_galeria" data-url="{{ route('root.dt_galeria') }}" style="width: 100%">
            <thead>
                <tr>
                    <th>Imagen</th>
                    <th>Acciones</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection