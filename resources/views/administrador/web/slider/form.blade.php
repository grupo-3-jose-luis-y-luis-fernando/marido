@extends('layouts.panel')
@section('titulo', 'Sliders :: Marido Administrador')
@section('area', $form_edit ? "Editar Slider" :"Nuevo Slider")
@section('contenido')
    <form class="form row" enctype="multipart/form-data" method="POST" action="{{ $form_edit ? route('root.actualizar_slider') : route('root.guardar_slider') }}" id="form_slider">
        @csrf
        <div class="col-12 col-md-7 mx-auto">
            <div class="form-group col-12">
                <label for="imagen">Imagen(1920px * 720px)<span class="text-danger">*</span></label>
                @if ($form_edit)
                <input type="hidden" name="slider" value="{{ $slider->id }}">
                <div class="col-8 mx-auto mb-2">
                    <img src="{{ asset('storage/slider/'.$slider->imagen) }}" alt="Slider {{ $slider->id }}" class="img-fluid">
                </div>
                @endif
                <input type="file" name="imagen" id="imagen" class="form-control">
                <span class="invalid-feedback"></span>
            </div>
            <div class="row"></div>
            <div class="form-group col-12">
                <label for="url">URL</label>
                <input type="text" name="url" id="url" class="form-control" value="{{ $form_edit ? $slider->url : '' }}">
            </div>
            <div class="form-group col-12 {{ $form_edit && $slider->url != "" ? "" : "d-none" }}">
                <label for="">¿Dónde se abre la acción de la url?</label>
                <select name="target" id="target" class="form-control">
                    <option value="1" {{ $form_edit && $slider->target==1 ? "selected" : "" }}>En otra ventana</option>
                    <option value="2" {{ $form_edit && $slider->target==2 ? "selected" : "" }}>En la misma ventana</option>
                </select>
            </div>
            <div class="form-group col-12 text-center">
                <button type="submit" class="btn btn-success">Guardar</button>
            </div>
        </div>
    </form> 
@endsection

@section('post_script')
<script>
    $("#url").on("change keyup",function(){
        console.log($(this).val().length);
        if($(this).val().length>0){
            $("#target").parent().removeClass("d-none");
        }else{
            $("#target").parent().addClass("d-none");
        }
    });
</script>
@endsection