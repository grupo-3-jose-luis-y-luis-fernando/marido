@extends('layouts.panel')
@section('titulo','Slider :: Marido Administrador')

@section('contenido')
<div>
    <div class="card w-100 p-5"> 
        <div class="w-100 text-right form-group">
            <a class="btn btn-success col-md-2" href="{{ route('root.crear_slider') }}">Nuevo</a>
        </div>
        <table class="table" id="dt_slider" data-url="{{ route('root.dt_slider') }}" style="width: 100%">
            <thead>
                <tr>
                    <th>Imagen</th>
                    <th>Acciones</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection