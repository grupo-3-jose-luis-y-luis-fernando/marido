@extends('layouts.panel')
@section('titulo', "Nosotros :: Marido Administrador")
@section('area', 'Nostros - Guardar información')
@section('contenido')
    <form action="{{ route('root.actualizar_nosotros') }}" class="form row" id="form_nosotros" data-archivos="true" enctype="multipart/form-data">
        @csrf
        <div class="form-group col-md-6 col-12">
            <label for="nosotros">¿Quiénes somos?</label>
            <textarea name="nosotros" class="form-control" id="nosotros" cols="30" rows="5">{{ $nosotros->nosotros ?? "" }}</textarea>
            <span class="invalid-feedback"></span>
        </div>
        <div class="form-group col-md-6 col-12">
            <label for="mision">Misión</label>
            <textarea name="mision" class="form-control" id="mision" cols="30" rows="5">{{ $nosotros->mision ?? "" }}</textarea>
            <span class="invalid-feedback"></span>
        </div>
        <div class="form-group col-md-6 col-12">
            <label for="vision">Visión</label>
            <textarea name="vision" class="form-control" id="vision" cols="30" rows="5">{{ $nosotros->vision ?? "" }}</textarea>
            <span class="invalid-feedback"></span>
        </div>
        <div class="form-group col-md-6 col-12">
            <label for="valores">Valores</label>
            <textarea name="valores" class="form-control" id="valores" cols="30" rows="5">{{ $nosotros->valores ?? "" }}</textarea>
            <span class="invalid-feedback"></span>
        </div>
        <div class="form-group col-md-6 col-12">
            <label for="imagen">Imagen(700 x 432)</label>
            @if (isset($nosotros->imagen))
            <img src="{{ asset('storage/nosotros/'.$nosotros->imagen) }}" alt="nosotros" class="img-fluid">
            @endif
            
            <input type="file" name="imagen" id="imagen" class="form-control">
            <span class="invalid-feedback"></span>
        </div>
        <div class="form-group col-12">
            <button type="submit" class="btn btn-success">Guardar</button>
        </div>
    </form>
@endsection