@extends('layouts.panel')
@section('titulo', 'Proveedores :: Marido Administrador')
@section('area', $form_edit ? "Editar Proveedor" :"Nuevo Proveedor")
@section('contenido') 
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="alert alert-warning"> Los campos con <span class="text-danger">*</span> son requeridos</div>
                <form action="{{ $form_edit ? route('root.actualizar_proveedor', $proveedor->id) : route('root.guardar_proveedor') }}"
                    method="POST" class="form row" {{ $form_edit ? "data-alert=true" : "" }}>
                    @csrf
                    <div class="form-group col-12 col-md-4">
                        <label for="nombre">Nombre <span class="text-danger">*</span></label>
                        <input type="text" class="form-control form-control-lg" name="nombre" id="nombre" value="{{ $form_edit ? $proveedor->nombre : "" }}">
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group col-12 col-md-8">
                        <label for="direccion">Dirección <span class="text-danger">*</span></label>
                        <input type="text" class="form-control form-control-lg" name="direccion" id="direccion" value="{{ $form_edit ? $proveedor->direccion : "" }}">
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group col-12 col-md-4">
                        <label for="rfc">RFC <span class="text-danger">*</span></label>
                        <input type="text" class="form-control form-control-lg" name="rfc" id="rfc" value="{{ $form_edit ? $proveedor->rfc : "" }}">
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group col-12 col-md-4">
                        <label for="telefono">Teléfono <span class="text-danger">*</span></label>
                        <input type="text" class="form-control form-control-lg" name="telefono" id="telefono" value="{{ $form_edit ? $proveedor->telefono : "" }}">
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group col-12 col-md-4">
                        <label for="email">Correo </label>
                        <input type="email" class="form-control form-control-lg" name="email" id="email" value="{{ $form_edit ? $proveedor->email : "" }}">
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group text-center col-12">
                        <button class="btn btn-success btn-lg mb-2 submit" type="submit"> {{ $form_edit ? "Actualizar" : "Guardar" }}</button>
                        <a href="{{ route('root.proveedores') }}" class="btn btn-danger btn-lg mb-2 cancelar">Cancelar</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="{{asset('js/numeros.js')}}"></script>
@endsection
