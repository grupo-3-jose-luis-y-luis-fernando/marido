@extends('layouts.panel')
@section('titulo', "Proveedores :: Marido Administrador")
@section('area',  'Proveedores')
@section('contenido')
<div>
    <div class="card w-100 p-5">
        <div class="w-100 text-right form-group">
            <a class="btn btn-success col-md-2" href="{{ route('root.crear_proveedor') }}">Nuevo</a>
        </div>
        <table class="table" id="dt_proveedores" data-url="{{ route('root.dt_proveedores') }}" style="width: 100%">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>RFC</th>
                    <th>Dirección</th>
                    <th>Correo</th>
                    <th>Teléfono</th>
                    <th>Acciones</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection