@extends('layouts.panel')
@section('titulo', "Conexión Conekta :: Marido Administrador")
@section('area', 'Conexión Conekta')
@section('contenido')
    <form class="form" action="{{ route('root.guardar_conekta') }}" method="POST" data-alert="true">
        <div class="col-6 mx-auto mt-5">
            @csrf
            <div class="form-group">
                <label for="llave_publica">Llave Pública</label>
                <input type="text" class="form-control" id="llave_publica" name="llave_publica" value="{{ $conekta->puk_conekta ?? "" }}">
                <span class="invalid-feedback"></span>
            </div>

            <div class="form-group">
                <label for="llave_privada">Llave Privada</label>
                <input type="text" class="form-control" id="llave_privada" name="llave_privada" value="{{ $conekta->pvk_conekta ?? "" }}">
                <span class="invalid-feedback"></span>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-success">Guardar</button>
            </div>
        </div>
        
    </form>
@endsection