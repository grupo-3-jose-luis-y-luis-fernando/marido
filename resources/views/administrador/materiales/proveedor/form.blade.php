<form class="form" action="{{ $form_edit ? route('root.actualizar_material_proveedor',$material->id) : route('root.guardar_material_proveedor',$material->id) }}">
    @csrf
    <div class="modal-header">
        <h5 class="modal-title">{{ $form_edit ? "Editando precios del material ".$material->nombre : "Agregar proveedor a ".$material->nombre }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body"> 
        @if (!$form_edit)
        <div class="form-group">
            <label for="proveedor">Proveedor</label>
            <select id="proveedor" name="proveedor" class="form-control">
                <option value="">-- Seleccione un proveedor --</option>
                @foreach ($proveedores as $item)
                    <option value="{{ $item->id }}">{{ $item->nombre }}</option>
                @endforeach
            </select>
            <span class="invalid-feedback"></span>
        </div>  
        @else
            <h6 class="text-center mt-0">{{ $proveedor->nombre }}</h6>  
            <input type="hidden" name="proveedor" value="{{ $proveedor->id }}">
        @endif
        <div class="form-group">
            <label for="precio">Precio del proveedor</label>
            <input type="text" name="precio" id="precio" class="form-control onlynumber" value="{{ $form_edit ? $material_proveedor->precio : "" }}">
            <span class="invalid-feedback"></span>
        </div>
        <div class="form-group">
            <label for="precio_publico">Precio para el público</label>
            <input type="text" name="precio_publico" id="precio_publico" class="form-control onlynumber" value="{{ $form_edit ? $material_proveedor->precio_publico : "" }}">
            <span class="invalid-feedback"></span>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">{{ $form_edit ? "Actualizar" : "Guardar" }}</button>
    </div>
</form>