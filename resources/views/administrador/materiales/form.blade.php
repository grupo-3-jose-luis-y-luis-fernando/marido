<form class="form" action="{{ $form_edit ? route('root.actualizar_material') : route('root.guardar_material') }}" method="POST">
    @csrf
    <div class="modal-header">
        <h5 class="modal-title">{{ $form_edit ? "Editar material" : "Crear material" }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        @if ($form_edit)
            <input type="hidden" name="material_id" value="{{ $material->id }}">
        @endif
        <div class="form-group">
            <label for="unidad">Unidad de medida</label>
            <select name="unidad" id="unidad" class="form-control">
                <option value="">--Seleccionar unidad de medida--</option>
                @foreach ($unidades as $item)
                    <option value="{{ $item->id }}" {{ $form_edit && $material->unidad_id == $item->id ? "selected" : "" }}>{{ $item->nombre }}</option>
                @endforeach
            </select>
            <span class="invalid-feedback"></span>
        </div>
        <div class="form-group">
            <label for="material">Material</label>
            <input type="text" name="material" class="form-control" id="material" value="{{ $form_edit ? $material->nombre : "" }}">
            <span class="invalid-feedback"></span>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">{{ $form_edit ? "Actualizar" : "Guardar" }}</button>
    </div>
</form>