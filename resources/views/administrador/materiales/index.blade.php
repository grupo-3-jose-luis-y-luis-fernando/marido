@extends('layouts.panel')
@section('titulo', 'Materiales :: Marido Administrador')
@section('area',  'Materiales')
@section('contenido')
<div>
    <div class="card w-100 p-5"> 
        <div class="w-100 text-right form-group">
            <form class="form" action="{{ route('root.crear_material') }}" method="POST">
                @csrf
                <button type="submit" class="btn btn-success col-md-2">Nuevo</button>
            </form>
        </div>
        <table class="table text-center" id="dt_materiales" data-url="{{ route('root.dt_materiales') }}" style="width: 100%">
            <thead>
                <tr>
                    <th>Nombre</th> 
                    <th>Unidad de medida</th>
                    <th>Proveedores</th>
                    <th>Acciones</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection