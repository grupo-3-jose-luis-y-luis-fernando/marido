@extends('layouts.panel')
@section('titulo', 'Inventario :: Marido Administrador')
@section('area',  'Inventario')
@section('contenido')
<div>
    <div class="card w-100 p-5">
        <table class="table text-center" id="dt_inventario" data-url="{{ route('root.dt_inventario') }}" style="width: 100%">
            <thead>
                <tr>
                    <th>Nombre</th> 
                    <th>Unidad de medida</th>
                    <th>Cantidad disponible</th>
                    <th>Acciones</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection