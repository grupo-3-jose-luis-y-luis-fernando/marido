<div class="modal-header">
    <h4 class="modal-title">{{ $material->nombre }}</h4>
</div>
<form class="modal-body form">
    <div class="form-group input-group mb-3">
        <label for="cantidad" class="w-100">Cantidad a ingresar</label>
        <input type="number" class="form-control onlynumber" id="cantidad">
        <div class="input-group-append">
            <span class="input-group-text" id="basic-addon2">{{ $material->unidad }}</span>
        </div>
    </div>
    <div class="form-group">
        <button class="btn btn-success" type="submit">Ingresar</button>
    </div>
</form>