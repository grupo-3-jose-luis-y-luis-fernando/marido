@extends('layouts.panel')
@section('titulo', 'Unidades de medida :: Marido Administrador')
@section('area',  'Unidades de medida')
@section('contenido')
<div>
    <div class="card w-100 p-5"> 
        <div class="w-100 text-right form-group">
            <form class="form" action="{{ route('root.crear_unidad_medida') }}" method="POST">
                @csrf
                <button type="submit" class="btn btn-success col-md-2">Nuevo</button>
            </form>
        </div>
        <table class="table text-center" id="dt_unidades_medidas" data-url="{{ route('root.dt_unidades_medidas') }}" style="width: 100%">
            <thead>
                <tr>
                    <th>Nombre</th> 
                    <th>Acciones</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection