<form class="form" action="{{ $form_edit ? route('root.actualizar_unidad_medida') : route('root.guardar_unidad_medida') }}">
    @csrf
    <div class="modal-header">
        <h5 class="modal-title">{{ $form_edit ? "Editar unidad de medida" : "Crear unidad de medida" }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        @if ($form_edit)
            <input type="hidden" name="unidad_id" value="{{ $unidad->id }}">
        @endif
        <div class="form-group">
            <label for="unidad">Unidad de medida</label>
            <input type="text" name="unidad" class="form-control" id="unidad" value="{{ $form_edit ? $unidad->nombre : "" }}">
            <span class="invalid-feedback"></span>
        </div>
        <div class="form-group">
            <label for="abreviatura">Abreviatura</label>
            <input type="text" name="abreviatura" class="form-control" id="abreviatura" value="{{ $form_edit ? $unidad->abreviatura : "" }}">
            <span class="invalid-feedback"></span>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">{{ $form_edit ? "Actualizar" : "Guardar" }}</button>
    </div>
</form>