@extends('layouts.panel')
@section('titulo', 'Servicios :: Marido Administrador')
@section('area', $form_edit ? "Editar Servicio" :"Nuevo Servicio")
@section('contenido') 
<div class="row"> 
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="alert alert-danger"> 
                    Nota: Estos servicios son los conceptos generales que le aparecerán a los clientes, es decir, el nombre común del servicio.<br>
                    Los campos con <span class="text-danger">*</span> son requeridos.
                </div>
                <form action="{{ $form_edit ? route('root.actualizar_servicio', $servicio->id) : route('root.guardar_servicio') }}" method="POST" class="form row" {{ $form_edit ? "data-alert=true" : "" }}>
                    @csrf
                    <div class="form-group col-12 col-md-4">
                        <label for="nombre">Nombre<span class="text-danger">*</span></label>
                        <input type="text" class="form-control form-control-lg" name="nombre" id="nombre" value="{{ $form_edit ? $servicio->nombre : "" }}">
                        <span class="invalid-feedback"></span>
                    </div> 
                    <div class="form-group col-12 col-md-4">
                        <label for="agendado">Costo Programado <span class="text-danger">*</span></label>
                        <input type="number" class="form-control form-control-lg" name="agendado" id="agendado" min="0" step=".01" value="{{ $form_edit ? $servicio->costo_agendado : "0" }}">
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group col-12 col-md-4">
                        <label for="urgente">Costo Urgente <span class="text-danger">*</span></label>
                        <input type="number" class="form-control form-control-lg" name="urgente" id="urgente" min="0" step=".01" value="{{ $form_edit ? $servicio->costo_urgente : "0" }}">
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group col-12 col-md-4">
                        <label for="categoria">Categoría <span class="text-danger">*</span></label>
                        <select name="categoria" id="categoria" class="form-control form-control-lg">
                            <option value="">Selecciona la categoría</option>
                            @foreach ($categorias as $item)
                                <option value="{{ $item->id }}" {{ $form_edit && $item->id == $servicio->categoria_id  ? "selected" : "" }}>{{ $item->nombre }}</option>
                            @endforeach
                        </select>
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group col-12">
                        <label for="descripcion">Descripción <span class="text-danger">*</span></label>
                        <textarea class="form-control form-control-lg" name="descripcion" id="descripcion" rows="6">{{ $form_edit ? $servicio->descripcion : "" }}</textarea>
                        <span class="invalid-feedback"></span>
                    </div>
                    
                    <div class="form-group text-center col-12">
                        <button class="btn btn-success btn-lg mb-2 submit" type="submit"> {{ $form_edit ? "Actualizar" : "Guardar" }}</button>
                        <a href="{{ route('root.servicios') }}" class="btn btn-danger btn-lg mb-2 cancelar">Cancelar</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
