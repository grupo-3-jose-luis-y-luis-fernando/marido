@extends('layouts.panel')
@section('titulo', 'Servicios Conceptos :: Marido Administrador')
@section('area', 'Conceptos de '.$servicio->nombre)
@section('contenido')
<div>
    <div class="card w-100 p-5"> 
        <div class="w-100 text-right form-group">
            <a class="btn btn-danger" href="{{ route('root.servicios') }}">Volver a servicios</a>
            <a class="btn btn-success" href="{{ route('root.crear_concepto',$servicio->id) }}">Nuevo concepto</a>
        </div>
        <table class="table" id="dt_conceptos" data-url="{{ route('root.dt_conceptos',$servicio->id) }}" style="width: 100%">
            <thead>
                <tr>
                    <th>Clave</th>
                    <th>Nombre</th> 
                    <th>Costo</th> 
                    <th>Acciones</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection