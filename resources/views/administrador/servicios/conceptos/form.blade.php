@extends('layouts.panel')
@section('titulo', 'Conceptos :: Marido Administrador')
@section('area', $form_edit ? "Editar Concepto" :"Nuevo Concepto")
@section('contenido') 
<div class="row"> 
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="alert alert-danger"> 
                    Nota: Estos servicios son los conceptos de cada servicio general, estos les aparecerán al técnico, al residente y al analista.<br>
                    Los campos con <span class="text-danger">*</span> son requeridos.
                </div>
                <form action="{{ $form_edit ? route('root.actualizar_concepto',['id_servicio'=>$servicio_id,'id_concepto'=>$concepto->id]) : route('root.guardar_concepto',$servicio_id) }}" class="form row">
                    @csrf 
                    <div class="form-group col-12 col-md-4">
                        <label for="clave">Clave</label>
                        <input type="text" class="form-control form-control-lg" name="clave" id="clave" value="{{ $form_edit ? $concepto->clave : "" }}">
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group col-12 col-md-8">
                        <label for="nombre">Nombre</label>
                        <input type="text" class="form-control form-control-lg" name="nombre" id="nombre" value="{{ $form_edit ? $concepto->nombre : "" }}">
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group col-12 col-md-4">
                        <label for="agendado">Costo Programado <span class="text-danger">*</span></label>
                        <input type="text" class="form-control form-control-lg onlynumber" name="agendado" id="agendado" min="0" value="{{ $form_edit ? $concepto->costo_agendado : "0" }}">
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group col-12 col-md-4">
                        <label for="urgente">Costo Urgente <span class="text-danger">*</span></label>
                        <input type="text" class="form-control form-control-lg onlynumber" name="urgente" id="urgente" min="0" value="{{ $form_edit ? $concepto->costo_urgente : "0" }}">
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group text-center col-12">
                        <button class="btn btn-success btn-lg mb-2 submit" type="submit"> {{ $form_edit ? "Actualizar" : "Guardar" }}</button>
                        <a href="{{ route('root.conceptos',$servicio_id) }}" class="btn btn-danger btn-lg mb-2 cancelar">Cancelar</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
