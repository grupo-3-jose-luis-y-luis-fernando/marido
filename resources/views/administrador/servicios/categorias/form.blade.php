@extends('layouts.panel')
@section('titulo', "Categorías :: Marido Administrador")
@section('area', $form_edit ? "Editar categoría" : "Crear categoría")
@section('contenido')
<form class="form" action="{{ $form_edit ? route('root.actualizar_categoria', $categoria->id) : route('root.guardar_categoria') }}" enctype="multipart/form-data" id="form_categoria" data-archivos="true">
    @csrf
    <div class="modal-header">
        <h5 class="modal-title">{{ $form_edit ? "Editar categoría" : "Crear categoría" }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="form-group">
            <label for="categoria">Nombre</label>
            <input type="text" name="categoria" class="form-control" id="categoria" value="{{ $form_edit ? $categoria->nombre : "" }}">
            <span class="invalid-feedback"></span>
        </div>
        <div class="form-group">
            <label for="costo_mano_obra">% del costo de mano de obra <i class="fa fa-question" data-toggle="tooltip" data-placement="top" title="Es el porcentaje de la cotización total que se cobrará como mano de obra"></i></label>
            <input type="text" name="costo_mano_obra" class="form-control" id="costo_mano_obra" value="{{ $form_edit ? $categoria->costo_mano_obra : "" }}">
            <span class="invalid-feedback"></span>
        </div>
        <div class="form-group">
            @if ($form_edit)
                <img src="{{ asset('storage/categorias/'.$categoria->imagen) }}" alt="{{ $categoria->nombre }}">
            @endif
            <label for="imagen">Imagen(600 x 400)</label>
            <input type="file" class="form-control" name="imagen" id="imagen">
            <span class="invalid-feedback"></span>
        </div>
    </div>
    <div class="modal-footer">
        <a href="{{ route('root.categorias') }}" class="btn btn-secondary text-white" >Cerrar</a>
        <button type="submit" class="btn btn-primary">{{ $form_edit ? "Actualizar" : "Guardar" }}</button>
    </div>
</form>    
@endsection
