@extends('layouts.panel')
@section('titulo', "Categorías :: Marido Administrador")
@section('area', 'Categorías')
@section('contenido')
<div>
    <div class="card w-100 p-5"> 
        <div class="w-100 text-right form-group"> 
            <a href="{{ route('root.crear_categoria') }}" class="btn btn-success col-md-2 input-text">Nueva</a> 
        </div>
        <table class="table" id="dt_categorias" data-url="{{ route('root.dt_categorias') }}" style="width: 100%">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Porcentaje de mano de obra</th>
                    <th>Imagen</th>
                    <th>Acciones</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection