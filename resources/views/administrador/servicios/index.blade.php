@extends('layouts.panel')
@section('titulo', 'Servicios :: Marido Administrador')
@section('area', 'Servicios Generales')
@section('contenido')
<div>
    <div class="card w-100 p-5"> 
        <div class="w-100 text-right form-group">
            <a class="btn btn-success col-md-2" href="{{ route('root.crear_servicio') }}">Nuevo</a>
        </div>
        <table class="table" id="dt_servicios" data-url="{{ route('root.dt_servicios') }}" style="width: 100%">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Categoría</th>
                    <th>Costo</th>
                    <th>Descripción</th>
                    <th>Acciones</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection