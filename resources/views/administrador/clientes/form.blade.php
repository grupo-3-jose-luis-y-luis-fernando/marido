@extends('layouts.panel')
@section('titulo',  "Cliente :: Marido Administrador")
@section('area', "Editar cliente")
@section('contenido') 
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="alert alert-warning"> Los campos con <span class="text-danger">*</span> son requeridos</div>
                <form action="{{ route('root.actualizar_cliente', $cliente->id) }}" method="POST" class="form row" data-alert=true>
                    @csrf
                    <div class="form-group col-12 col-md-4">
                        <label for="nombre">Nombre <span class="text-danger">*</span></label>
                        <input type="text" class="form-control form-control-lg" name="nombre" id="nombre" value="{{ $cliente->name }}">
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group col-12 col-md-4">
                        <label for="paterno">Apellido paterno <span class="text-danger">*</span></label>
                        <input type="text" class="form-control form-control-lg" name="paterno" id="paterno" value="{{ $cliente->paterno }}">
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group col-12 col-md-4">
                        <label for="materno">Apellido materno <span class="text-danger">*</span></label>
                        <input type="text" class="form-control form-control-lg" name="materno" id="materno" value="{{ $cliente->materno }}">
                        <span class="invalid-feedback"></span>
                    </div>
                    <div class="form-group col-12 col-md-4">
                        <label for="rfc">RFC </label>
                        <input type="text" class="form-control form-control-lg" name="rfc" id="rfc" value="{{ $cliente->rfc }}">
                        <span class="invalid-feedback"></span>
                    </div>
                    
                    <div class="form-group text-center col-12">
                        <button class="btn btn-success btn-lg mb-2 submit" type="submit">Guardar cambios</button>
                        <a href="{{ route('root.clientes') }}" class="btn btn-danger btn-lg mb-2 cancelar">Cancelar</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
