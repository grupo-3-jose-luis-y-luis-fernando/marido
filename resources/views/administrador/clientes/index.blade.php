@extends('layouts.panel')
@section('titulo', "Clientes :: Marido Administrador")
@section('area', 'Clientes')
@section('contenido')
<div>
    <div class="card w-100 p-5">
        <table class="table" id="dt_clientes" data-url="{{ route('root.dt_clientes') }}" style="width: 100%">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Dirección</th>
                    <th>RFC</th> 
                    <th>Acciones</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection