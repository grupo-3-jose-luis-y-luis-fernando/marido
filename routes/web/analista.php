<?php

use Illuminate\Support\Facades\Route;

Route::name('analista.')->prefix('analista_marido')->middleware(['auth', 'role:analista|root'])->group(function () {
    Route::get('/', 'HomeController@analista')->name('index');
    Route::namespace('Analista')->prefix('solicitudes')->group(function () {
        Route::get('index','SolicitudesController@index')->name('solicitudes');
        Route::post('datatable','SolicitudesController@datatable')->name('dt_solicitudes');
        Route::get('asignar/{solicitud_id}','SolicitudesController@editar')->name('editar_solicitud');
        Route::post('asignados','SolicitudesController@asignados')->name('asignados'); //Obtiene todas las solicitudes asignadas al técnico
        Route::post('confirmar','SolicitudesController@asignar')->name('asignar_solicitud');
        Route::get('ver/{solicitud_id}','SolicitudesController@ver')->name('ver_solicitud');
        Route::post('cancelar','SolicitudesController@cancelar')->name('cancelar');
        Route::prefix('{solicitud_id}/cotizacion')->group(function () {
            Route::get('/','CotizacionesController@index')->name('cotizacion');
            Route::post('guardar','CotizacionesController@guardar')->name('guardar_cotizacion');
            Route::post('previsualizacion','CotizacionesController@previsualizacion')->name('previsualizacion_cotizacion');
            Route::get('previsualizacion','CategoriasController@previsualizar')->name('cotizacion_previa');
        });
    });
});