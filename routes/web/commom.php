<?php
 
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

Route::get('/', 'WebController@index')->name('index');

Route::get('storage-link', function(){
    Artisan::call("storage:link");
    return "Storage Link activado";
});