<?php
//Rutas de inicio de sesión

use Illuminate\Support\Facades\Route;

Route::get('accesomarido', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('accesomarido', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

//Rutas de registro
Route::get('registromarido', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('registromarido', 'Auth\RegisterController@register');

//Rutas de Reseteo de Password
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');

//Rutas de Verificacion de Email
Route::emailVerification();

Route::get('control_marido', 'HomeController@index')->name('administrador');