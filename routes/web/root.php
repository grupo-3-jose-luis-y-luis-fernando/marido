<?php

use Illuminate\Support\Facades\Route; 

Route::namespace('Administrativo')->name('root.')->prefix('control_marido')->middleware(['auth', 'role:root|analista'])->group(function () { 
    Route::prefix('categorias')->group(function () {
        Route::get('/', 'CategoriasController@index')->name('categorias');
        Route::post('datatable','CategoriasController@datatable')->name('dt_categorias');
        Route::get('crear','CategoriasController@crear')->name('crear_categoria');
        Route::post('guardar','CategoriasController@guardar')->name('guardar_categoria');
        Route::get('editar/{id_categoria}','CategoriasController@editar')->name('editar_categoria');
        Route::post('actualizar/{id_categoria}','CategoriasController@actualizar')->name('actualizar_categoria');
        Route::post('eliminar/{id_categoria}','CategoriasController@eliminar')->name('eliminar_categoria');
    });
    Route::prefix('servicios')->group(function () {
        Route::get('/', 'ServiciosController@index')->name('servicios');
        Route::post('datatable','ServiciosController@datatable')->name('dt_servicios');
        Route::get('crear','ServiciosController@crear')->name('crear_servicio');
        Route::get('editar/{id_servicio}','ServiciosController@editar')->name('editar_servicio');
        Route::post('guardar','ServiciosController@guardar')->name('guardar_servicio');
        Route::post('actualizar/{id_servicio}','ServiciosController@actualizar')->name('actualizar_servicio');
        Route::post('eliminar/{id_servicio}','ServiciosController@eliminar')->name('eliminar_servicio');
        Route::prefix('{id_servicio}/conceptos')->group(function () {
            Route::get('/','ServiciosConceptosController@index')->name('conceptos');
            Route::post('datatable','ServiciosConceptosController@datatable')->name('dt_conceptos');
            Route::get('crear','ServiciosConceptosController@crear')->name('crear_concepto');
            Route::get('editar/{id_concepto}','ServiciosConceptosController@editar')->name('editar_concepto');
            Route::post('guardar','ServiciosConceptosController@guardar')->name('guardar_concepto');
            Route::post('actualizar/{id_concepto}','ServiciosConceptosController@actualizar')->name('actualizar_concepto');
            Route::post('eliminar/{id_concepto}','ServiciosConceptosController@eliminar')->name('eliminar_concepto');
        });
    }); 
    Route::prefix('proveedores')->group(function () {
        Route::get('/', 'ProveedoresController@index')->name('proveedores');
        Route::post('datatable','ProveedoresController@datatable')->name('dt_proveedores');
        Route::get('crear','ProveedoresController@crear')->name('crear_proveedor');
        Route::get('editar/{id_proveedor}','ProveedoresController@editar')->name('editar_proveedor');
        Route::post('guardar','ProveedoresController@guardar')->name('guardar_proveedor');
        Route::post('actualizar/{id_proveedor}','ProveedoresController@actualizar')->name('actualizar_proveedor');
        Route::post('eliminar/{id_proveedor}','ProveedoresController@eliminar')->name('eliminar_proveedor');
    }); 
    Route::prefix('empleados')->group(function () {
        Route::get('/', 'EmpleadosController@index')->name('empleados');
        Route::post('datatable','EmpleadosController@datatable')->name('dt_empleados');
        Route::get('crear','EmpleadosController@crear')->name('crear_empleado');
        Route::get('editar/{id_empleado}','EmpleadosController@editar')->name('editar_empleado');
        Route::post('guardar','EmpleadosController@guardar')->name('guardar_empleado');
        Route::post('actualizar/{id_empleado}','EmpleadosController@actualizar')->name('actualizar_empleado');
        Route::post('eliminar/{id_empleado}','EmpleadosController@eliminar')->name('eliminar_empleado');
    }); 
    Route::prefix('clientes')->group(function () {
        Route::get('/', 'ClientesController@index')->name('clientes');
        Route::post('datatable','ClientesController@datatable')->name('dt_clientes');
        Route::get('editar/{id_cliente}','ClientesController@editar')->name('editar_cliente');
        Route::post('actualizar/{id_cliente}','ClientesController@actualizar')->name('actualizar_cliente');
        Route::post('eliminar/{id_cliente}','ClientesController@eliminar')->name('eliminar_cliente');
    }); 
    Route::prefix('materiales')->group(function(){
        Route::get('/','MaterialesController@index')->name('materiales');
        Route::post('datatable','MaterialesController@datatable')->name('dt_materiales');
        Route::post('crear','MaterialesController@crear')->name('crear_material');
        Route::post('guardar','MaterialesController@guardar')->name('guardar_material');
        Route::post('editar/{id_material}','MaterialesController@editar')->name('editar_material');
        Route::post('actualizar','MaterialesController@actualizar')->name('actualizar_material');
        Route::post('eliminar','MaterialesController@eliminar')->name('eliminar_material');
        Route::post('buscar','MaterialesController@buscar')->name('buscar_material'); //Busca los materiales para agregar a la cotización
        Route::post('agregar','MaterialesController@agregar')->name('agregar_material'); //Agrega un material a la lista de materiales de la cotización
        Route::prefix('unidades-medidas')->group(function(){
            Route::get('/','UnidadesMedidasController@index')->name('unidades_medidas');
            Route::post('datatable','UnidadesMedidasController@datatable')->name('dt_unidades_medidas');
            Route::post('crear','UnidadesMedidasController@crear')->name('crear_unidad_medida');
            Route::post('guardar','UnidadesMedidasController@guardar')->name('guardar_unidad_medida');
            Route::post('editar/{id_unidad_medida}','UnidadesMedidasController@editar')->name('editar_unidad_medida');
            Route::post('actualizar','UnidadesMedidasController@actualizar')->name('actualizar_unidad_medida');
            Route::post('eliminar','UnidadesMedidasController@eliminar')->name('eliminar_unidad_medida');
        });
        Route::prefix('{id_material}/proveedores')->group(function(){
            Route::post('/','MaterialesController@proveedor')->name('material_proveedor');
            Route::post('guardar','MaterialesController@guardar_proveedor')->name('guardar_material_proveedor');
            Route::post('editar/{id_proveedor}','MaterialesController@editar_proveedor')->name('editar_material_proveedor');
            Route::post('actualizar','MaterialesController@actualizar_proveedor')->name('actualizar_material_proveedor');
            Route::post('eliminar/{id_proveedor}','MaterialesController@eliminar_proveedor')->name('eliminar_material_proveedor');
        });


        Route::prefix('inventario')->group(function(){
            Route::get('/','InventarioController@index')->name('inventario');
            Route::post('datatable','InventarioController@datatable')->name('dt_inventario');
            Route::post('crear','InventarioController@crear')->name('crear_inventario');
            Route::post('guardar','InventarioController@guardar')->name('guardar_inventario');
        });
    });

    Route::prefix('configuraciones')->group(function(){
        Route::get('conexion-conekta','ConfiguracionesController@index_conekta')->name('conekta');
        Route::post('guardar-conekta','ConfiguracionesController@guardar_conekta')->name('guardar_conekta');
    });

    Route::prefix('slider')->group(function(){
        Route::get('/','SliderController@index')->name('slider');
        Route::post('datatable','SliderController@datatable')->name('dt_slider');
        Route::get('crear','SliderController@crear')->name('crear_slider');
        Route::post('guardar','SliderController@guardar')->name('guardar_slider');
        Route::get('editar/{id_slider}','SliderController@editar')->name('editar_slider');
        Route::post('actualizar','SliderController@actualizar')->name('actualizar_slider');
        Route::post('eliminar','SliderController@eliminar')->name('eliminar_slider');
    });
    
    Route::prefix('nosotros')->group(function(){
        Route::get('/','NosotrosController@index')->name('nosotros');
        Route::post('actualizar','NosotrosController@actualizar')->name('actualizar_nosotros');
    });

    Route::prefix('galeria')->group(function(){
        Route::get('/','GaleriaController@index')->name('galeria');
        Route::post('crear','GaleriaController@crear')->name('crear_galeria');
        Route::post('guardar','GaleriaController@guardar')->name('guardar_galeria');
        Route::post('editar/{id_galeria}','GaleriaController@editar')->name('editar_galeria');
        Route::post('actualizar','GaleriaController@actualizar')->name('actualizar_galeria');
        Route::post('eliminar','GaleriaController@eliminar')->name('eliminar_galeria');
        Route::post('datatable','GaleriaController@datatable')->name('dt_galeria');
    });
});
