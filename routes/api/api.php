<?php

use Illuminate\Support\Facades\Route;

Route::namespace('Api')->group(function(){
    Route::post('login', 'AuthController@login');
    Route::post('register', 'AuthController@register');
    Route::group(['middleware' => 'auth:api'], function() {
        Route::post('perfil','AuthController@perfil'); //Retorna los datos del perfil del usuario
        Route::post('actualizar-perfil','AuthController@actualizar'); //Actualizar perfil del usuario
        Route::post('servicios','ApiServiciosController@index'); //Retorna todos los servicios al cliente
        Route::post('categorias','ApiServiciosController@categorias'); //Retorna todas las categorías de los servicios
        Route::post('crear-solicitud','ApiServiciosController@crear_solicitud'); //Crea una solicitud de servicio por parte del cliente
        Route::post('guardar-direccion','ApiClientesController@agregar_direccion'); //Guarda o actualiza la dirección de un cliente
        Route::post('actualizar-direccion','ApiClientesController@actualizar_direccion'); //Actualiza una dirección de un cliente
        Route::post('direcciones','ApiClientesController@direcciones'); //Ver dirección de un cliente
        Route::post('solicitudes-residente','ApiServiciosController@solicitudes_residente'); //Lista las solicitudes de servicio asignadas a un residente
       
        Route::post('tecnicos','ApiServiciosController@tecnicos'); //Lista los técnicos existentes
        Route::post('asigar-solicitud-tecnico','ApiServiciosController@asignar'); //Asigna una solicitud al técnico
        Route::post('listar-eventos','ApiServiciosController@lista_eventos'); //Muestra los horarios disponibles para selección de servicio
        Route::post('cliente-solicitudes','ApiClientesController@solicitudes'); //Lista todos los servicios realizados por el cliente
        Route::post('solicitud-materiales-tecnico','ApiTecnicosController@agregar_materiales'); //Agrega los materiales que necesita el técnico para la solicitud del cliente
        
        Route::post('guardar-cliente-id-conekta','ApiClientesController@guardar_id_conekta'); //Guarda el id de cliente proporcionado por Conekta
        Route::post('cliente-id-conekta','ApiClientesController@'); //Obtiene el id del cliente generado por conekta
        Route::post('aceptar-cotizacion','ApiClientesController@aceptar_cotizacion'); //Acepta o rechaza la cotización
        Route::post('ver-tecnico','ApiTecnicosController@perfil'); //Muestra los datos del perfil del técnico
        Route::post('cancelar-solicitud','ApiServiciosController@cancelar')->name('api_cancelar_solicitud'); //Cancela una solicitud de servicio antes de que sea asignada a un técnico
        Route::post('enviar-evidencias','ApiTecnicosController@enviar_evidencias'); //Permite al técnico enviar las evidencias del trabajo finalizado para que el residente las vea
       
        Route::post('finalizar','ApiServiciosController@finalizar'); //Finaliza el proceso para el técnico y el residente
        Route::post('cancelar-evidencias','ApiServiciosController@cancelar_evidencias'); //El residente cancela las evidencias del técnico
        Route::post('llaves-conekta','ApiClientesController@key_conekta'); //Regresa la llave privada de Conekta
        Route::post('materiales','ApiTecnicosController@materiales'); //Retorna la lista de materiales existentes
        Route::post('conceptos','ApiServiciosController@conceptos'); //Retorna los conceptos únicos del servicio general


        // Ruta de técnicos
        Route::post('solicitudes-tecnico','ApiTecnicosController@solicitudes'); //Lista las solicitudes de servicio asignadas a un técnico
        Route::post('tecnico-mensaje','ApiTecnicosController@mensaje');//El técnico envía un mensaje al cliente
        Route::post('ver-mensaje','ApiClientesController@mensaje_tecnico');//Muestra el mensaje envíado por el técnico al cliente
        Route::post('actualizar-llegada','ApiTecnicosController@actualizar_llegada'); //Actualizar fecha de llegada del técnico
        Route::post('marcar-inicio','ApiTecnicosController@marcar_inicio'); //Marca el inicio del trabajo del técnico
        Route::post('ver-evidencias','ApiTecnicosController@evidencias');//Muestra las evidencias enviadas por el técnico
    });
});

