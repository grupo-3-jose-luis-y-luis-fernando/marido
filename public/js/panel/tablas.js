$(document).ready(function () {
    $('#dt_proveedores').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: $("#dt_proveedores").data('url'),
            dataType: "json",
            type: "POST",
            data: {
                _token: $("meta[name='csrf-token'] ").attr('content')
            }
        },
        language: {
            url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        columns: [{
            data: "nombre"
        }, {
            data: "rfc"
        }, {
            data: "direccion"
        }, {
            data: "email"
        }, {
            data: "telefono"
        }, {
            className:"w-150px",
            data: "btn",
            "orderable": false
        }]
    });

    $('#dt_clientes').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: $("#dt_clientes").data('url'),
            dataType: "json",
            type: "POST",
            data: {
                _token: $("meta[name='csrf-token'] ").attr('content')
            }
        },
        language: {
            url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        columns: [{
            data: "nombre"
        }, {
            data: "direccion"
        }, {
            data: "rfc"
        }, {
            className:"w-150px",
            data: "btn",
            "orderable": false
        }]
    });
 
    $('#dt_unidades_medidas').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: $("#dt_unidades_medidas").data('url'),
            dataType: "json",
            type: "POST",
            data: {
                _token: $("meta[name='csrf-token'] ").attr('content')
            }
        },
        language: {
            url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        columns: [{
            data: "nombre"
        }, {
            className:"w-150px",
            data: "btn",
            "orderable": false
        }]
    });
    
    $('#dt_categorias').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: $("#dt_categorias").data('url'),
            dataType: "json",
            type: "POST",
            data: {
                _token: $("meta[name='csrf-token'] ").attr('content')
            }
        },
        language: {
            url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        columns: [{
            data: "nombre",
        },{
            data: "porcentaje"
        },
        {
            data: "imagen",
            className:"w-150px",
        },
        {
            className:"w-150px",
            data: "btn",
            "orderable": false
        }]
    });
    
    $('#dt_servicios').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: $("#dt_servicios").data('url'),
            dataType: "json",
            type: "POST",
            data: {
                _token: $("meta[name='csrf-token'] ").attr('content')
            }
        },
        language: {
            url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        columns: [{
            data: "nombre"
        }, {
            data: "categoria"
        }, {
            data: "costo"
        },{
            data: "descripcion"
        },  {
            className:"w-150px",
            data: "btn",
            "orderable": false
        }]
    });

    $('#dt_conceptos').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: $("#dt_conceptos").data('url'),
            dataType: "json",
            type: "POST",
            data: {
                _token: $("meta[name='csrf-token'] ").attr('content')
            }
        },
        language: {
            url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        columns: [
            { data: "clave" },
            { data: "nombre" },
            { data: "costo" },
            {
                className:"w-150px",
                data: "btn",
                "orderable": false
            }]
    });

    $('#dt_empleados').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: $("#dt_empleados").data('url'),
            dataType: "json",
            type: "POST",
            data: {
                _token: $("meta[name='csrf-token'] ").attr('content')
            }
        },
        language: {
            url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        columns: [{
            data: "name"
        }, {
            className:"w-250px",
            data: "email"
        }, {
            className:"w-150px",
            data: "rol"
        }, {
            className:"w-150px",
            data: "btn",
            "orderable": false
        }]
    });
    
    $('#dt_solicitudes').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: $("#dt_solicitudes").data('url'),
            dataType: "json",
            type: "POST",
            data: {
                _token: $("meta[name='csrf-token'] ").attr('content')
            }
        },
        language: {
            url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        columns: [
            { data: "folio"},
            { data: "servicio"},
            { data: "cliente" },
            { data: "tipo" },
            { data: "horario" },
            { data: "direccion" },
            { data: "estatus" },
            { data: "btn" }
        ]
    });

    $('#dt_materiales').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: $("#dt_materiales").data('url'),
            dataType: "json",
            type: "POST",
            data: {
                _token: $("meta[name='csrf-token'] ").attr('content')
            }
        },
        language: {
            url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        columns: [{
            data: "nombre"
        },
        {
            data: "unidad"
        },
        {
            data: "proveedores"
        },
        {
            className:"w-150px",
            data: "btn",
            "orderable": false
        }]
    });

    $('#dt_inventario').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: $("#dt_inventario").data('url'),
            dataType: "json",
            type: "POST",
            data: {
                _token: $("meta[name='csrf-token'] ").attr('content')
            }
        },
        language: {
            url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        columns: [{
            data: "nombre"
        },
        {
            data: "unidad"
        },
        {
            data: "cantidad"
        },
        {
            className:"w-150px",
            data: "btn",
            "orderable": false
        }]
    });

    $('#dt_slider').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: $("#dt_slider").data('url'),
            dataType: "json",
            type: "POST",
            data: {
                _token: $("meta[name='csrf-token'] ").attr('content')
            }
        },
        language: {
            url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        columns: [
            { data: "imagen" },
            { data: "btn" }
        ]
    });

    $('#dt_galeria').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: $("#dt_galeria").data('url'),
            dataType: "json",
            type: "POST",
            data: {
                _token: $("meta[name='csrf-token'] ").attr('content')
            }
        },
        language: {
            url: "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        columns: [
            { data: "imagen" },
            { data: "btn" }
        ]
    });
});