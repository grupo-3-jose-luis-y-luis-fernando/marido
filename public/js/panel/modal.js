$(document).ready(function () {
    $("body").on("submit", ".form", function (ev) {
        ev.preventDefault();
        var $this = $(this);
        var btnsubmit = $this.find("button[type='submit']"); 
        var data = $this.serialize();
        var alert = $this.data('alert');
        var processData = true;
        var contentType = 'application/x-www-form-urlencoded; charset=UTF-8';
        if($this.data('archivos')){
            data = new FormData(document.getElementById($this.attr('id')));
            processData = false;
            contentType = false;
        }
        if(alert){
            Swal.fire({
                title: '¿Está seguro de realizar esta acción?',
                text: 'No se podrá revertir esta acción',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if (result.isConfirmed) {
                    ajax($this,data,processData,contentType,btnsubmit)
                }else{

                }
            })
        }else{
            ajax($this,data,processData,contentType,btnsubmit)
        }
    }); 

    function ajax($this,data,processData,contentType,btnsubmit) {
        $.ajax({
            beforeSend: function beforeSend() {
                $(".cargando").removeClass('d-none');
                $this.find('.is-invalid').removeClass('is-invalid');
                btnsubmit.prop('disabled', true);
            },
            url: $this.attr('action'),
            type: "POST",
            data: data,
            processData: processData,
            contentType: contentType
        }).done(function (response) {
            $(".cargando").addClass('d-none');
            btnsubmit.prop('disabled', false); 
            if (response.swal) {
                Swal.fire({
                    icon: response.type,
                    title: response.title,
                    text: response.text
                }).then(function (result) {
                    if (result.value || result.dismiss === Swal.DismissReason.backdrop) {
                        funciones_ajax(response, $this);
                    }
                });
            }else{
                funciones_ajax(response, $this);
            }
        }).fail(function (response) { 
            if (response.status == 422) {
                $this.find('.is-invalid').removeClass('is-invalid');
                Swal.fire({
                    icon: "error",
                    title: "Error",
                    text: "Faltan datos por rellenar o existe algún detalle con éstos"
                });
                $.each(response.responseJSON.errors, function (index, value) {
                    $this.find("#" + index).addClass('is-invalid').parent().find('.invalid-feedback').html(value);
                });
            } else {
                Swal.fire({
                    icon: "error",
                    title: "Error",
                    text: response.responseJSON.message
                });
            }
            $(".cargando").addClass('d-none');
            btnsubmit.prop('disabled', false);
        });
    }
    
    function funciones_ajax(response,form) {    
        console.log(response);   
        if (response.reload) { //Recarga la página
            location.reload();
        }
        if (response.dttable) { //Recarga un datatable
            $(response.dttable).DataTable().ajax.reload();  
        } 
        if (response.modal_close) { //Cierra un modal abierto
            $(".modal").modal('hide');
        } 
        if (response.modal) { //Llena el cuerpo de un modal
            $(".modal .modal-content").html(response.modal);
            $(".modal").modal();
        }
        if (response.load) { //Redirecciona a una url específica
            window.location.href = response.load;
        }
        if(response.form_clear){
            form[0].reset();
        }
    }

    $("body").on('keyup', '.onlynumber', function(){
        var costo = moneda($(this).val());
        $(this).val(costo);
    }); 
    
});
function moneda(v){     
    v=v.replace(/([^0-9\.]+)/g,''); 
    v=v.replace(/^[\.]/,''); 
    v=v.replace(/[\.][\.]/g,''); 
    v=v.replace(/\.(\d)(\d)(\d)/g,'.$1$2'); 
    v=v.replace(/\.(\d{1,2})\./g,'.$1'); 
    v = v.toString().split('').reverse().join('').replace(/(\d{3})/g,'$1,');    
    v = v.split('').reverse().join('').replace(/^[\,]/,''); 
    return v;  
} 
